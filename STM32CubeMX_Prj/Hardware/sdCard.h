/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sdCard.h
 * Author     : 上上签
 * Date       : 2023-06-01
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __SDCARD_H__
#define __SDCARD_H__

/* 头文件 */
#include "../Core/Inc/main.h"
#include "../Core/Inc/sdio.h"
#include "./sys.h"
/* 宏定义 */
#define SD_TIMEOUT 			((uint32_t)100000000)  	//超时时间
#define SD_TRANSFER_OK     	((uint8_t)0x00)
#define SD_TRANSFER_BUSY   	((uint8_t)0x01) 

/* 结构体定义 */

/* 全局变量声明 */
extern HAL_SD_CardInfoTypeDef  SDCardInfo; //SD卡信息结构体

/* 函数声明 */

uint8_t SD_GetCardState(void);
void show_sdcard_info(void);
uint8_t SD_ReadDisk(uint8_t* buf,uint32_t sector,uint32_t cnt);
uint8_t SD_WriteDisk(uint8_t* buf,uint32_t sector,uint32_t cnt);
void SD_Test(void);

#endif /* __SDCARD_H__ */
