/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : lcd.h
 * Author     : 上上签
 * Date       : 2023-05-21
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __LCD_H__
#define __LCD_H__

/* 头文件 */
#include <string.h>
#include "main.h"
#include "../font/font_data.h"
#include "../font/func_font.h"

/* 宏定义 */
/******************************* ILI9341 显示屏的 FSMC 参数定义 ***************************/
//FSMC_Bank1_NORSRAM用于LCD命令操作的地址
#define FSMC_Addr_ILI9341_CMD  ( ( uint32_t )(0X6C000000))
//FSMC_Bank1_NORSRAM用于LCD数据操作的地址      
#define FSMC_Addr_ILI9341_DATA ( ( uint32_t )(0X6C000800))

#define WHITE           0xFFFF
#define BLACK           0x0000
#define BLUE            0x001F
#define BRED            0XF81F
#define GRED            0XFFE0
#define GBLUE           0X07FF
#define RED             0xF800
#define MAGENTA         0xF81F
#define GREEN           0x07E0
#define CYAN            0x7FFF
#define YELLOW          0xFFE0
#define BROWN           0XBC40  // 棕色
#define BRRED           0XFC07  // 棕红色
#define GRAY            0X8430  // 灰色

//扫描方向定义
#define XY_NORMAL              0  // 竖屏(X,240列；Y,320行)，从左到右，从上到下（竖着看）
#define XY_EXCHANGE            1  // 横屏(X,320列；Y,240行)，从上到下，从左到右（竖着看）
#define X_MIRROR               2  // 竖屏(X,240列；Y,320行)，从右到左，从上到下（竖着看）
#define XY_EXCHANGE_X_MIRROR   3  // 横屏(X,320列；Y,240行)，从上到下，从右到左（竖着看）
#define Y_MIRROR               4  // 竖屏(X,240列；Y,320行)，从左到右，从下到上（竖着看）
#define XY_EXCHANGE_Y_MIRROR   5  // 横屏(X,320列；Y,240行)，从下到上，从左到右（竖着看）
#define XY_MIRROR              6  // 竖屏(X,240列；Y,320行)，从右到左，从下到上（竖着看）
#define XY_EXCHANGE_XY_MIRROR  7  // 横屏(X,320列；Y,240行)，从下到上，从右到左（竖着看）

/***************************** ILI934 显示区域的起始坐标和总行列数 ***************************/
#define ILI9341_DispWindow_X_Star 0     // 起始点的X坐标
#define ILI9341_DispWindow_Y_Star 0     // 起始点的Y坐标
#define ILI9341_LESS_PIXEL        240   // 液晶屏较短方向的像素宽度
#define ILI9341_MORE_PIXEL        320   // 液晶屏较长方向的像素宽度

/******************************* 定义 ILI934 常用命令 ********************************/
#define CMD_SetCoordinateX 0x2A	     // 设置X坐标
#define CMD_SetCoordinateY 0x2B	     // 设置Y坐标
#define CMD_SetPixel       0x2C	     // 填充像素

#define ZOOMMAXBUFF 16384            // 缩放字模缓冲区最大大小

/* 定义 LCD 驱动芯片 ID */
#define LCDID_UNKNOWN      0
#define LCDID_ILI9341      0x9341
#define LCDID_ST7789V      0x8552
/* 结构体定义 */
//LCD重要参数集
typedef struct
{
    uint16_t id;           // LCD ID
    uint8_t LCD_SCAN_MODE; // LCD 扫描模式，参数可选值为0-7
    uint16_t LCD_X_LENGTH; // LCD X方向长度
    uint16_t LCD_Y_LENGTH; // LCD Y方向长度
    uint16_t PonitColor;   // LCD 画笔颜色，就是前景色
    uint16_t BackColor;    // LCD 背景颜色
    FONT_PARAM *LCD_Currentfonts; // LCD 英文字符数组
} _lcd_dev;

/* 全局变量声明 */
extern _lcd_dev lcd_param; //管理LCD重要参数

/* 函数声明 */

void ILI9341_Write_Cmd(uint16_t usCmd);
void ILI9341_Write_Data(uint16_t usData);
uint16_t ILI9341_Read_Data(void);
void ILI9341_ID_Read(void);
void ILI9341_DisplayOn(void);
void ILI9341_DisplayOff(void);
void ILI9341_SetCursor(uint16_t usX, uint16_t usY);
void ILI9341_SetPointPixel(uint16_t usX, uint16_t usY);
void ILI9341_Fast_DrawPoint(uint16_t usX, uint16_t usY, uint16_t usColor);
uint16_t ILI9341_PowerStatus_Read(void);
void ILI9341_Clear(uint16_t usColor);
void ILI9341_GramScan(uint8_t ucOption);
void ILI9341_Config(void);

void LCD_DrawLine(uint16_t usX1, uint16_t usY1, uint16_t usX2, uint16_t usY2);
void LCD_DrawRectangle(uint16_t usX_Start, uint16_t usY_Start, uint16_t usWidth, uint16_t usHeight, uint8_t ucFilled);
void LCD_DrawCircle(uint16_t usX_Center, uint16_t usY_Center, uint16_t usRadius, uint8_t ucFilled);
void LCD_Fill(uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight, uint16_t usColor);

void LCD_SetBackColor(uint16_t Color);
void LCD_SetTextColor(uint16_t Color);
void LCD_SetColors(uint16_t TextColor, uint16_t BackColor);

void LCD_SetFontSize(FONT_PARAM *fonts);

uint32_t LCD_Pow(uint8_t m, uint8_t n);
void LCD_ShowChar(uint16_t usX, uint16_t usY, const char cChar);
void LCD_ShowString(uint16_t usX, uint16_t usY, char *pStr);
void LCD_ShowNum(uint16_t usX, uint16_t usY, uint32_t num, uint8_t len);
void LCD_ShowxNum(uint16_t usX, uint16_t usY, uint32_t num, uint8_t len, uint8_t mode);

void LCD_ShowChar_CH(uint16_t usX, uint16_t usY, char *font);
void LCD_Show_Str_ENCH(uint16_t usX, uint16_t usY, char *str);

void LCD_Show_Info(void);
void LCD_init(void);

void LCD_Func_Test(void);
#endif
