/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sdCard.c
 * Author     : 上上签
 * Date       : 2023-06-01
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./sdCard.h"

HAL_SD_CardInfoTypeDef  SDCardInfo;                 //SD卡信息结构体

/**
  * @brief  获取SD卡状态
  * @note   判断SD卡是否可以传输(读写)数据
  * @param  
  * @retval 返回当前SD卡状态
  *         @arg SD_TRANSFER_OK 传输完成，可以继续下一次传输;
  *         @arg SD_TRANSFER_BUSY SD卡正忙，不可以进行下一次传输
  */
uint8_t SD_GetCardState(void)
{
	return((HAL_SD_GetCardState(&hsd)==HAL_SD_CARD_TRANSFER )?SD_TRANSFER_OK:SD_TRANSFER_BUSY);
}

/**
  * @brief  通过串口打印SD卡相关信息
  * @note   
  * @param  
  * @retval 
  */
void show_sdcard_info(void)
{
    uint64_t CardCap; // SD卡容量
    HAL_SD_CardCIDTypeDef SDCard_CID;

    /* 检测SD卡是否正常（处于数据传输模式的传输状态） */
    if (HAL_SD_GetCardState(&hsd) != HAL_SD_CARD_TRANSFER)
    {
        printf("SD card init fail!\r\n");
        return;
    }
    printf("Initialize SD card successfully!\r\n");
    HAL_SD_GetCardCID(&hsd, &SDCard_CID);  // 获取CID
    HAL_SD_GetCardInfo(&hsd, &SDCardInfo); // 获取SD卡信息
    switch (SDCardInfo.CardType)
    {
        case CARD_SDSC:
        {
            if (SDCardInfo.CardVersion == CARD_V1_X)
            {
                printf("Card Type          :SDSC V1 \r\n");
            }
            else if (SDCardInfo.CardVersion == CARD_V2_X)
            {
                printf("Card Type          :SDSC V2 \r\n");
            }
        }
        break;
        case CARD_SDHC_SDXC:
            printf("Card Type          :SDHC \r\n");
            break;
    }
    CardCap = (uint64_t)(SDCardInfo.LogBlockNbr) * (uint64_t)(SDCardInfo.LogBlockSize); // 计算SD卡容量
    printf("Card ManufacturerID:%d \r\n", SDCard_CID.ManufacturerID);                   // 制造商ID
    printf("Card RCA           :%d \r\n", SDCardInfo.RelCardAdd);                       // 卡相对地址
    printf("LogBlockNbr        :%d \r\n", (uint32_t)(SDCardInfo.LogBlockNbr));          // 显示逻辑块数量
    printf("LogBlockSize       :%d \r\n", (uint32_t)(SDCardInfo.LogBlockSize));         // 显示逻辑块大小
    printf("Card Capacity      :%d MB\r\n", (uint32_t)(CardCap >> 20));                 // 显示容量
    printf("Card BlockSize     :%d \r\n\r\n", SDCardInfo.BlockSize);                    // 显示块大小
}

/**
  * @brief  向SD卡写入数据
  * @note   
  * @param  buf 写数据缓存区
  * @param  sector 扇区地址
  * @param  cnt 扇区个数
  * @retval 返回错误状态;0,正常;其他,错误代码;
  */
uint8_t SD_WriteDisk(uint8_t *buf, uint32_t sector, uint32_t cnt)
{
    uint8_t sta = HAL_OK;
    uint32_t timeout = SD_TIMEOUT;
    long long lsector = sector;
    INTX_DISABLE();                                                           // 关闭总中断(POLLING模式,严禁中断打断SDIO读写操作!!!)
    sta = HAL_SD_WriteBlocks(&hsd, (uint8_t *)buf, lsector, cnt, SD_TIMEOUT); // 多个sector的写操作

    // 等待SD卡写完
    while (HAL_SD_GetCardState(&hsd) != HAL_SD_CARD_TRANSFER)
    {
        if (timeout-- == 0)
        {
            sta = SD_TRANSFER_BUSY;
        }
    }
    INTX_ENABLE(); // 开启总中断
    return sta;
}

/**
  * @brief  从SD卡读取数据
  * @note   
  * @param  buf 写数据缓存区
  * @param  sector 扇区地址
  * @param  cnt 扇区个数
  * @retval 返回错误状态;0,正常;其他,错误代码;
  */
uint8_t SD_ReadDisk(uint8_t *buf, uint32_t sector, uint32_t cnt)
{
    uint8_t sta = HAL_OK;
    uint32_t timeout = SD_TIMEOUT;
    long long lsector = sector;
    INTX_DISABLE();                                                          // 关闭总中断(POLLING模式,严禁中断打断SDIO读写操作!!!)
    sta = HAL_SD_ReadBlocks(&hsd, (uint8_t *)buf, lsector, cnt, SD_TIMEOUT); // 多个sector的读操作

    // 等待SD卡读完
    while (SD_GetCardState() != SD_TRANSFER_OK)
    {
        if (timeout-- == 0)
        {
            sta = SD_TRANSFER_BUSY;
        }
    }
    INTX_ENABLE(); // 开启总中断
    return sta;
}

uint8_t sdTxBuffer[512];
uint8_t sdRxBuffer[512];

/**
  * @brief  测试SD卡的读取
  * @note   从secaddr地址开始,读取seccnt个扇区的数据
  * @param  secaddr 扇区地址
  * @param  seccnt 扇区数
  * @retval 
  */
void sd_test_read(uint32_t secaddr, uint32_t seccnt)
{
    uint32_t i;
    uint8_t sta = 0;
    sta = SD_ReadDisk(sdRxBuffer, secaddr, seccnt); // 读取secaddr扇区开始的内容
    if (sta == 0)
    {
        printf("SD test:read %#x over!!!\r\n", secaddr);
        for (i = 0; i < seccnt * 512; i++)
            printf("%#x ", sdRxBuffer[i]); // 打印secaddr开始的扇区数据
        printf("\r\nread data show end!!!\r\n");
    }
    else
        printf("SD test:read err:%d\r\n", sta);
}

/**
  * @brief  测试SD卡的写入
  * @note   从secaddr地址开始,写入seccnt个扇区的数据
  * @param  secaddr 扇区地址
  * @param  seccnt 扇区数 
  * @retval 
  */
void sd_test_write(uint32_t secaddr, uint32_t seccnt)
{
    uint32_t i;
    uint8_t sta = 0;
    for (i = 0; i < seccnt * 512; i++)
        sdTxBuffer[i] = i * 3;                       // 初始化写入的数据,是3的倍数.
    sta = SD_WriteDisk(sdTxBuffer, secaddr, seccnt); // 从secaddr扇区开始写入seccnt个扇区内容
    if (sta == 0)
        printf("SD test:Write over!\r\n");
    else
        printf("SD test:write test err:%d\r\n", sta);
}

void SD_Test(void)
{
    show_sdcard_info();
	sd_test_write(0, 1);
	sd_test_read(0, 1);
}

