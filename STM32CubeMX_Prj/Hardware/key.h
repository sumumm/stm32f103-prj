/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : key.h
 * Author     : 上上签
 * Date       : 2023-05-31
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __KEY_H__
#define __KEY_H__

/* 头文件 */
#include "main.h"
#include "./sys.h"
/* 宏定义 */
// 下面的方式是通过位带操作方式读取IO
// #define KEY0        PEin(4) 	// KEY0按键PE4
// #define KEY1        PEin(3) 	// KEY1按键PE3
// #define KEY2        PEin(2)	// KEY2按键PE2
// #define WK_UP       PAin(0) 	// WKUP按键PA0

// 下面的方式是通过直接操作HAL库函数方式读取IO
#define KEY0  HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4)  // KEY0按键PE4
#define KEY1  HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)  // KEY1按键PE3
#define KEY2  HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_2)  // KEY2按键PE2
#define WK_UP HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)  // WKUP按键PA0

#define KEY0_PRES 1
#define KEY1_PRES 2
#define KEY2_PRES 3
#define WKUP_PRES 4
/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */

uint8_t KEY_Scan(uint8_t mode);

void key_test(void);
#endif
