/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sram.h
 * Author     : 上上签
 * Date       : 2023-05-21
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __SRAM_H__
#define __SRAM_H__

/* 头文件 */
#include "main.h"
#include "./lcd.h"
/* 宏定义 */
// 使用NOR/SRAM的 Bank1.sector3,地址位HADDR[27,26]=10 
// 对IS61LV25616/IS62WV25616,地址线范围为A0~A17 
// 对IS61LV51216/IS62WV51216,地址线范围为A0~A18
#define Bank1_SRAM3_ADDR    ((uint32_t)(0x68000000))			
#define IS62WV51216_SIZE     0x100000   // 512*16/2bits = 0x100000  ，1M字节
#define PrestoredTest        0
/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */
void FSMC_SRAM_WriteBuffer(uint8_t *pBuffer,uint32_t WriteAddr,uint32_t n);
void FSMC_SRAM_ReadBuffer(uint8_t *pBuffer,uint32_t ReadAddr,uint32_t n);

int rw_8bit_test(void);
int rw_16bit_test(void);
void rw_nByte_test(void);
int8_t func_exsram_test(uint16_t x, uint16_t y);

#if PrestoredTest == 1
void SRAM_Test(void);
#endif
#endif
