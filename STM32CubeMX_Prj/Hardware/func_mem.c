/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : memory.c
 * Author     : 上上签
 * Date       : 2023-06-03
 * Version    : 
 * Description: 
 * ======================================================
 */

#include "./func_mem.h"

// 内存池(32字节对齐)
uint8_t mem1base[MEM1_MAX_SIZE] __attribute__((aligned(32)));                                 // 内部SRAM内存池
uint8_t mem2base[MEM2_MAX_SIZE] __attribute__((aligned(32))) __attribute__((at(0X68000000))); // 外部SRAM内存池
// 内存管理表
uint16_t mem1mapbase[MEM1_ALLOC_TABLE_SIZE];                                                 // 内部SRAM内存池MAP
uint16_t mem2mapbase[MEM2_ALLOC_TABLE_SIZE] __attribute__((at(0X68000000 + MEM2_MAX_SIZE))); // 外部SRAM内存池MAP
// 内存管理参数
const uint32_t memtblsize[SRAMBANK] = {MEM1_ALLOC_TABLE_SIZE, MEM2_ALLOC_TABLE_SIZE}; // 内存表大小
const uint32_t memblksize[SRAMBANK] = {MEM1_BLOCK_SIZE, MEM2_BLOCK_SIZE};             // 内存分块大小
const uint32_t memsize[SRAMBANK] = {MEM1_MAX_SIZE, MEM2_MAX_SIZE};                    // 内存总大小

// 内存管理控制器
FUNC_MEM mallco_dev = {
        pub_mem_init,            // 内存初始化
        pub_mem_perused,         // 内存使用率
        mem1base, mem2base,       // 内存池
        mem1mapbase, mem2mapbase, // 内存管理状态表
        0, 0,                     // 内存管理未就绪
};

/**
  * @brief  内存分配(内部调用)
  * @note   
  * @param  memx 所属内存块
  * @param  size 要分配的内存大小(字节)
  * @retval 0XFFFFFFFF,代表错误;其他,内存偏移地址
  */
uint32_t func_mem_malloc(uint8_t memx, uint32_t size)
{
    signed long offset = 0;
    uint32_t nmemb;     // 需要的内存块数
    uint32_t cmemb = 0; // 连续空内存块数
    uint32_t i;
    if (!mallco_dev.memrdy[memx])
        mallco_dev.init(memx); // 未初始化,先执行初始化
    if (size == 0)
        return 0XFFFFFFFF;           // 不需要分配
    nmemb = size / memblksize[memx]; // 获取需要分配的连续内存块数
    if (size % memblksize[memx])
        nmemb++;
    for (offset = memtblsize[memx] - 1; offset >= 0; offset--) // 搜索整个内存控制区
    {
        if (!mallco_dev.memmap[memx][offset])
            cmemb++; // 连续空内存块数增加
        else
            cmemb = 0;      // 连续内存块清零
        if (cmemb == nmemb) // 找到了连续nmemb个空内存块
        {
            for (i = 0; i < nmemb; i++) // 标注内存块非空
            {
                mallco_dev.memmap[memx][offset + i] = nmemb;
            }
            return (offset * memblksize[memx]); // 返回偏移地址
        }
    }
    return 0XFFFFFFFF; // 未找到符合分配条件的内存块
}

/**
  * @brief  释放内存(内部调用)
  * @note   
  * @param  memx 所属内存块
  * @param  offset 内存地址偏移
  * @retval 0,释放成功;1,释放失败;
  */
uint8_t func_mem_free(uint8_t memx, uint32_t offset)
{
    int i = 0;
    if (!mallco_dev.memrdy[memx]) // 未初始化,先执行初始化
    {
        mallco_dev.init(memx);
        return 1; // 未初始化
    }
    if (offset < memsize[memx]) // 偏移在内存池内.
    {
        int index = offset / memblksize[memx];      // 偏移所在内存块号码
        int nmemb = mallco_dev.memmap[memx][index]; // 内存块数量
        for (i = 0; i < nmemb; i++)                 // 内存块清零
        {
            mallco_dev.memmap[memx][index + i] = 0;
        }
        return 0;
    }
    else
        return 2; // 偏移超区了.
}

/**
  * @brief  复制内存中的数据
  * @note   
  * @param  dst 目的地址
  * @param  src 源地址
  * @param  n 需要复制的内存长度(字节为单位)
  * @retval 
  */
void pub_memcpy(void *dst, void *src, uint32_t n)
{
    uint8_t *xdst = dst;
    uint8_t *xsrc = src;
    while (n--)
    {
        *xdst++ = *xsrc++;
    }
}

/**
  * @brief  用指定的值填充内存块
  * @note   
  * @param  addr 内存首地址
  * @param  value 要填充的值
  * @param  count 要填充的内存大小(字节为单位)
  * @retval 
  */
void pub_memset(void *addr, uint8_t value, uint32_t count)
{
    uint8_t *xaddr = addr;
    while (count--)
    {
        *xaddr++ = value;
    }
}

/**
  * @brief  获取内存使用率
  * @note   
  * @param  memx 所属内存块
  * @retval 使用率(0~100)
  */
uint8_t pub_mem_perused(uint8_t memx)
{
    uint32_t used = 0;
    uint32_t i;
    for (i = 0; i < memtblsize[memx]; i++)
    {
        if (mallco_dev.memmap[memx][i])
            used++;
    }
    return (used * 100) / (memtblsize[memx]);
}

/**
  * @brief  释放内存(外部调用)
  * @note   
  * @param  memx 所属内存块
  * @retval ptr 内存首地址
  */
void pub_free(uint8_t memx, void *ptr)
{
    uint32_t offset;
    if (ptr == NULL)
    {
        return; // 地址为0.
    }
    offset = (uint32_t)ptr - (uint32_t)mallco_dev.membase[memx];
    func_mem_free(memx, offset); // 释放内存
}

/**
  * @brief  分配内存(外部调用)
  * @note   
  * @param  memx 所属内存块
  * @param  size 内存大小(字节)
  * @retval 分配到的内存首地址.
  */
void *pub_malloc(uint8_t memx, uint32_t size)
{
    uint32_t offset;
    offset = func_mem_malloc(memx, size);
    if (offset == 0XFFFFFFFF)
        return NULL;
    else
        return (void *)((uint32_t)mallco_dev.membase[memx] + offset);
}

/**
  * @brief  重新分配内存(外部调用)
  * @note   
  * @param  memx 所属内存块
  * @param  ptr 旧内存首地址
  * @param  size 要分配的内存大小(字节)
  * @retval 新分配到的内存首地址.
  */
void *pub_realloc(uint8_t memx, void *ptr, uint32_t size)
{
    uint32_t offset;
    offset = func_mem_malloc(memx, size);
    if (offset == 0XFFFFFFFF)
    {
        return NULL;
    }
    else
    {
        pub_memcpy((void *)((uint32_t)mallco_dev.membase[memx] + offset), ptr, size); // 拷贝旧内存内容到新内存
        pub_free(memx, ptr);                                                           // 释放旧内存
        return (void *)((uint32_t)mallco_dev.membase[memx] + offset);                  // 返回新内存首地址
    }
}
/**
  * @brief  内存管理模块初始化
  * @note   
  * @param  memx 所属内存块
  * @retval 
  */
int8_t pub_mem_init(uint8_t memx)
{
    pub_memset(mallco_dev.memmap[memx], 0, memtblsize[memx] * 2); // 内存状态表数据清零
    pub_memset(mallco_dev.membase[memx], 0, memsize[memx]);       // 内存池所有数据清零
    mallco_dev.memrdy[memx] = 1;                                   // 内存管理初始化OK
    return 0;
}

/**
  * @brief  初始化所有的内存模块
  * @note   包括内部和外部SRAM
  * @param  mem_num 内存模块数量
  * @retval 
  */
void pub_mem_init_all(void)
{
    uint8_t i = 0;
    for (i = 0; i < SRAMBANK; i++)
    {
        pub_mem_init(i);
    }
    printf("mem1mapbase addr:0x%08X - 0x%08X [%6dB %3dKB]\r\n", 
			(uint32_t)mem1mapbase, (uint32_t)&mem1mapbase[MEM1_ALLOC_TABLE_SIZE], 
			(&mem1mapbase[MEM1_ALLOC_TABLE_SIZE] - mem1mapbase), 
			(&mem1mapbase[MEM1_ALLOC_TABLE_SIZE] - mem1mapbase) / 1024);
    printf("mem1base addr:0x%08X - 0x%08X blk_size:%dB [%6dB %3dKB]\r\n", 
			(uint32_t)mem1base, (uint32_t)&mem1base[MEM1_MAX_SIZE], MEM1_BLOCK_SIZE, 
			(&mem1base[MEM1_MAX_SIZE] - mem1base), (&mem1base[MEM1_MAX_SIZE] - mem1base) / 1024);
    if (SRAMBANK == 2)
    {
        printf("mem2mapbase addr:0x%08X - 0x%08X [%6dB %3dKB]\r\n", 
			(uint32_t)mem2mapbase, (uint32_t)&mem2mapbase[MEM2_ALLOC_TABLE_SIZE], 
			(&mem2mapbase[MEM2_ALLOC_TABLE_SIZE] - mem2mapbase), 
			(&mem2mapbase[MEM2_ALLOC_TABLE_SIZE] - mem2mapbase) / 1024);
		printf("mem2base addr:0x%08X - 0x%08X blk_size:%dB [%6dB %3dKB]\r\n", 
			(uint32_t)mem2base, (uint32_t)&mem2base[MEM2_MAX_SIZE], MEM2_BLOCK_SIZE, 
			(&mem2base[MEM2_MAX_SIZE] - mem2base), (&mem2base[MEM2_MAX_SIZE] - mem2base) / 1024);
    }
}

void func_mem_test(void)
{
    uint8_t key = 0;
    uint8_t *p = NULL;
    uint8_t *p_last = NULL;
    uint8_t paddr[18]; // 存放P Addr:+p地址的ASCII值
    uint8_t sramx = 0;
    uint32_t i = 0;

    pub_mem_init_all(); // 初始化内部内存池

	LCD_ShowString(0, 0*lcd_param.LCD_Currentfonts->Height, "This is my memory test!!!");
	LCD_ShowString(0, 1*lcd_param.LCD_Currentfonts->Height, "KEY0:Malloc  KEY2:Free");
	LCD_ShowString(0, 2*lcd_param.LCD_Currentfonts->Height, "KEY_UP:SRAMx KEY1:Write");
	LCD_ShowString(0, 3*lcd_param.LCD_Currentfonts->Height, "current mem test:SRAMIN");
	LCD_ShowString(0, 5*lcd_param.LCD_Currentfonts->Height, "SRAMIN USED:   %");
	LCD_ShowString(0, 6*lcd_param.LCD_Currentfonts->Height, "SRAMEX USED:   %");
	
    while (1)
    {
		key = KEY_Scan(0);
        switch (key)
        {
			case KEY0_PRES:                  // KEY0按下
				p = pub_malloc(sramx, 2048); // 申请2K字节
				if (p != NULL)
				{
					sprintf((char *)p, "Memory Malloc Test %03d", i); // 向p写入一些内容
				}
				break;
			case KEY1_PRES: // KEY1按下
				if (p != NULL)
				{
					sprintf((char *)p, "Memory Malloc Test %03d", i);          // 更新显示内容
					LCD_ShowString(0, 8*lcd_param.LCD_Currentfonts->Height, (char *)p); // 显示P的内容
				}
				break;
			case KEY2_PRES:         // KEY2按下
				pub_free(sramx, p); // 释放内存
				p = NULL;           // 指向空地址
				break;
			case WKUP_PRES:     // KEY UP按下
				sramx = !sramx; // 切换当前malloc/free操作对象
				if (sramx)
					LCD_ShowString(17*lcd_param.LCD_Currentfonts->Width, 3*lcd_param.LCD_Currentfonts->Height, "SRAMEX");
				else
					LCD_ShowString(17*lcd_param.LCD_Currentfonts->Width, 3*lcd_param.LCD_Currentfonts->Height, "SRAMIN");
				break;
			default:
				break;
        }
        if (p_last != p)
        {
            p_last = p;
            sprintf((char *)paddr, "P Addr:0X%08X", (uint32_t)p_last);
            LCD_ShowString(0, 7*lcd_param.LCD_Currentfonts->Height, (char *)paddr); // 显示p的地址
            if (p != NULL)
                LCD_ShowString(0, 8*lcd_param.LCD_Currentfonts->Height, (char *)p); // 显示P的内容
            else
                LCD_Fill(0, 8*lcd_param.LCD_Currentfonts->Height, 240, lcd_param.LCD_Currentfonts->Height, WHITE); // p=0,清除显示
        }
        HAL_Delay(10);
        i++;
        if ((i % 20) == 0) // DS0闪烁.
        {
            LCD_ShowNum(12*lcd_param.LCD_Currentfonts->Width, 5*lcd_param.LCD_Currentfonts->Height, pub_mem_perused(SRAMIN), 3); // 显示内部内存使用率
            LCD_ShowNum(12*lcd_param.LCD_Currentfonts->Width, 6*lcd_param.LCD_Currentfonts->Height, pub_mem_perused(SRAMEX), 3); // 显示外部内存使用率
            LED0 = !LED0;
        }
    }
}
