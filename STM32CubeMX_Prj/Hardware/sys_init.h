/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sys_init.h
 * Author     : 上上签
 * Date       : 2023-06-09
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __SYS_INIT_H__
#define __SYS_INIT_H__

/* 头文件 */
#include "../Core/Inc/main.h"
#include "../Core/Inc/gpio.h"

#include "../FatFs/func_fatfs.h"

#include "../font/func_font.h"

#include "./func_mem.h"
#include "./lcd.h"
#include "./sram.h"
#include "./w25qxx.h"
#include "./key.h"


/* 宏定义 */

/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */

void system_init(void);
void system_error_show(uint16_t x, uint16_t y, char *err);
uint8_t system_files_erase(uint16_t x, uint16_t y);
uint8_t system_font_update_confirm(uint16_t x, uint16_t y);
#endif /* __SYS_INIT_H__ */
