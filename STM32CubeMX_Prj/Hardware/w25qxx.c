/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : w25qxx.c
 * Author     : 上上签
 * Date       : 2023-06-01
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./w25qxx.h"
#include "./func_printf.h"
uint16_t W25QXX_TYPE = W25Q256;	//默认是W25Q25,读取到别的会再修改

/**
  * @brief  读取芯片ID
  * @note   
  * @param  
  * @retval 芯片型号，0XEF17,表示芯片型号为W25Q128
  */
uint16_t W25QXX_ReadID(void)
{
    uint16_t Temp = 0;
    W25QXX_CS = 0;
    SPI2_ReadWriteByte(0x90);//发送读取ID命令
    SPI2_ReadWriteByte(0x00);
    SPI2_ReadWriteByte(0x00);
    SPI2_ReadWriteByte(0x00);
    Temp |= SPI2_ReadWriteByte(0xFF) << 8;
    Temp |= SPI2_ReadWriteByte(0xFF);
    W25QXX_CS = 1;
    return Temp;
}

// 动态管理内存
#ifndef	SRAMIN   
uint8_t W25QXX_BUFFER[4096];
#endif	
/**
  * @brief  向 SPI FLASH 写入指定字节数据
  * @note   在指定地址开始写入指定长度的数据,该函数带擦除操作!
  * @param  pBuffer 数据存储区
  * @param  WriteAddr 开始写入的地址(24bit)
  * @param  NumByteToWrite 要写入的字节数(最大65535)  
  * @retval 成功返回0
  */
int8_t W25QXX_Write(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
    uint32_t secpos;
    uint16_t secoff;
    uint16_t secremain;
    uint16_t i;
    uint8_t *W25QXX_BUF;
#ifdef SRAMIN
    W25QXX_BUF = (uint8_t *)pub_malloc(SRAMIN, 4096); // 申请内存
    if (W25QXX_BUF == NULL)
    {
        PRTE("pub_malloc failed!!!\r\n");
        return -1; // 申请失败
    }
#else
    W25QXX_BUF = W25QXX_BUFFER;
#endif
    secpos = WriteAddr / W25QXX_SECTOR_SIZE; // 扇区地址
    secoff = WriteAddr % W25QXX_SECTOR_SIZE; // 在扇区内的偏移
    secremain = W25QXX_SECTOR_SIZE - secoff; // 扇区剩余空间大小
    // printf("ad:%X,nb:%X\r\n",WriteAddr,NumByteToWrite);//测试用
    if (NumByteToWrite <= secremain)
        secremain = NumByteToWrite; // 不大于4096个字节
    while (1)
    {
        W25QXX_Read(W25QXX_BUF, secpos * W25QXX_SECTOR_SIZE, W25QXX_SECTOR_SIZE); // 读出整个扇区的内容
        for (i = 0; i < secremain; i++)                                           // 校验数据
        {
            if (W25QXX_BUF[secoff + i] != 0XFF)
                break; // 需要擦除
        }
        if (i < secremain) // 需要擦除
        {
            W25QXX_Erase_Sector(secpos);    // 擦除这个扇区
            for (i = 0; i < secremain; i++) // 复制
            {
                W25QXX_BUF[i + secoff] = pBuffer[i];
            }
            W25QXX_Write_NoCheck(W25QXX_BUF, secpos * W25QXX_SECTOR_SIZE, W25QXX_SECTOR_SIZE); // 写入整个扇区
        }
        else
            W25QXX_Write_NoCheck(pBuffer, WriteAddr, secremain); // 写已经擦除了的,直接写入扇区剩余区间.
        if (NumByteToWrite == secremain)
            break; // 写入结束了
        else       // 写入未结束
        {
            secpos++;   // 扇区地址增1
            secoff = 0; // 偏移位置为0

            pBuffer += secremain;        // 指针偏移
            WriteAddr += secremain;      // 写地址偏移
            NumByteToWrite -= secremain; // 字节数递减
            if (NumByteToWrite > W25QXX_SECTOR_SIZE)
                secremain = W25QXX_SECTOR_SIZE; // 下一个扇区还是写不完
            else
                secremain = NumByteToWrite; // 下一个扇区可以写完了
        }
    }
#ifdef SRAMIN
    pub_free(SRAMIN, W25QXX_BUF); // 释放内存
#endif
    return 0;
}


// 状态寄存器1：
// BIT7  6   5   4   3   2   1   0
// SPR   RV  TB BP2 BP1 BP0 WEL BUSY
// SPR:默认0,状态寄存器保护位,配合WP使用
// TB,BP2,BP1,BP0:FLASH区域写保护设置
// WEL:写使能锁定
// BUSY:忙标记位(1,忙;0,空闲)
// 默认:0x00
// 状态寄存器2：
// BIT7  6   5   4   3   2   1   0
// SUS   CMP LB3 LB2 LB1 (R) QE  SRP1
// 状态寄存器3：
// BIT7      6    5    4   3   2   1   0
// HOLD/RST  DRV1 DRV0 (R) (R) WPS ADP ADS

/**
  * @brief  读取W25QXX的状态寄存器，W25QXX一共有3个状态寄存器
  * @note   
  * @param  regno 状态寄存器号，范围:1~3
  * @retval 状态寄存器值
  */
uint8_t W25QXX_ReadSR(uint8_t regno)
{
    uint8_t byte = 0, command = 0;
    switch (regno)
    {
        case 1:
            command = W25X_ReadStatusReg1; // 读状态寄存器1指令
            break;
        case 2:
            command = W25X_ReadStatusReg2; // 读状态寄存器2指令
            break;
        case 3:
            command = W25X_ReadStatusReg3; // 读状态寄存器3指令
            break;
        default:
            command = W25X_ReadStatusReg1;
            break;
    }
    W25QXX_CS = 0;                   // 使能器件
    SPI2_ReadWriteByte(command);     // 发送读取状态寄存器命令
    byte = SPI2_ReadWriteByte(0Xff); // 读取一个字节
    W25QXX_CS = 1;                   // 取消片选
    return byte;
}

/**
  * @brief  写W25QXX状态寄存器
  * @note   
  * @param  regno 状态寄存器号，范围:1~3
  * @param  sr 要写入的值
  * @retval 
  */
void W25QXX_Write_SR(uint8_t regno, uint8_t sr)
{
    uint8_t command = 0;
    switch (regno)
    {
        case 1:
            command = W25X_WriteStatusReg1; // 写状态寄存器1指令
            break;
        case 2:
            command = W25X_WriteStatusReg2; // 写状态寄存器2指令
            break;
        case 3:
            command = W25X_WriteStatusReg3; // 写状态寄存器3指令
            break;
        default:
            command = W25X_WriteStatusReg1;
            break;
    }
    W25QXX_CS = 0;               // 使能器件
    SPI2_ReadWriteByte(command); // 发送写取状态寄存器命令
    SPI2_ReadWriteByte(sr);      // 写入一个字节
    W25QXX_CS = 1;               // 取消片选
}

/**
  * @brief  等待空闲
  * @note   
  * @param  
  * @retval 
  */
void W25QXX_Wait_Busy(void)
{
    while((W25QXX_ReadSR(1) & 0x01) == 0x01); // 等待BUSY位清空
}

/**
  * @brief  W25QXX写使能
  * @note   将WEL置位
  * @param  
  * @retval 
  */
void W25QXX_Write_Enable(void)
{
    W25QXX_CS = 0;                        // 使能器件
    SPI2_ReadWriteByte(W25X_WriteEnable); // 发送写使能
    W25QXX_CS = 1;                        // 取消片选
}

/**
  * @brief  W25QXX写禁止
  * @note   将WEL清零
  * @param  
  * @retval 
  */
void W25QXX_Write_Disable(void)
{
    W25QXX_CS = 0;                         // 使能器件
    SPI2_ReadWriteByte(W25X_WriteDisable); // 发送写禁止指令
    W25QXX_CS = 1;                         // 取消片选
}

/**
  * @brief  SPI在一页(0~65535)内写入少于256个字节的数据
  * @note   在指定地址开始写入最大256字节的数据
  * @param  pBuffer 数据存储区
  * @param  WriteAddr 开始写入的地址(24bit)
  * @param  NumByteToWrite 要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!
  * @retval 
  */
void W25QXX_Write_Page(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
    uint16_t i;
    W25QXX_Write_Enable();                // SET WEL
    W25QXX_CS = 0;                        // 使能器件
    SPI2_ReadWriteByte(W25X_PageProgram); // 发送写页命令
    if (W25QXX_TYPE == W25Q256)           // 如果是W25Q256的话地址为4字节的，要发送最高8位
    {
        SPI2_ReadWriteByte((uint8_t)((WriteAddr) >> 24));
    }
    SPI2_ReadWriteByte((uint8_t)((WriteAddr) >> 16)); // 发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((WriteAddr) >> 8));
    SPI2_ReadWriteByte((uint8_t)WriteAddr);
    for (i = 0; i < NumByteToWrite; i++)
    {
        SPI2_ReadWriteByte(pBuffer[i]); // 循环写数
    }
    W25QXX_CS = 1;                          // 取消片选
    W25QXX_Wait_Busy();                     // 等待写入结束
}

/**
  * @brief  无检验写SPI FLASH
  * @note   必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!具有自动换页功能,在指定地址开始写入指定长度的数据,但是要确保地址不越界!
  * @param  pBuffer:数据存储区
  * @param  WriteAddr:开始写入的地址(24bit)
  * @param  NumByteToWrite:要写入的字节数(最大65535) 
  * @retval 
  */
void W25QXX_Write_NoCheck(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
    uint16_t pageremain;
    pageremain = 256 - WriteAddr % 256; // 单页剩余的字节数
    if (NumByteToWrite <= pageremain)
    {
        pageremain = NumByteToWrite; // 不大于256个字节
    }
    while (1)
    {
        W25QXX_Write_Page(pBuffer, WriteAddr, pageremain);
        if (NumByteToWrite == pageremain)
            break; // 写入结束了
        else       // NumByteToWrite>pageremain
        {
            pBuffer += pageremain;
            WriteAddr += pageremain;

            NumByteToWrite -= pageremain; // 减去已经写入了的字节数
            if (NumByteToWrite > 256)
                pageremain = 256; // 一次可以写入256个字节
            else
                pageremain = NumByteToWrite; // 不够256个字节了
        }
    }
}

/**
  * @brief  读取SPI FLASH
  * @note   在指定地址开始读取指定长度的数据
  * @param  pBuffer 数据存储区
  * @param  ReadAddr 开始读取的地址(24bit)
  * @param  NumByteToRead 要读取的字节数(最大65535)
  * @retval 成功返回0
  */
int8_t W25QXX_Read(uint8_t *pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
    uint16_t i;
    W25QXX_CS = 0;                     // 使能器件
    SPI2_ReadWriteByte(W25X_ReadData); // 发送读取命令
    if (W25QXX_TYPE == W25Q256)        // 如果是W25Q256的话地址为4字节的，要发送最高8位
    {
        SPI2_ReadWriteByte((uint8_t)((ReadAddr) >> 24));
    }
    SPI2_ReadWriteByte((uint8_t)((ReadAddr) >> 16)); // 发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((ReadAddr) >> 8));
    SPI2_ReadWriteByte((uint8_t)ReadAddr);
    for (i = 0; i < NumByteToRead; i++)
    {
        pBuffer[i] = SPI2_ReadWriteByte(0XFF); // 循环读数
    }
    W25QXX_CS = 1;
    return 0;
}

/**
  * @brief  擦除一个扇区
  * @note   擦除一个扇区的最少时间:150ms
  * @param  Dst_Addr 扇区地址 根据实际容量设置
  * @retval 
  */
void W25QXX_Erase_Sector(uint32_t Dst_Addr)
{
    // 监视falsh擦除情况,测试用
    // printf("fe:%x\r\n",Dst_Addr);
    Dst_Addr *= 4096;
    W25QXX_Write_Enable(); // SET WEL
    W25QXX_Wait_Busy();
    W25QXX_CS = 0;                        // 使能器件
    SPI2_ReadWriteByte(W25X_SectorErase); // 发送扇区擦除指令
    if (W25QXX_TYPE == W25Q256)           // 如果是W25Q256的话地址为4字节的，要发送最高8位
    {
        SPI2_ReadWriteByte((uint8_t)((Dst_Addr) >> 24));
    }
    SPI2_ReadWriteByte((uint8_t)((Dst_Addr) >> 16)); // 发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((Dst_Addr) >> 8));
    SPI2_ReadWriteByte((uint8_t)Dst_Addr);
    W25QXX_CS = 1;      // 取消片选
    W25QXX_Wait_Busy(); // 等待擦除完成
}

/**
  * @brief  擦除整个芯片	
  * @note   等待时间超长...
  * @param  
  * @retval 
  */
int8_t W25QXX_Erase_Chip(void)
{
    PRT("Start erasing the whole piece...\r\n");
    W25QXX_Write_Enable(); // SET WEL
    W25QXX_Wait_Busy();
    W25QXX_CS = 0;                      // 使能器件
    SPI2_ReadWriteByte(W25X_ChipErase); // 发送片擦除命令
    W25QXX_CS = 1;                      // 取消片选
    W25QXX_Wait_Busy();                 // 等待芯片擦除结束
    PRT("The entire film has been erased!!!\r\n");
    return 0;
}

/**
  * @brief  进入掉电模式
  * @note   尚未实现us级延时，先用1ms延时
  * @param  
  * @retval 
  */
void W25QXX_PowerDown(void)
{
    W25QXX_CS = 0;                      // 使能器件
    SPI2_ReadWriteByte(W25X_PowerDown); // 发送掉电命令
    W25QXX_CS = 1;                      // 取消片选
    HAL_Delay(1);                       // 等待TPD(3us即可)
}

/**
  * @brief  唤醒
  * @note   尚未实现us级延时，先用1ms延时
  * @param  
  * @retval 
  */
void W25QXX_WakeUP(void)
{
    W25QXX_CS = 0;                             // 使能器件
    SPI2_ReadWriteByte(W25X_ReleasePowerDown); // send W25X_PowerDown command 0xAB
    W25QXX_CS = 1;                             // 取消片选
    HAL_Delay(1);                              // 等待TRES1(3us即可)
}

/**
  * @brief  获取W25QXX型号
  * @note   目前只支持W25Q128
  * @param  
  * @retval 返回0表示W25Q128状态正常，返回-1表示W25Q128不可用
  */
int8_t Get_W25QXX_Type(void)
{
    W25QXX_TYPE = W25QXX_ReadID();
    //printf("SPI FLASH ID=%#x\r\n", W25QXX_TYPE);
    if(W25QXX_TYPE == 0XEF17)
        return 0;
    else
    {
        PRTE("W25QXX_TYPE not is W25Q128!!!\r\n");
        return -1;
    }
}

void W25QXX_Test(void)
{
    const uint8_t TEXT_Buffer[] = {"This is my SPI TEST!!!"};
    uint32_t FLASH_SIZE = 0;
    uint8_t datatemp[32] = {0};
    uint32_t size = sizeof(TEXT_Buffer);

    W25QXX_TYPE = W25QXX_ReadID();
    printf("SPI FLASH ID=%#x\r\n", W25QXX_TYPE);
    FLASH_SIZE = 16 * 1024 * 1024;
    printf("write to SPI FLASH data=%s\r\n", TEXT_Buffer);
    W25QXX_Write((uint8_t *)TEXT_Buffer, FLASH_SIZE - 100, size); // 从倒数第100个地址处开始,写入SIZE长度的数据
    W25QXX_Read(datatemp, FLASH_SIZE - 100, size);                // 从倒数第100个地址处开始,读出SIZE个字节
    printf("read from SPI FLASH data=%s\r\n\r\n", datatemp);
    W25QXX_Erase_Chip();
}
