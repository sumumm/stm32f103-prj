/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : config.h
 * Author     : 上上签
 * Date       : 2023-05-29
 * Version    : 
 * Description: 工程配置文件
 * ======================================================
 */
#ifndef __CONFIG_H__
#define __CONFIG_H__

/* 头文件 */

/* 宏定义 */
// 工程类型，注意下边的只能开放其中之一，因为两种工程将会影响printf和 __attribute__属性的使用
//#define MAKEFILE_PRG   // 是否使用makefile工程
#define MDK_PRG         // MDK工程

/* 结构体定义 */

/* 全局变量声明 */

/*  主要是记录一下定义的大的全局数组空间
    uint8_t USART_RX_BUF[USART_REC_LEN]; //usart.c 接收缓冲,最大USART_REC_LEN个字节.
    uint8_t W25QXX_BUFFER[4096]; // w25qxx.c
    uint16_t internalFLASH_BUF[internalFLASH_SECTOR_SIZE / 2]; // internalFlash.c
    uint8_t sdTxBuffer[512]; // sdCard.c
    uint8_t sdRxBuffer[512]; // sdCard.c
*/

/* 函数声明 */


#endif
