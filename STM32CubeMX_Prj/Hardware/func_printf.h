/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : func_printf.h
 * Author     : 上上签
 * Date       : 2023-06-04
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __FUNC_PRINTF_H__
#define __FUNC_PRINTF_H__

/* 头文件 */
#include <stdio.h>
#include <string.h>
/* 宏定义 */
#define filename(x) (strrchr(x,'/')?(strrchr(x,'/')+1):x)
/* 平时调试使用 */
#define PRTE(fmt...) \
        do \
        { \
            printf("[ERR_LOG][%s:%d][%s]", filename(__FILE__), __LINE__, __FUNCTION__); \
            printf(fmt); \
        } while(0)
#define PRT(fmt...) \
        do \
        { \
            printf("[INF_LOG][%s:%d][%s]", filename(__FILE__), __LINE__, __FUNCTION__); \
            printf(fmt); \
        } while(0)
/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */

int sys_pts_get_ms(void);

#endif /* __FUNC_PRINTF_H__ */
