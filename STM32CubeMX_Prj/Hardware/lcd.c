/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : lcd.c
 * Author     : 上上签
 * Date       : 2023-05-21
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./lcd.h"

_lcd_dev lcd_param = {
    0,
    XY_NORMAL,
    ILI9341_LESS_PIXEL,
    ILI9341_MORE_PIXEL,
    BLACK,
    WHITE,
    &font_8x16,
};

/**
  * @brief  向ILI9341写入命令
  * @note   写寄存器函数
  * @param  usCmd 要写入的命令（表示寄存器地址）
  * @retval 
  */
void ILI9341_Write_Cmd(uint16_t usCmd)
{
    *(__IO uint16_t *)(FSMC_Addr_ILI9341_CMD) = usCmd;
}


/**
  * @brief  向ILI9341写入数据
  * @note   写LCD数据
  * @param  usData 要写入的数据
  * @retval 
  */
void ILI9341_Write_Data(uint16_t usData)
{
    *(__IO uint16_t *)(FSMC_Addr_ILI9341_DATA) = usData;
}

/**
  * @brief  从ILI9341读取数据
  * @note   读LCD数据函数
  * @param  
  * @retval 读取到的数据
  */
uint16_t ILI9341_Read_Data(void)
{
    return (*(__IO uint16_t *)(FSMC_Addr_ILI9341_DATA));
}

/**
  * @brief  读取LCD驱动的ID
  * @note   
  * @param  
  * @retval 
  */
void ILI9341_ID_Read(void)
{
    uint16_t id = 0;
    // 显示一下LCD的命令和数据地址
    //printf("FSMC_Addr_ILI9341_CMD :0x%lX\r\n", FSMC_Addr_ILI9341_CMD);
    //printf("FSMC_Addr_ILI9341_DATA:0x%lX\r\n", FSMC_Addr_ILI9341_DATA);

    ILI9341_Write_Cmd(0xD3);
    id = ILI9341_Read_Data(); // dummy read
    id = ILI9341_Read_Data(); // 读到0X00
    id = ILI9341_Read_Data(); // 读取0X93
    id <<= 8;
    id |= ILI9341_Read_Data(); // 读取0X41
    lcd_param.id = id;
    //printf("LCD driver id:0x%X\r\n", lcd_param.id);
}

/**
  * @brief  ILI9341开启显示
  * @note   会开启LCD的显示
  * @param  
  * @retval 
  */
void ILI9341_DisplayOn(void)
{
    ILI9341_Write_Cmd(0X29); // 开启显示
}

/**
  * @brief  ILI9341关闭显示
  * @note   会关闭LCD的显示
  * @param  
  * @retval 
  */
void ILI9341_DisplayOff(void)
{
    ILI9341_Write_Cmd(0X28); // 关闭显示
}

/**
  * @brief  在ILI9341显示器上开辟一个窗口
  * @note   开辟窗口就相当于准备一块画布，并且确定了这个画布的大小，最大就是240x320
  * @param  usX 在特定扫描方向下窗口的起点X坐标
  * @param  usY 在特定扫描方向下窗口的起点Y坐标
  * @param  usWidth 窗口的宽度
  * @param  usHeight 窗口的高度
  * @retval 
  */
static void ILI9341_OpenWindow(uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight)
{
    ILI9341_Write_Cmd(CMD_SetCoordinateX);          /* 设置X坐标 0x2A */
    ILI9341_Write_Data(usX >> 8);                   /* 起始点的高8位 */
    ILI9341_Write_Data(usX & 0xff);                 /* 起始点的低8位 */
    ILI9341_Write_Data((usX + usWidth - 1) >> 8);   /* 结束点的高8位 */
    ILI9341_Write_Data((usX + usWidth - 1) & 0xff); /* 结束点的低8位 */

    ILI9341_Write_Cmd(CMD_SetCoordinateY); /* 设置Y坐标 0x2B*/
    ILI9341_Write_Data(usY >> 8);
    ILI9341_Write_Data(usY & 0xff);
    ILI9341_Write_Data((usY + usHeight - 1) >> 8);
    ILI9341_Write_Data((usY + usHeight - 1) & 0xff);
}

/**
   * @brief  在ILI9341显示器上以某一颜色填充像素点
   * @note   
   * @param  ulAmout_Point 要填充颜色的像素点的总数目
   * @param  usColor 颜色
   * @retval 
   */
void ILI9341_FillColor(uint32_t ulAmout_Point, uint16_t usColor)
{
    uint32_t i = 0;

    ILI9341_Write_Cmd(CMD_SetPixel); /* memory write 0x2C*/
    for (i = 0; i < ulAmout_Point; i++)
    {
        ILI9341_Write_Data(usColor);
    }
}

/**
  * @brief  设定ILI9341的光标坐标
  * @note   就是确定一个像素点的位置，就相当于确定我们的画笔在画布上的位置，方便后边画点
  * @param  usX 在特定扫描方向下光标的X坐标
  * @param  usY 在特定扫描方向下光标的Y坐标
  * @retval 
  */
void ILI9341_SetCursor(uint16_t usX, uint16_t usY)
{
#if 0
    ILI9341_OpenWindow(usX, usY, 1, 1);
#else
    uint16_t usWidth = 1;
    uint16_t usHeight = 1;

    ILI9341_Write_Cmd(CMD_SetCoordinateX);          /* 设置X坐标 0x2A */
    ILI9341_Write_Data(usX >> 8);                   /* 起始点的高8位 */
    ILI9341_Write_Data(usX & 0xff);                 /* 起始点的低8位 */
    ILI9341_Write_Data((usX + usWidth - 1) >> 8);   /* 结束点的高8位 */
    ILI9341_Write_Data((usX + usWidth - 1) & 0xff); /* 结束点的低8位 */

    ILI9341_Write_Cmd(CMD_SetCoordinateY); /* 设置Y坐标 0x2B*/
    ILI9341_Write_Data(usY >> 8);
    ILI9341_Write_Data(usY & 0xff);
    ILI9341_Write_Data((usY + usHeight - 1) >> 8);
    ILI9341_Write_Data((usY + usHeight - 1) & 0xff);
#endif
}

/**
  * @brief  画一个像素点，对ILI9341显示器的某一点以某种颜色进行填充
  * @note   画一个像素点，相当于我们用画笔在画布的指定位置点上一个点
  *         可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色 
  * @param  usX 在特定扫描方向下该点的X坐标
  * @param  usY 在特定扫描方向下该点的Y坐标  
  * @retval 
  */
void ILI9341_SetPointPixel(uint16_t usX, uint16_t usY)
{
    if ((usX < lcd_param.LCD_X_LENGTH) && (usY < lcd_param.LCD_Y_LENGTH))
    {
        ILI9341_SetCursor(usX, usY);// 设置像素点的位置
        #if 0
        ILI9341_FillColor(1, lcd_param.PonitColor);// 以指定颜色画点
        #else
        uint16_t usColor = lcd_param.PonitColor;
        ILI9341_Write_Cmd(CMD_SetPixel); /* memory write 0x2C*/
        ILI9341_Write_Data(usColor);
        #endif
    }
}

/**
  * @brief  ILI9341快速画一个点，可以指定颜色
  * @note   其实跟上边的画点函数一样，只是这里直接操作寄存器，可能是省去了函数调用的时间吧
  * @param  usX 在特定扫描方向下该点的X坐标
  * @param  usY 在特定扫描方向下该点的Y坐标  
  * @param  usColor 画的点的颜色
  * @retval 
  */
void ILI9341_Fast_DrawPoint(uint16_t usX, uint16_t usY, uint16_t usColor)
{
    if ((usX < lcd_param.LCD_X_LENGTH) && (usY < lcd_param.LCD_Y_LENGTH))
    {
        // 开一个1像素的窗口
        uint16_t usWidth = 1;
        uint16_t usHeight = 1;

        ILI9341_Write_Cmd(CMD_SetCoordinateX);          /* 设置X坐标 0x2A */
        ILI9341_Write_Data(usX >> 8);                   /* 起始点的高8位 */
        ILI9341_Write_Data(usX & 0xff);                 /* 起始点的低8位 */
        ILI9341_Write_Data((usX + usWidth - 1) >> 8);   /* 结束点的高8位 */
        ILI9341_Write_Data((usX + usWidth - 1) & 0xff); /* 结束点的低8位 */

        ILI9341_Write_Cmd(CMD_SetCoordinateY); /* 设置Y坐标 0x2B*/
        ILI9341_Write_Data(usY >> 8);
        ILI9341_Write_Data(usY & 0xff);
        ILI9341_Write_Data((usY + usHeight - 1) >> 8);
        ILI9341_Write_Data((usY + usHeight - 1) & 0xff);
        // 写入写GRAM命令 0x2C
        ILI9341_Write_Cmd(CMD_SetPixel); /* memory write 0x2C*/
        // 以向GRAM写入指定的颜色数据
        ILI9341_Write_Data(usColor);
    }
}

/**
  * @brief  读取LCD电源状态
  * @note   
  * @param  
  * @retval 读取到的状态
  */
uint16_t ILI9341_PowerStatus_Read(void)
{
    uint16_t power_status = 0;

    ILI9341_Write_Cmd(0x0A);
    power_status = ILI9341_Read_Data(); // dummy read
    power_status = ILI9341_Read_Data(); // D[7:2] 0000 0000 1111 1100
    power_status &= 0x00FC;
    printf("power_status:0x%X\r\n", power_status);

    return power_status;
}

/**
  * @brief  ILI9341清屏函数
  * @note   可以以指定颜色对LCD整个屏幕刷屏，但是不会改圈全局参数的背景色和前景色
  * @param  usColor 刷屏要使用的颜色
  * @retval 
  */
void ILI9341_Clear(uint16_t usColor)
{
    uint16_t usWidth = lcd_param.LCD_X_LENGTH;
    uint16_t usHeight = lcd_param.LCD_Y_LENGTH;

    ILI9341_FillColor(usWidth * usHeight, usColor);
}

/***********************************************************
											 LCDID_ILI9341
------------------------------------------------------------
模式0：				 .	模式1：	    .	模式2：			 .	模式3：					
        ^		.					^		.		^					.		^									
        |		.					|		.		|					.		|							
        |		.					|		.		|					.		|					
        0Y	.				 1X		.		2Y				.		3X					
	<--- X0 o		.	<----Y1	o		.		o 2X--->  .		o 3Y--->	
------------------------------------------------------------	
模式4：				 .	模式5：		  .	模式6：			 .	模式7：					
	<--- X4 o		.	<--- Y5 o		.		o 6X--->  .		o 7Y--->	
				 4Y		.				 5X		.		6Y				.		7X	
					|		.					|		.		|					.		|						
					|		.					|		.		|					.		|							
					V		.					V		.		V					.		V		
------------------------------------------------------------		
								      LCDID_ST7789V							
------------------------------------------------------------
模式0：				 .		模式1：		.	模式2：			 .	模式3：					
	o 0X--->  	.		o 1Y--->  .	<--- X2 o		.	<--- Y3 o				
	0Y					.		1X				.				 2Y	  .				 3X			
	|						.		| 				.					|		.					|		
	|						.		|					.					|		.					|			
	V								V					.					V		.					V		
------------------------------------------------------------	
模式4：				 .	模式5：			.	模式6：		   .	模式7：					
	^						.		^					.					^	  .					^	
	|						.		|					.					|	  .					|
	|						.		|					.					|	  .					|				
	4Y					.		5X				.				 6Y	  .				 7X				
	o 4X--->  	.		o 5Y--->  .	<--- 6X o	  .	<--- 7Y o	
---------------------------------------------------------				
											 LCD屏示例
								|-----------------|
								|			        		|
								|									|
								|									|
								|									|
								|									|
								|									|
								|									|
								|									|
								|									|
								|-----------------|
								屏幕正面（宽240，高320）								
 *******************************************************/

/**
  * @brief  设置ILI9341的GRAM的扫描方向
  * @note   坐标图例：o表示(0,0)，^表示向上，V表示向下，<表示向左，>表示向右，X表示X轴，Y表示Y轴 
  * @param  ucOption 选择GRAM的扫描方向 
  *         @arg 0-7,参数可选值为0-7这八个方向
  *  	           ！！！其中0、3、5、6 模式适合从左至右显示文字，
  *				       不推荐使用其它模式显示文字	其它模式显示文字会有镜像效果			
  *	             其中0、2、4、6 模式的X方向像素为240，Y方向像素为320
  *	             其中1、3、5、7 模式下X方向像素为320，Y方向像素为240
  * @retval 
  */
void ILI9341_GramScan(uint8_t ucOption)
{
    // 参数检查，只可输入0-7
    if (ucOption > 7)
    {
        return;
    }

    // 根据模式更新LCD_SCAN_MODE的值，主要用于触摸屏选择计算参数
    lcd_param.LCD_SCAN_MODE = ucOption;

    // 根据模式更新XY方向的像素宽度
    if (ucOption % 2 == 0)
    {
        // 0 2 4 6模式下X方向像素宽度为240，Y方向为320
        lcd_param.LCD_X_LENGTH = ILI9341_LESS_PIXEL;
        lcd_param.LCD_Y_LENGTH = ILI9341_MORE_PIXEL;
    }
    else
    {
        // 1 3 5 7模式下X方向像素宽度为320，Y方向为240
        lcd_param.LCD_X_LENGTH = ILI9341_MORE_PIXEL;
        lcd_param.LCD_Y_LENGTH = ILI9341_LESS_PIXEL;
    }

    // 0x36命令参数的高3位可用于设置GRAM扫描方向
    ILI9341_Write_Cmd(0x36);
    if (lcd_param.id == LCDID_ILI9341)
    {
        ILI9341_Write_Data(0x08 | (ucOption << 5)); // 根据ucOption的值设置LCD参数，共0-7种模式
    }
    else if (lcd_param.id == LCDID_ST7789V)
    {
        ILI9341_Write_Data(0x00 | (ucOption << 5)); // 根据ucOption的值设置LCD参数，共0-7种模式
    }
    ILI9341_Write_Cmd(CMD_SetCoordinateX);
    ILI9341_Write_Data(0x00);                             /* x 起始坐标高8位 */
    ILI9341_Write_Data(0x00);                             /* x 起始坐标低8位 */
    ILI9341_Write_Data(((lcd_param.LCD_X_LENGTH - 1) >> 8) & 0xFF); /* x 结束坐标高8位 */
    ILI9341_Write_Data((lcd_param.LCD_X_LENGTH - 1) & 0xFF);        /* x 结束坐标低8位 */

    ILI9341_Write_Cmd(CMD_SetCoordinateY);
    ILI9341_Write_Data(0x00);                             /* y 起始坐标高8位 */
    ILI9341_Write_Data(0x00);                             /* y 起始坐标低8位 */
    ILI9341_Write_Data(((lcd_param.LCD_Y_LENGTH - 1) >> 8) & 0xFF); /* y 结束坐标高8位 */
    ILI9341_Write_Data((lcd_param.LCD_Y_LENGTH - 1) & 0xFF);        /* y 结束坐标低8位 */

    /* write gram start */
    ILI9341_Write_Cmd(CMD_SetPixel);
}

/**
  * @brief  ILI9341寄存器配置
  * @note   
  * @param  
  * @retval 
  */
void ILI9341_Config(void)
{
    ILI9341_Write_Cmd(0xCF);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0xC1);
    ILI9341_Write_Data(0X30);
    ILI9341_Write_Cmd(0xED);
    ILI9341_Write_Data(0x64);
    ILI9341_Write_Data(0x03);
    ILI9341_Write_Data(0X12);
    ILI9341_Write_Data(0X81);
    ILI9341_Write_Cmd(0xE8);
    ILI9341_Write_Data(0x85);
    ILI9341_Write_Data(0x10);
    ILI9341_Write_Data(0x7A);
    ILI9341_Write_Cmd(0xCB);
    ILI9341_Write_Data(0x39);
    ILI9341_Write_Data(CMD_SetPixel);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x34);
    ILI9341_Write_Data(0x02);
    ILI9341_Write_Cmd(0xF7);
    ILI9341_Write_Data(0x20);
    ILI9341_Write_Cmd(0xEA);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Cmd(0xC0);  // Power control
    ILI9341_Write_Data(0x1B); // VRH[5:0]
    ILI9341_Write_Cmd(0xC1);  // Power control
    ILI9341_Write_Data(0x01); // SAP[2:0];BT[3:0]
    ILI9341_Write_Cmd(0xC5);  // VCM control
    ILI9341_Write_Data(0x30); // 3F
    ILI9341_Write_Data(0x30); // 3C
    ILI9341_Write_Cmd(0xC7);  // VCM control2
    ILI9341_Write_Data(0XB7);
    ILI9341_Write_Cmd(0x36); // Memory Access Control
    ILI9341_Write_Data(0x48);
    ILI9341_Write_Cmd(0x3A);
    ILI9341_Write_Data(0x55);
    ILI9341_Write_Cmd(0xB1);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x1A);
    ILI9341_Write_Cmd(0xB6); // Display Function Control
    ILI9341_Write_Data(0x0A);
    ILI9341_Write_Data(0xA2);
    ILI9341_Write_Cmd(0xF2); // 3Gamma Function Disable
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Cmd(0x26); // Gamma curve selected
    ILI9341_Write_Data(0x01);
    ILI9341_Write_Cmd(0xE0); // Set Gamma
    ILI9341_Write_Data(0x0F);
    ILI9341_Write_Data(CMD_SetCoordinateX);
    ILI9341_Write_Data(0x28);
    ILI9341_Write_Data(0x08);
    ILI9341_Write_Data(0x0E);
    ILI9341_Write_Data(0x08);
    ILI9341_Write_Data(0x54);
    ILI9341_Write_Data(0XA9);
    ILI9341_Write_Data(0x43);
    ILI9341_Write_Data(0x0A);
    ILI9341_Write_Data(0x0F);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Cmd(0XE1); // Set Gamma
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x15);
    ILI9341_Write_Data(0x17);
    ILI9341_Write_Data(0x07);
    ILI9341_Write_Data(0x11);
    ILI9341_Write_Data(0x06);
    ILI9341_Write_Data(CMD_SetCoordinateY);
    ILI9341_Write_Data(0x56);
    ILI9341_Write_Data(0x3C);
    ILI9341_Write_Data(0x05);
    ILI9341_Write_Data(0x10);
    ILI9341_Write_Data(0x0F);
    ILI9341_Write_Data(0x3F);
    ILI9341_Write_Data(0x3F);
    ILI9341_Write_Data(0x0F);
    ILI9341_Write_Cmd(CMD_SetCoordinateY);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x01);
    ILI9341_Write_Data(0x3f);
    ILI9341_Write_Cmd(CMD_SetCoordinateX);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0x00);
    ILI9341_Write_Data(0xef);
    ILI9341_Write_Cmd(0x11); // Exit Sleep
    HAL_Delay(120);
    ILI9341_Write_Cmd(0x29); // display on

    ILI9341_Clear(BLACK);	/* 清屏，显示全黑 */
		
}
//==========================================================================
/**
  * @brief  在 ILI9341 显示器上使用 Bresenham 算法画线段
  * @note   可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色
  * @param  usX1 在特定扫描方向下线段的一个端点X坐标
  * @param  usY1 在特定扫描方向下线段的一个端点Y坐标
  * @param  usX2 在特定扫描方向下线段的另一个端点X坐标
  * @param  usY2 在特定扫描方向下线段的另一个端点Y坐标 
  * @retval 
  */
void LCD_DrawLine(uint16_t usX1, uint16_t usY1, uint16_t usX2, uint16_t usY2)
{
    uint16_t us;
    uint16_t usX_Current, usY_Current;

    int32_t lError_X = 0, lError_Y = 0, lDelta_X, lDelta_Y, lDistance;
    int32_t lIncrease_X, lIncrease_Y;

    lDelta_X = usX2 - usX1; // 计算坐标增量
    lDelta_Y = usY2 - usY1;

    usX_Current = usX1;
    usY_Current = usY1;

    if (lDelta_X > 0)
        lIncrease_X = 1; // 设置单步方向
    else if (lDelta_X == 0)
        lIncrease_X = 0; // 垂直线
    else
    {
        lIncrease_X = -1;
        lDelta_X = -lDelta_X;
    }

    if (lDelta_Y > 0)
        lIncrease_Y = 1;
    else if (lDelta_Y == 0)
        lIncrease_Y = 0; // 水平线
    else
    {
        lIncrease_Y = -1;
        lDelta_Y = -lDelta_Y;
    }

    if (lDelta_X > lDelta_Y)
        lDistance = lDelta_X; // 选取基本增量坐标轴
    else
        lDistance = lDelta_Y;
      
    for (us = 0; us <= lDistance + 1; us++) // 画线输出
    {
        ILI9341_SetPointPixel(usX_Current, usY_Current); // 画点

        lError_X += lDelta_X;
        lError_Y += lDelta_Y;

        if (lError_X > lDistance)
        {
          lError_X -= lDistance;
          usX_Current += lIncrease_X;
        }

        if (lError_Y > lDistance)
        {
          lError_Y -= lDistance;
          usY_Current += lIncrease_Y;
        }
    }
}

/**
  * @brief  用指定颜色团宠一个矩形区域
  * @note   可以用来对某个区域刷屏
  * @param  usX 在特定扫描方向下窗口的起点X坐标
  * @param  usY 在特定扫描方向下窗口的起点Y坐标
  * @param  usWidth 窗口的宽度
  * @param  usHeight 窗口的高度
  * @retval 
  */
void LCD_Fill(uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight, uint16_t usColor)
{
    ILI9341_OpenWindow(usX, usY, usWidth, usHeight);
    ILI9341_FillColor(usWidth * usHeight, usColor);
}

/**
  * @brief  在 ILI9341 显示器上画一个矩形
  * @note   可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色
  * @param  usX_Start 在特定扫描方向下矩形的起始点X坐标
  * @param  usY_Start 在特定扫描方向下矩形的起始点Y坐标
  * @param  usWidth 矩形的宽度（单位：像素）
  * @param  usHeight 矩形的高度（单位：像素）
  * @param  ucFilled 选择是否填充该矩形,该参数为以下值之一：
  *             @arg 0,空心矩形
  *             @arg 1,实心矩形
  * @retval 
  */
void LCD_DrawRectangle(uint16_t usX_Start, uint16_t usY_Start, uint16_t usWidth, uint16_t usHeight, uint8_t ucFilled)
{
    if (ucFilled)
    {
        ILI9341_OpenWindow(usX_Start, usY_Start, usWidth, usHeight);
        ILI9341_FillColor(usWidth * usHeight, lcd_param.BackColor);
    }
    else
    {
        LCD_DrawLine(usX_Start, usY_Start, usX_Start + usWidth - 1, usY_Start);
        LCD_DrawLine(usX_Start, usY_Start + usHeight - 1, usX_Start + usWidth - 1, usY_Start + usHeight - 1);
        LCD_DrawLine(usX_Start, usY_Start, usX_Start, usY_Start + usHeight - 1);
        LCD_DrawLine(usX_Start + usWidth - 1, usY_Start, usX_Start + usWidth - 1, usY_Start + usHeight - 1);
    }
}

/**
  * @brief  在 ILI9341 显示器上使用 Bresenham 算法画圆
  * @note   可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色
  * @param  usX_Center ：在特定扫描方向下圆心的X坐标
  * @param  usY_Center ：在特定扫描方向下圆心的Y坐标
  * @param  usRadius：圆的半径（单位：像素）
  * @param  ucFilled ：选择是否填充该圆,该参数为以下值之一：
  *         @arg 0,空心圆
  *         @arg 1,实心圆 
  * @retval 
  */
void LCD_DrawCircle(uint16_t usX_Center, uint16_t usY_Center, uint16_t usRadius, uint8_t ucFilled)
{
    int16_t sCurrentX, sCurrentY;
    int16_t sError;

    sCurrentX = 0;
    sCurrentY = usRadius;

    sError = 3 - (usRadius << 1); // 判断下个点位置的标志

    while (sCurrentX <= sCurrentY)
    {
        int16_t sCountY;

        if (ucFilled)
          for (sCountY = sCurrentX; sCountY <= sCurrentY; sCountY++)
          {
            ILI9341_SetPointPixel(usX_Center + sCurrentX, usY_Center + sCountY); // 1，研究对象
            ILI9341_SetPointPixel(usX_Center - sCurrentX, usY_Center + sCountY); // 2
            ILI9341_SetPointPixel(usX_Center - sCountY, usY_Center + sCurrentX); // 3
            ILI9341_SetPointPixel(usX_Center - sCountY, usY_Center - sCurrentX); // 4
            ILI9341_SetPointPixel(usX_Center - sCurrentX, usY_Center - sCountY); // 5
            ILI9341_SetPointPixel(usX_Center + sCurrentX, usY_Center - sCountY); // 6
            ILI9341_SetPointPixel(usX_Center + sCountY, usY_Center - sCurrentX); // 7
            ILI9341_SetPointPixel(usX_Center + sCountY, usY_Center + sCurrentX); // 0
          }

        else
        {
          ILI9341_SetPointPixel(usX_Center + sCurrentX, usY_Center + sCurrentY); // 1，研究对象
          ILI9341_SetPointPixel(usX_Center - sCurrentX, usY_Center + sCurrentY); // 2
          ILI9341_SetPointPixel(usX_Center - sCurrentY, usY_Center + sCurrentX); // 3
          ILI9341_SetPointPixel(usX_Center - sCurrentY, usY_Center - sCurrentX); // 4
          ILI9341_SetPointPixel(usX_Center - sCurrentX, usY_Center - sCurrentY); // 5
          ILI9341_SetPointPixel(usX_Center + sCurrentX, usY_Center - sCurrentY); // 6
          ILI9341_SetPointPixel(usX_Center + sCurrentY, usY_Center - sCurrentX); // 7
          ILI9341_SetPointPixel(usX_Center + sCurrentY, usY_Center + sCurrentX); // 0
        }

        sCurrentX++;

        if (sError < 0)
          sError += 4 * sCurrentX + 6;

        else
        {
          sError += 10 + 4 * (sCurrentX - sCurrentY);
          sCurrentY--;
        }
    }
}

/**
  * @brief  设置LCD的背景颜色,RGB565
  * @note   
  * @param  Color 指定背景颜色，RGB565
  * @retval 
  */
void LCD_SetBackColor(uint16_t Color)
{
    lcd_param.BackColor = Color;
}

/**
  * @brief  设置LCD的前景(字体)颜色,RGB565
  * @note   
  * @param  Color 指定前景(字体)颜色，RGB565
  * @retval 
  */
void LCD_SetTextColor(uint16_t Color)
{
    lcd_param.PonitColor = Color;
}

/**
  * @brief  设置LCD的前景(字体颜色)和背景色
  * @note   相当于是对上边两个函数进行封装
  * @param  TextColor 指定前景(字体)颜色，RGB565
  * @param  BackColor 指定背景颜色，RGB565
  * @retval 
  */
void LCD_SetColors(uint16_t TextColor, uint16_t BackColor)
{
    lcd_param.PonitColor = TextColor;
    lcd_param.BackColor = BackColor;
}

//==========================================================================
/**
  * @brief  设置英文字体大小
  * @note   
  * @param  fonts: 指定要选择的字体, 参数为以下值之一
  * 	    @arg font_24x32
  * 	    @arg font_16x24
  * 	    @arg font_8x16
  * @retval 
  */
void LCD_SetFontSize(FONT_PARAM *fonts)
{
    lcd_param.LCD_Currentfonts = fonts;
}

/**
  * @brief  求m^n函数
  * @note   
  * @param  m 底数
  * @param  n 幂
  * @retval m^n次方
  */
uint32_t LCD_Pow(uint8_t m, uint8_t n)
{
    uint32_t result = 1;

    while (n--)
        result *= m;

    return result;
}

/**
  * @brief  在 LCD 上显示一个英文字符
  * @note   可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色
  * @param  usX 在特定扫描方向下字符的起始X坐标
  * @param  usY 在特定扫描方向下该点的起始Y坐标
  * @param  cChar 要显示的英文字符 
  * @retval 
  */
void LCD_ShowChar(uint16_t usX, uint16_t usY, const char cChar)
{
    uint8_t byteCount, bitCount, fontLength;
    uint16_t ucRelativePositon;
    uint8_t *Pfont;

    // 对ascii码表偏移（字模表不包含ASCII表的前32个非图形符号）
    ucRelativePositon = cChar - ' ';

    // 每个字模的字节数
    fontLength = (lcd_param.LCD_Currentfonts->Width * lcd_param.LCD_Currentfonts->Height) / 8;

    // 字模首地址
    /*ascii码表偏移值乘以每个字模的字节数，求出字模的偏移位置*/
    Pfont = (uint8_t *)&(lcd_param.LCD_Currentfonts->table[ucRelativePositon * fontLength]);

    // 设置显示窗口
    ILI9341_OpenWindow(usX, usY, lcd_param.LCD_Currentfonts->Width, lcd_param.LCD_Currentfonts->Height);

    ILI9341_Write_Cmd(CMD_SetPixel);

    // 按字节读取字模数据
    // 由于前面直接设置了显示窗口，显示数据会自动换行
    for (byteCount = 0; byteCount < fontLength; byteCount++)
    {
        // 一位一位处理要显示的颜色
        for (bitCount = 0; bitCount < 8; bitCount++)
        {
            if (Pfont[byteCount] & (0x80 >> bitCount))
                ILI9341_Write_Data(lcd_param.PonitColor);
            else
                ILI9341_Write_Data(lcd_param.BackColor);
        }
    }
}

/**
  * @brief  在 LCD 上显示英文字符串
  * @note   可使用LCD_SetBackColor、LCD_SetTextColor、LCD_SetColors函数设置颜色,可以自动换行
  * @param  usX 在特定扫描方向下字符的起始X坐标
  * @param  usY 在特定扫描方向下字符的起始Y坐标
  * @param  pStr 要显示的英文字符串的首地址 
  * @retval 
  */
void LCD_ShowString(uint16_t usX, uint16_t usY, char *pStr)
{
    while (*pStr != '\0')
    {
        if ((usX - ILI9341_DispWindow_X_Star + lcd_param.LCD_Currentfonts->Width) > lcd_param.LCD_X_LENGTH)
        {
            usX = ILI9341_DispWindow_X_Star;
            usY += lcd_param.LCD_Currentfonts->Height;
        }

        if ((usY - ILI9341_DispWindow_Y_Star + lcd_param.LCD_Currentfonts->Height) > lcd_param.LCD_Y_LENGTH)
        {
            usX = ILI9341_DispWindow_X_Star;
            usY = ILI9341_DispWindow_Y_Star;
        }

        LCD_ShowChar(usX, usY, *pStr);

        pStr++;

        usX += lcd_param.LCD_Currentfonts->Width;
    }
}

/**
  * @brief  显示数字,高位为0,则不显示
  * @note   
  * @param  usX 在特定扫描方向下字符的起始X坐标
  * @param  usY 在特定扫描方向下字符的起始Y坐标 
  * @param  num 数值(0~4294967295)
  * @param  len 数字的位数
  * @retval 
  */
void LCD_ShowNum(uint16_t usX, uint16_t usY, uint32_t num, uint8_t len)
{
    uint8_t t, temp;
    uint8_t enshow = 0;
    uint8_t size = lcd_param.LCD_Currentfonts->Height;
    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                LCD_ShowChar(usX + (size / 2) * t, usY, ' ');
                continue;
            }
            else
                enshow = 1;
        }

        LCD_ShowChar(usX + (size / 2) * t, usY, temp + '0');
    }
}

/**
  * @brief  显示数字,高位为0,可选择显示或者不显示
  * @note   
  * @param  usX 在特定扫描方向下字符的起始X坐标
  * @param  usY 在特定扫描方向下字符的起始Y坐标 
  * @param  num 数值(0~999999999)
  * @param  len 数字的位数
  * @param  mode bit[7]:0,不填充;1,填充0.bit[6:0]:保留
  * @retval 
  */
void LCD_ShowxNum(uint16_t usX, uint16_t usY, uint32_t num, uint8_t len, uint8_t mode)
{
    uint8_t t, temp;
    uint8_t enshow = 0;
    uint8_t size = lcd_param.LCD_Currentfonts->Height;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                if (mode & 0X80)
                    LCD_ShowChar(usX+(size/2)*t, usY, '0');
                else
                    LCD_ShowChar(usX+(size/2)*t, usY, ' ');

                continue;
            }
            else
                enshow = 1;
        }

        LCD_ShowChar(usX+(size/2)*t, usY, temp + '0');
    }
}

/**
  * @brief  显示汉字
  * @note   使用的字体大小跟英文一样
  * @param  usX 第几列
  * @param  usY 第几行 
  * @param  fontChar 汉字GBK码
  * @retval 
  */
void LCD_ShowChar_CH(uint16_t usX, uint16_t usY, char *fontChar)
{
    uint8_t temp, t, t1;
    uint16_t y0 = usY;
    uint8_t fontData[72]; // 字模数据缓冲区
    uint8_t size = lcd_param.LCD_Currentfonts->Height;// 大小与高度保持一致
    uint8_t csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size); // 得到字体一个字符对应点阵集所占的字节数
    if (size != 12 && size != 16 && size != 24) return; // 不支持的size
    func_Get_HzMat(fontChar, fontData, size); // 得到相应大小的点阵数据
    for (t = 0; t < csize; t++)
    {
        temp = fontData[t]; // 得到点阵数据
        for (t1 = 0; t1 < 8; t1++)
        {
            if (temp & (0x80 >> t1))
            {
                ILI9341_Fast_DrawPoint(usX, usY, lcd_param.PonitColor);
                //printf("*");// 检测获取的字模数据用，这样打印出来是旋转了90度的状态
            }
            else
            {
                ILI9341_Fast_DrawPoint(usX, usY, lcd_param.BackColor);
                //printf(" ");// 检测获取的字模数据用，这样打印出来是旋转了90度的状态
            }
            usY++;
            if ((usY - y0) == size)// 用于换行
            {
                usY = y0;
                usX++;
                //printf("\r\n");// 检测获取的字模数据用，这样打印出来是旋转了90度的状态
                break;
            }
        }
    }
}

/**
  * @brief  在指定位置开始显示一个字符串
  * @note   可以中英文混合显示，支持自动换行
  * @param  usX 第几列
  * @param  usY 第几行
  * @param  str 字符串
  * @retval 
  */
void LCD_Show_Str_ENCH(uint16_t usX, uint16_t usY, char *str)
{
    uint16_t x0 = usX;
    uint8_t size = lcd_param.LCD_Currentfonts->Height;
    uint8_t bHz = 0;  // 字符或者中文
    while (*str != 0) // 数据未结束
    {
        if (!bHz)
        {
            if (*str > 0x80)
                bHz = 1; // 中文
            else         // 字符
            {
                if (usX > (x0 + lcd_param.LCD_X_LENGTH - size / 2)) // 换行
                {
                    usY += size;
                    usX = x0;
                }
                if (*str == 0x0D) // 换行符号
                {
                    usY += size;
                    usX = x0;
                    str++;
                }
                else
                    LCD_ShowChar(usX, usY, *str); // 有效部分写入
                str++;
                usX += size / 2; // 字符,为全字的一半
            }
        }
        else // 中文
        {
            bHz = 0;                       // 有汉字库
            if (usX > (x0 + lcd_param.LCD_X_LENGTH - size)) // 换行
            {
                usY += size;
                usX = x0;
            }
            LCD_ShowChar_CH(usX, usY, str); // 显示这个汉字,空心显示
            str += 2;
            usX += size; // 下一个汉字偏移
        }
    }
}

/**
  * @brief  获取当前LCD的各项参数
  * @note   
  * @param  
  * @retval 
  */
void LCD_Show_Info(void)
{
    printf("FSMC_Addr_ILI9341_CMD  : 0x%lX\r\n", FSMC_Addr_ILI9341_CMD);
    printf("FSMC_Addr_ILI9341_DATA : 0x%lX\r\n", FSMC_Addr_ILI9341_DATA);
    printf("lcd_param.id           : 0x%X\r\n", lcd_param.id);
    printf("lcd_param.LCD_X_LENGTH : %d(lcd width)\r\n", lcd_param.LCD_X_LENGTH);
    printf("lcd_param.LCD_Y_LENGTH : %d(lcd height)\r\n", lcd_param.LCD_Y_LENGTH);
    printf("lcd_param.PonitColor   : 0x%X\r\n", lcd_param.PonitColor);
    printf("lcd_param.BackColor    : 0x%X\r\n", lcd_param.BackColor);
    printf("lcd_param.LCD_SCAN_MODE: %d\r\n", lcd_param.LCD_SCAN_MODE);
    printf("lcd_param font info    : %dx%d\r\n", lcd_param.LCD_Currentfonts->Width, lcd_param.LCD_Currentfonts->Height);
}

void LCD_init(void)
{
    // 读取id
    ILI9341_ID_Read();
    ILI9341_Config();
    lcd_param.LCD_SCAN_MODE = XY_NORMAL;
    ILI9341_GramScan(lcd_param.LCD_SCAN_MODE);
    ILI9341_Clear(BLACK);

    LCD_SetColors(WHITE, BLACK);
}

void LCD_Func_Test(void)
{
    LCD_init();
    LCD_Show_Info();
    ILI9341_Clear(BLACK);
    HAL_Delay(500);
    ILI9341_Clear(RED);
    HAL_Delay(500);
    ILI9341_Clear(BLUE);
    HAL_Delay(500);
    ILI9341_Clear(GREEN);
    uint16_t i = 0;
    for(i = 0;i<240;i++)
        ILI9341_Fast_DrawPoint(i, 100, RED);
    LCD_Show_Info();

    LCD_ShowxNum(0, 0, 345, 5, 0x80);
    LCD_ShowxNum(0, 16, 345, 5, 0x00);

    func_font_init();
    LCD_Show_Str_ENCH(0,100,"我的STM32F103ZET6我的STM32F103ZET6我的STM32F103ZET6");
}
