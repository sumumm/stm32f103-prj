/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sys_init.c
 * Author     : 上上签
 * Date       : 2023-06-09
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./sys_init.h"

/**
  * @brief  系统初始化
  * @note   
  * @param  
  * @retval 
  */
void system_init(void)
{
    uint16_t ypos = 0; // 信息起始行
    uint16_t xpos = 0; // 开始显示的列
    uint16_t okoffset = 0;
    uint16_t j = 0;    // 记录行数
    uint8_t fsize = 0; // LCD字体高度
    uint16_t temp = 0;
    int8_t res = 0;
    uint32_t dtsize, dfsize;

REINIT://重新初始化
    j = 0;
    xpos = 5;
    ypos = 0;
    okoffset = 200;

    // LCD初始化
    PRT("This is my stm32f103ze test!!!\r\n");
    LCD_init();
    fsize = lcd_param.LCD_Currentfonts->Height;
    LCD_ShowString(xpos, ypos+fsize*j++, "This is My stm32f103ze test!");
    LED0=0;LED1=0;		 // 同时点亮两个LED
    LCD_ShowString(xpos, ypos+fsize*j++, "CPU:STM32F103ZET6 72Mhz");
    LCD_ShowString(xpos, ypos+fsize*j++, "FLASH:512KB   SRAM:64KB");	
		// 外部SRAM检测
    if(func_exsram_test(xpos, ypos+fsize*j) != 0)
		    system_error_show(xpos, ypos+fsize*j++, "EX Memory Error!");
    LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK");
		pub_mem_init_all();// 初始化内存池
		// 外部SPI FLASH检测与擦除
		if(Get_W25QXX_Type() != 0)
        system_error_show(xpos, ypos+fsize*j++, "EX Flash Error!");
    else
        temp = 16*1024;
    LCD_ShowString(xpos, ypos+fsize*j, "Ex Flash:     KB");			   
	  LCD_ShowNum(xpos+9*(fsize/2), ypos+fsize*j, temp, 5);//显示flash大小  
	  LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK");
    res = KEY_Scan(1);
    if (res == KEY2_PRES)
    {
        res = system_files_erase(5, ypos + fsize * j);
        if (res != 0)
          goto REINIT;
    }
    // 文件系统挂载
    func_fatfs_init(); /* 为fatfs相关变量申请内存 */
    LCD_ShowString(xpos, ypos + fsize * j, "FatFs check...");
    f_mount(gFatfsParam.fs[0], "0:", 1); // 挂载SD卡
    f_mount(gFatfsParam.fs[1], "1:", 1); // 挂载挂载FLASH.
    LCD_ShowString(xpos+okoffset, ypos + fsize * j++, "OK");
    // SD卡检测,SD卡一般不需要格式化，因为我们在电脑用的时候就会直接格式化好
    LCD_ShowString(xpos, ypos+fsize*j, "SD Card:     MB"); // FATFS检测
    temp = 0;
    do
    {
        temp++;
        res = func_fatfs_getfree("0:", &dtsize, &dfsize); // 得到SD卡剩余容量和总容量
        HAL_Delay(200);
    } while (res && temp < 5); // 连续检测5次
    if (res != 0)              // 得到容量正常
    {
        temp = 0;
        LCD_ShowNum(xpos+8*(fsize/2), ypos+fsize*j, temp, 5);                                  // 显示SD卡容量大小
        LCD_ShowString(xpos+okoffset-24, ypos+fsize*j++, "ERROR"); // SD卡状态
    }
    else
    {
        temp = dtsize >> 10; // 单位转换为MB
        LCD_ShowNum(xpos+8*(fsize/2), ypos+fsize*j, temp, 5);                                  // 显示SD卡容量大小
        LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK"); // SD卡状态
    }
    // W25Q128检测,如果不存在文件系统,则先创建.
    temp = 0;
    do
    {
        temp++;
        res = func_fatfs_getfree("1:", &dtsize, &dfsize); // 得到FLASH剩余容量和总容量
        HAL_Delay(200);
    } while (res && temp < 20); // 连续检测20次
    if (res == 0X0D)            // 文件系统不存在
    {
        LCD_ShowString(xpos, ypos+fsize*j, "Flash Disk Formatting..."); // 格式化FLASH
        res = f_mkfs("1:", 0, 0, FF_MAX_SS);                                                                         // 格式化FLASH,1,盘符;1,不需要引导区,8个扇区为1个簇
        if (res == 0)
        {
          f_setlabel((const TCHAR *)"1:SPIFLASH");// 设置Flash磁盘的名字
          LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK"); // 标志格式化成功
          res = func_fatfs_getfree("1:", &dtsize, &dfsize);// 重新获取容量
        }
    }
    if (res == 0) // 得到FLASH卡剩余容量和总容量
    {
        LCD_Fill(0, ypos + fsize * j, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(xpos, ypos + fsize * j, "Flash Disk:     KB"); // FATFS检测
        temp = dtsize;
    }
    else
        system_error_show(xpos, ypos+fsize*(j+1), "Flash Fat Error!");                // flash 文件系统错误
    LCD_ShowNum(xpos+11*(fsize/2), ypos+fsize*j, temp, 5);// 显示FLASH容量大小
    LCD_ShowString(xpos + okoffset, ypos+fsize*j++, "OK"); // FLASH卡状态
    //字库检测									    
   	LCD_ShowString(xpos, ypos+fsize*j, "Font Check...");
    res = KEY_Scan(1);//检测按键
    if(res == KEY1_PRES)//更新？确认
    {
        res = system_font_update_confirm(xpos, ypos+fsize*(j+1));
    }
    else 
        res = 0;
    if (func_font_init() || (res == 1)) // 检测字体,如果字体不存在/强制更新,则更新字库
    {
        res = 0;                                                // 按键无效
        if (func_update_font(xpos, ypos+fsize*(j+1), "0:") != 0) // 从SD卡更新
            LCD_ShowString(xpos+okoffset-24, ypos+fsize*j++, "ERROR"); // SD卡状态  
        else
				{
            LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK"); // 字库检测OK
						LCD_Show_Str_ENCH(xpos, ypos+fsize*j++, "现在可以使用中文啦!!!");
				}
				
    }
    else
		{
        LCD_ShowString(xpos+okoffset, ypos+fsize*j++, "OK"); // 字库检测OK
				LCD_Show_Str_ENCH(xpos, ypos+fsize*j++, "现在可以使用中文啦!!!");
		} 
}

/**
  * @brief  显示错误信息
  * @note   
  * @param  x 第几列
  * @param  y 第几行
  * @param  err 错误信息
  * @retval 
  */
void system_error_show(uint16_t x, uint16_t y, char *err)
{
    LCD_SetTextColor(RED);
    while (1)
    {
        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, err);
        HAL_Delay(500);
        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        HAL_Delay(100);
        LED0 = !LED0;
    }
}

/**
  * @brief  擦除整个SPI FLASH(即所有资源都删除),以快速更新系统.
  * @note   
  * @param  x 第几列
  * @param  y 第几行
  * @param  err 错误信息
  * @retval 0,没有擦除;1,擦除了
  */
uint8_t system_files_erase(uint16_t x, uint16_t y)
{
    uint8_t key;
    uint8_t t = 0;
    LCD_SetTextColor(RED);
    LCD_ShowString(x, y, "Erase all system files?");
    while (1)
    {
        t++;
        if (t == 20)
          LCD_ShowString(x, y + lcd_param.LCD_Currentfonts->Height, "KEY0:NO / KEY2:YES");
        if (t == 40)
        {
          LCD_Fill(x, y+lcd_param.LCD_Currentfonts->Height, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
          t = 0;
          LED0 = !LED0;
        }

        key = KEY_Scan(0);
        if (key == KEY0_PRES) // 不擦除,用户取消了
        {
            LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
            LCD_Fill(x, y+lcd_param.LCD_Currentfonts->Height, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
            LCD_SetTextColor(WHITE);
            LED0 = 1;
            return 0;
        }
        else if (key == KEY2_PRES) // 要擦除,要重新来过
        {
            LED0 = 1;
            LCD_ShowString(x, y+lcd_param.LCD_Currentfonts->Height, "Erasing SPI FLASH...");
            W25QXX_Erase_Chip();
            LCD_ShowString(x, y+lcd_param.LCD_Currentfonts->Height, "Erasing SPI FLASH OK");
            HAL_Delay(600);
            return 1;
        }
        HAL_Delay(10);
    }
}

/**
  * @brief  字库更新确认提示.
  * @note   
  * @param  x 第几列
  * @param  y 第几行 
  * @retval 0,不需要更新;1,确认要更新
  */
uint8_t system_font_update_confirm(uint16_t x, uint16_t y)
{
    uint8_t key;
    uint8_t t = 0;
    uint8_t res = 0;
    LCD_SetTextColor(RED);
    LCD_ShowString(x, y, "Update font?");
    while (1)
    {
        t++;
        if (t == 20)
            LCD_ShowString(x, y+lcd_param.LCD_Currentfonts->Height, "KEY0:NO / KEY2:YES");
        if (t == 40)
        {
            LCD_Fill(x, y+lcd_param.LCD_Currentfonts->Height, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor); // 清除显示
            t = 0;
            LED0 = !LED0;
        }
        key = KEY_Scan(0);
        if (key == KEY0_PRES)
            break; // 不更新
        else if (key == KEY2_PRES)
        {
            res = 1;
            break;
        } // 要更新
        HAL_Delay(10);
    }
    LED0 = 1;
    LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor); // 清除显示
    LCD_Fill(x, y+lcd_param.LCD_Currentfonts->Height, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor); // 清除显示
    LCD_SetTextColor(WHITE);
    return res;
}
