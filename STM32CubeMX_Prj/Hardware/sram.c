/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sram.c
 * Author     : 上上签
 * Date       : 2023-05-21
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./sram.h"

/**
  * @brief  在指定地址(WriteAddr+Bank1_SRAM3_ADDR)开始,连续写入n个字节.
  * @note   按字节写入
  * @param  pBuffer 字节指针
  * @param  WriteAddr 要写入的地址
  * @param  n 要写入的字节数
  * @retval 
  */
void FSMC_SRAM_WriteBuffer(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t n)
{
    for(; n != 0; n--)
    {
        *(volatile uint8_t *)(Bank1_SRAM3_ADDR + WriteAddr) = *pBuffer;
        WriteAddr++;
        pBuffer++;
    }
}

/**
  * @brief  从指定地址((WriteAddr+Bank1_SRAM3_ADDR))开始,连续读出n个字节.
  * @note   按字节读取
  * @param  pBuffer 字节指针
  * @param  WriteAddr 要读取的地址
  * @param  n 要读取的字节数
  * @retval 
  */
void FSMC_SRAM_ReadBuffer(uint8_t *pBuffer, uint32_t ReadAddr, uint32_t n)
{
    for(; n != 0; n--)
    {
        *pBuffer++ = *(volatile uint8_t *)(Bank1_SRAM3_ADDR + ReadAddr);
        ReadAddr++;
    }
}

/**
  * @brief  8位数据读写测试
  * @note   
  * @param  
  * @retval 
  */
int rw_8bit_test(void)
{
    uint32_t i = 0;
    uint8_t ubWritedata_8b = 0, ubReaddata_8b = 0;  
    /* 向整个SRAM写入数据  8位 */
    for (i = 0; i < IS62WV51216_SIZE; i++)
    {
        *(volatile uint8_t*) (Bank1_SRAM3_ADDR + i) = (uint8_t)(ubWritedata_8b + i);
    }

    /* 读取 SRAM 数据并检测*/
    for(i = 0; i < IS62WV51216_SIZE; i++ )
    {
        ubReaddata_8b = *(volatile uint8_t*)(Bank1_SRAM3_ADDR + i);  //从该地址读出数据

        if(ubReaddata_8b != (uint8_t)(ubWritedata_8b + i))      //检测数据，若不相等，跳出函数,返回检测失败结果。
        {
            printf("The 8-bit data read/write error!!!\r\n");
            return -1;
        }
    }
	printf("The 8-bit data read/write test succeeds!!!\r\n");
	return 0;
}

/**
  * @brief  16位数据读写测试
  * @note   
  * @param  
  * @retval 
  */
int rw_16bit_test(void)
{
    uint32_t i = 0;
    uint16_t ubWritedata_16b = 0, ubReaddata_16b = 0;  
    /* 向整个SRAM写入数据  8位 */
    for (i = 0; i < IS62WV51216_SIZE/2; i++)
    {
        *(volatile uint16_t*) (Bank1_SRAM3_ADDR + i*2) = (uint16_t)(ubWritedata_16b + i);
    }

    /* 读取 SRAM 数据并检测*/
    for(i = 0; i < IS62WV51216_SIZE/2; i++ )
    {
        ubReaddata_16b = *(volatile uint16_t*)(Bank1_SRAM3_ADDR + i*2);  //从该地址读出数据

        if(ubReaddata_16b != (uint16_t)(ubWritedata_16b + i))      //检测数据，若不相等，跳出函数,返回检测失败结果。
        {
            printf("The 16-bit data read/write error!!!\r\n");
            return -1;
        }
    }
	printf("The 16-bit data read/write test succeeds!!!\r\n");
	return 0;
}

/**
  * @brief  按字节连续写入和读取测试
  * @note   
  * @param  
  * @retval 
  */
void rw_nByte_test(void)
{
    uint32_t i = 0;
    uint8_t temp[32] = "This is my sram test!!!";
    uint8_t read[32] = {0};

    FSMC_SRAM_WriteBuffer(temp, 0, sizeof(temp));
    for(i = 0; i < 32; i++)
    {
        if(i % 2 == 0)
        {
            printf("\r\n0x%p:", (uint8_t*)(Bank1_SRAM3_ADDR + i));
        }
        printf("%c ", *(volatile uint8_t*)(Bank1_SRAM3_ADDR + i)); //显示测试数据
    }
    printf("\r\n\r\n");

    FSMC_SRAM_ReadBuffer(read, 0, sizeof(read));
    printf("read data:%s\r\n", read);
}

/**
  * @brief  外部内存测试(最大支持1M字节内存测试)
  * @note   在LCD指定的位置显示外部SRAM的容量
  * @param  x 信息显示在第几行
  * @param  y 信息显示在第几列
  * @retval 0,成功;-1,失败.
  */
int8_t func_exsram_test(uint16_t x, uint16_t y)
{
    uint32_t i = 0;
    uint16_t temp = 0;
    uint16_t sval = 0; // 在地址0读到的数据
    LCD_ShowString(x, y, "Ex Memory Test:   0KB");
    // 每隔1K字节,写入一个数据,总共写入1024个数据,刚好是1M字节
    for (i = 0; i < 1024 * 1024; i += 1024)
    {
        FSMC_SRAM_WriteBuffer((uint8_t *)&temp, i, 2);
        temp++;
    }
    // 依次读出之前写入的数据,进行校验
    for (i = 0; i < 1024 * 1024; i += 1024)
    {
        FSMC_SRAM_ReadBuffer((uint8_t *)&temp, i, 2);
        if (i == 0)
        {
            sval = temp;
        }
        else if (temp <= sval)
            break;                                                                      // 后面读出的数据一定要比第一次读到的数据大.
        LCD_ShowNum(x + 15*lcd_param.LCD_Currentfonts->Width, y, (uint16_t)(temp - sval + 1), 4); // 显示内存容量
		//HAL_Delay(1);// 延时一下，看上去测试的慢一些，也可以去掉
	}
    if (i >= 1024 * 1024)
    {
        LCD_ShowNum(x + 15*lcd_param.LCD_Currentfonts->Width, y, i / 1024, 4); // 显示内存值
        return 0;                                                     // 内存正常,成功
    }
    return -1; // 失败
}

#if PrestoredTest == 1
#ifdef MDK_PRG
uint8_t testSram[500] __attribute__((at(0X68000000)));// 测试用数组 MDK中用这个格式
#endif
#ifdef MAKEFILE_PRG
uint8_t testSram[500] __attribute__((section(".sram")));// 测试用数组
#endif
void SRAM_Test(void)
{
    uint32_t i = 0;

    for(i = 0; i < 10; i++)
    {
        testSram[i] = i; //预存测试数据
        printf("%p testSram[%ld]:%d\r\n", &testSram[i], i, testSram[i]); // 显示测试数据
    }
    printf("\r\n\r\n");

    for(i = 0; i < 10; i++)
    {
        if(i % 2 == 0)
        {
            printf("\r\n%p:", (uint8_t*)(Bank1_SRAM3_ADDR + i));
        }
        printf("%d ", *(volatile uint8_t*)(Bank1_SRAM3_ADDR + i)); //显示测试数据
    }
    printf("\r\n\r\n");
}
#endif
