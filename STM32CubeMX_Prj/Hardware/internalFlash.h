/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : internalFlash.h
 * Author     : 上上签
 * Date       : 2023-05-30
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __INTERNALFLASH_H__
#define __INTERNALFLASH_H__

/* 头文件 */
#include "main.h"
#include "../Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_flash.h"
#include "../Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_flash_ex.h"

/* 宏定义 */
// 用户根据自己的需要设置
#define internalFLASH_SIZE 512    // 所选STM32的FLASH容量大小(单位为K)
#define internalFLASH_WREN 1      // 使能FLASH写入(0，不是能;1，使能)
#define FLASH_WAITETIME    50000  // FLASH等待超时时间
//FLASH起始地址
#define internalFLASH_BASE 0x08000000 //STM32 FLASH的起始地址

/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */

extern void FLASH_PageErase(uint32_t PageAddress);

uint16_t internalFLASH_ReadHalfWord(uint32_t faddr);                                  // 读出半字
void internalFLASH_Write(uint32_t WriteAddr, uint16_t *pBuffer, uint16_t NumToWrite); // 从指定地址开始写入指定长度的数据
void internalFLASH_Read(uint32_t ReadAddr, uint16_t *pBuffer, uint16_t NumToRead);    // 从指定地址开始读出指定长度的数据

void internalFlash_Test(void);						   
#endif
