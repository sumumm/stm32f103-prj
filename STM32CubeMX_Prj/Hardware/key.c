/** =====================================================
 * Copyright © hk. 2022-2025. All rights reserved.
 * File name  : key.c
 * Author     : 上上签
 * Date       : 2023-05-31
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./key.h"

/**
  * @brief  按键处理函数
  * @note   注意此函数有响应优先级,KEY0>KEY1>KEY2>WK_UP!!
  * @param  mode 0,不支持连续按;1,支持连续按
  * @retval 返回按键值 
  *         @arg 0,没有任何按键按下
  *         @arg 4，WKUP 按下
  *         @arg 3，KEY2 按下 
  *         @arg 2，KEY1 按下
  *         @arg 1，KEY0 按下
  */
uint8_t KEY_Scan(uint8_t mode)
{
    static uint8_t key_up = 1; // 按键松开标志
    if (mode == 1)
	{
        key_up = 1; // 支持连按
	}
    if (key_up && (KEY0 == 0 || KEY1 == 0 || KEY2 == 0 || WK_UP == 1))
    {
        HAL_Delay(10);
        key_up = 0;
        if (KEY0 == 0)       return KEY0_PRES;
        else if (KEY1 == 0)  return KEY1_PRES;
        else if (KEY2 == 0)  return KEY2_PRES;
        else if (WK_UP == 1) return WKUP_PRES;
    }
    else if (KEY0 == 1 && KEY1 == 1 && KEY2 == 1 && WK_UP == 0)
	{
        key_up = 1;
	}
    return 0; // 无按键按下
}

void key_test(void)
{
    uint8_t key_value = 0;
    key_value = KEY_Scan(0);
    switch (key_value)
    {
        case WKUP_PRES:
            printf("wk_up pressed!!!\r\n");
            break;
        case KEY2_PRES:
            printf("key 2 pressed!!!\r\n");
            break;
        case KEY1_PRES:
            printf("key 1 pressed!!!\r\n");
            break;
        case KEY0_PRES:
            printf("key 0 pressed!!!\r\n");
            break;
        default:
            break;
    }
}
