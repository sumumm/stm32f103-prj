/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : sys.c
 * Author     : 上上签
 * Date       : 2023-05-21
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./sys.h"

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#ifdef __USART_H__
#ifdef MDK_PRG

#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdin; 
FILE __stdout;

//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 

// .\stm32f103-prj\output\stm32f103-prj.axf: Error: L6915E: Library reports error: __use_no_semihosting was requested, but _ttywrch was referenced
void _ttywrch(int x)
{
	x = x;
}
//重定义fputc函数 
int fputc(int ch, FILE *f)
{
	while((USART1->SR & (1<<6)) == 0);  //等待DR为空//循环发送,直到发送完毕 
    USART1->DR = (uint8_t) ch;
	return ch;
}
#endif
#ifdef MAKEFILE_PRG
/**
  * @brief  重定义 _write 函数 
  * @note   
  * @param  fd 文件描述符
  * @param  ch 要写入的字符串
  * @param  len 要写入的字符串长度
  * @retval len 返回写入字符串的长度
  */
int _write(int fd, char *ch, int len)
{
  HAL_UART_Transmit(&huart1, (uint8_t *)ch, len, 0xFFFF);
  return len;
}
#endif

//关闭所有中断
void INTX_DISABLE(void)
{		  
	__ASM volatile("cpsid i");
}
//开启所有中断
void INTX_ENABLE(void)
{
	__ASM volatile("cpsie i");		  
}

#endif 
