/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "../Hardware/w25qxx.h"
#include "../Hardware/sdCard.h"
/* Definitions of physical drive number for each drive */
#define DEV_SDCARD      0	/* Example: Map SD card to physical drive 0 */
#define DEV_SPI_FLASH   1	/* Example: Map SPI FLASH to physical drive 1 */

// 注意：这样转换的话在老版本的FatFs系统是可行的，但是我用的这个版本在格式化的时候会出现段错误
// 原来是16MB 256块(每块64KB)，每块16扇区(每个扇区4KB)，每个扇区16页(每页256B)
// 现在是16MB 256块(每块64KB)，每块16大扇区(每个大扇区4KB)，每个大扇区分8个小块，每块512字节，定义为1个小扇区
// 擦除的时候必须按4KB擦除，但是操作可以按256字节操作，擦除操作在写函数内部已经自动处理，座椅这里按512操作会很方便，内存也会少很多	  	 			    
#define FLASH_BLOCK_SIZE   8     	  // 每个BLOCK有8个扇区,这样一个BLOCK就等于原来的一个扇区4KB
#define FLASH_SECTOR_SIZE  512	      // 定义一个扇区为512字节	
#define FLASH_SECTOR_COUNT (2048*12)  // W25Q128,前12M字节给FATFS占用，每个扇区512B，一共12,582,912就是12M
#define FLASH_FATFS_BASE   0          // FATFS 在外部FLASH的起始地址 从0开始

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(
	BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
	int result = 0;

	switch (pdrv)
	{
		case DEV_SDCARD:
			result = SD_GetCardState();
			break;
		case DEV_SPI_FLASH:
			result = Get_W25QXX_Type();
			break;
		default:
			result = -1;
			break;
	}
	if(result == 0)
		return RES_OK;
	else
		return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(
	BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
	int result = 0;

	switch (pdrv)
	{
		case DEV_SDCARD:
			//MX_SDIO_SD_Init();// SDIO初始化
			//show_sdcard_info();
			result = SD_GetCardState();
			break;

		case DEV_SPI_FLASH:
			//MX_SPI2_Init(); // SPI初始化，在main.c中完成
			result = Get_W25QXX_Type();
			break;
		default:
			result = -1;
			break;
	}
	if(result == 0)
		return RES_OK;
	else
		return STA_NOINIT; //初始化失败
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(
	BYTE pdrv,	  /* Physical drive nmuber to identify the drive */
	BYTE *buff,	  /* Data buffer to store read data */
	LBA_t sector, /* Start sector in LBA */
	UINT count	  /* Number of sectors to read */
)
{
	int result = 0;

	switch (pdrv)
	{
		case DEV_SDCARD:
			result = SD_ReadDisk(buff, sector, count);	// 成功时返回0;
			break;
		case DEV_SPI_FLASH:
			result = W25QXX_Read((uint8_t *)buff, FLASH_FATFS_BASE + sector * FLASH_SECTOR_SIZE, count * FLASH_SECTOR_SIZE);
			break;
		default:
			result = -1;
			break;
	}
	if(result == 0)
		return RES_OK;
	else
		return RES_ERROR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write(
	BYTE pdrv,		  /* Physical drive nmuber to identify the drive */
	const BYTE *buff, /* Data to be written */
	LBA_t sector,	  /* Start sector in LBA */
	UINT count		  /* Number of sectors to write */
)
{
	int result = 0;

	switch (pdrv)
	{
		case DEV_SDCARD:
			result =SD_WriteDisk((uint8_t*)buff, sector, count);
			break;
		case DEV_SPI_FLASH:
			result = W25QXX_Write((uint8_t *)buff, FLASH_FATFS_BASE + sector * FLASH_SECTOR_SIZE, count * FLASH_SECTOR_SIZE);
			break;
		default:
			result = -1;
			break;
	}

	if(result == 0)
		return RES_OK;
	else
		return RES_ERROR;
}

#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl(
	BYTE pdrv, /* Physical drive nmuber (0..) */
	BYTE cmd,  /* Control code */
	void *buff /* Buffer to send/receive control data */
)
{
	int result = 0;

	switch (pdrv)
	{
		case DEV_SDCARD:
			switch(cmd)
			{
				case GET_SECTOR_COUNT:// 扇区数量
					*(DWORD *)buff = SDCardInfo.LogBlockNbr;
					break;
				case GET_SECTOR_SIZE:// 扇区大小
					*(WORD *)buff = SDCardInfo.BlockSize;// 每个扇区是512B
					break;
				case GET_BLOCK_SIZE:// 每次擦除块的大小的个数
					*(DWORD *)buff = SDCardInfo.LogBlockSize;
				default:
					break;
			}
			result = 0;
			break;
		case DEV_SPI_FLASH:
			switch(cmd)
			{
				case GET_SECTOR_COUNT:// 扇区数量
					// *(DWORD *)buff = 256 * 16;//256块x16个扇区
					*(DWORD *)buff = FLASH_SECTOR_COUNT;//12M/512B个扇区
					break;
				case GET_SECTOR_SIZE:// 扇区大小
					// *(WORD *)buff = 4096;// 每个扇区是4KB
					*(WORD *)buff = FLASH_SECTOR_SIZE;// 每个扇区是512B
					break;
				case GET_BLOCK_SIZE:// 每次擦除块的大小的个数
					// *(DWORD *)buff = 1;// 每次只擦除一个扇区
					*(DWORD *)buff = FLASH_BLOCK_SIZE;// 每次只擦除一个大扇区，512*8=4096
				default:
					break;
			}
			result = 0;
			break;
		default:
			result = -1;
			break;
	}

	if(result == 0)
		return RES_OK;
	else
		return RES_ERROR;
}

/**
  * @brief  获取当前系统时间
  * @note   用于文件时间属性的确定
  * @param  
  * @retval 
  */
DWORD get_fattime(void)
{
	/* 返回当前时间戳 */
	return ((DWORD)(2023 - 1980) << 25) /* Year 2015 */
		   | ((DWORD)1 << 21)			/* Month 1 */
		   | ((DWORD)1 << 16)			/* Mday 1 */
		   | ((DWORD)0 << 11)			/* Hour 0 */
		   | ((DWORD)0 << 5)			/* Min 0 */
		   | ((DWORD)0 >> 1);			/* Sec 0 */
}
