/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : ex_fatfs_test.h
 * Author     : 上上签
 * Date       : 2023-06-07
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __EX_FATFS_TEST_H__
#define __EX_FATFS_TEST_H__

/* 头文件 */
#include "./ff.h"
#include "../Hardware/func_mem.h"
#include "./func_fatfs.h"
/* 宏定义 */
#define USE_FATTESTER 1 // 定义是否支持文件系统测试功能 1, 支持(开启); 0, 不支持(关闭);
#if USE_FATTESTER == 1
/* 结构体定义 */
/* FATFS 测试用结构体 */
typedef struct __ex_fatfs_t
{
    FIL *file;          /* 文件结构体指针1 */
    FILINFO fileinfo;   /* 文件信息 */
    DIR dir;            /* 目录 */
    BYTE *fatbuf;       /* 读写缓存 */
    BYTE initflag;      /* 初始化标志 */
} EX_FATFS_TEST;
/* 全局变量声明 */
extern EX_FATFS_TEST fattester;
/* 函数声明 */

int8_t mf_init(void);
void mf_free(void);
uint8_t mf_open(uint8_t *path, uint8_t mode);
uint8_t mf_close(void);

uint8_t mf_read(uint16_t len);
uint8_t mf_write(uint8_t*dat,uint16_t len);
uint8_t mf_opendir(uint8_t* path);
uint8_t mf_closedir(void);
uint8_t mf_readdir(void);
uint8_t mf_scan_files(uint8_t * path);

uint8_t mf_lseek(uint32_t offset);
uint32_t mf_tell(void);
uint32_t mf_size(void);
uint8_t mf_mkdir(uint8_t*pname);
uint8_t mf_fmkfs(uint8_t* path,uint8_t opt,uint16_t au);
uint8_t mf_unlink(uint8_t *pname);
uint8_t mf_rename(uint8_t *oldname,uint8_t* newname);
void mf_getlabel(uint8_t *path);
void mf_setlabel(uint8_t *path); 
void mf_gets(uint16_t size);
uint8_t mf_putc(uint8_t c);
uint8_t mf_puts(uint8_t*c);

void fatfs_Test(void);

#endif

#endif /* __EX_FATFS_TEST_H__ */
