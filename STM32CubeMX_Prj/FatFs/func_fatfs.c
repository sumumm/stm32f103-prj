/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : func_fatfs.c
 * Author     : 上上签
 * Date       : 2023-06-08
 * Version    : 
 * Description: 
 * ======================================================
 */

#include "./func_fatfs.h"

/* 公共文件区,使用malloc的时候 */
FUNC_FATFS gFatfsParam = {0};

static BYTE API_RET[][90] = {
    "(0) Succeeded ",
    "(1) A hard error occurred in the low level disk I/O layer ",
    "(2) Assertion failed ",
    "(3) The physical drive cannot work ",
    "(4) Could not find the file ",
    "(5) Could not find the path ",
    "(6) The path name format is invalid ",
    "(7) Access denied due to prohibited access or directory full ",
    "(8) Access denied due to prohibited access ",
    "(9) The file/directory object is invalid ",
    "(10) The physical drive is write protected ",
    "(11) The logical drive number is invalid ",
    "(12) The volume has no work area ",
    "(13) There is no valid FAT volume ",
    "(14) The f_mkfs() aborted due to any problem ",
    "(15) Could not get a grant to access the volume within defined period ",
    "(16) The operation is rejected according to the file sharing policy ",
    "(17) LFN working buffer could not be allocated ",
    "(18) Number of open files > FF_FS_LOCK ",
    "(19) Given parameter is invalid ",
};

/* 文件类型列表 */
char * const FILE_TYPE_TBL[FILE_MAX_TYPE_NUM][FILE_MAX_SUBT_NUM] = {
    {"BIN"},                                            /* BIN文件 */
    {"LRC"},                                            /* LRC文件 */
    {"NES", "SMS"},                                     /* NES/SMS文件 */
    {"TXT", "C", "H"},                                  /* 文本文件 */
    {"WAV", "MP3", "OGG", "FLAC", "AAC", "WMA", "MID"}, /* 支持的音乐文件 */
    {"BMP", "JPG", "JPEG", "GIF"},                      /* 图片文件 */
    {"AVI"},                                            /* 视频文件 */
};

/**
  * @brief  为文件系统所用变量申请内存
  * @note   申请的是fs和work的内存，若是使能了文件测试，也会申请文件使用的空间
  * @param  
  * @retval 0，申请成功，-1，申请失败
  */
int8_t func_fatfs_init(void)
{
    uint8_t i = 0;
    int8_t res = 0;
    for (i = 0; i < FF_VOLUMES; i++)
    {
        gFatfsParam.fs[i] = (FATFS *)pub_malloc(SRAMIN, sizeof(FATFS)); // 为磁盘i工作区申请内存
        if (!gFatfsParam.fs[i])
            break;
    }
    if (i != FF_VOLUMES )
    {
        printf("malloc fs failed!!!\r\n");
        return -1;
    }
    gFatfsParam.work = (uint8_t *)pub_malloc(SRAMEX, FF_MAX_SS); // 为work申请内存
    if(gFatfsParam.work == NULL)
    {
        printf("malloc work failed!!!\r\n");
        return -1;
    }
#if USE_FATTESTER == 1  /* 如果使能了文件系统测试 */
    res = mf_init();    /* 初始化文件系统测试(申请内存) */
    if(res != 0)
    {
        printf("mf_init failed!!!\r\n");
        return -1;
    }
#endif
    return 0;
}

/**
  * @brief  将小写字母转为大写字母,如果是数字,则保持不变.
  * @note   
  * @param  ch 要转换的字母
  * @retval 转换后的字母,大写
  */
uint8_t func_fatfs_char_upper(uint8_t ch)
{
    if (ch < 'A')
        return ch; /* 数字,保持不变. */

    if (ch >= 'a')
    {
        return ch - 0x20; /* 变为大写. */
    }
    else
    {
        return ch; /* 大写,保持不变 */
    }
}

/**
  * @brief  报告文件的类型
  * @note   
  * @param  fname 文件名
  * @retval 文件类型,0XFF,表示无法识别的文件类型编号.其他 , 高四位表示所属大类, 低四位表示所属小类.
  */
uint8_t func_fatfs_file_type(char *fname)
{
    uint8_t tbuf[5];
    char *attr = NULL; /* 后缀名 */
    uint8_t i = 0, j = 0;

    while (i < 250)
    {
        i++;

        if (*fname == '\0')
            break; /* 偏移到了最后了. */

        fname++;
    }

    if (i == 250)
        return 0XFF; /* 错误的字符串. */

    for (i = 0; i < 5; i++) /* 得到后缀名 */
    {
        fname--;

        if (*fname == '.')
        {
            fname++;
            attr = fname;
            break;
        }
    }

    if (attr == 0)
        return 0XFF;

    strcpy((char *)tbuf, (const char *)attr); /* copy */

    for (i = 0; i < 4; i++)
        tbuf[i] = func_fatfs_char_upper(tbuf[i]); /* 全部变为大写 */

    for (i = 0; i < FILE_MAX_TYPE_NUM; i++) /* 大类对比 */
    {
        for (j = 0; j < FILE_MAX_SUBT_NUM; j++) /* 子类对比 */
        {
            if (*FILE_TYPE_TBL[i][j] == 0)
                break; /* 此组已经没有可对比的成员了. */

            if (strcmp((const char *)FILE_TYPE_TBL[i][j], (const char *)tbuf) == 0) /* 找到了 */
            {
                return (i << 4) | j;
            }
        }
    }

    return 0XFF; /* 没找到 */
}

/**
  * @brief  得到磁盘剩余容量
  * @note   好像是去掉了FatFs自身占据的空间
  * @param  drv 磁盘编号("0:"~"9:")
  * @param  total 总容量 （单位KB）
  * @param  free 剩余容量 （单位KB）
  * @retval 0,正常.其他,错误代码
  */
uint8_t func_fatfs_getfree(uint8_t *drv, uint32_t *total, uint32_t *free)
{
    FATFS *fs1 = NULL;
    uint8_t res = 0;
    uint32_t fre_clust = 0, fre_sect = 0, tot_sect = 0;
    // 得到磁盘信息及空闲簇数量
    res = (uint32_t)f_getfree((const TCHAR *)drv, (DWORD *)&fre_clust, &fs1);
    if (res == 0)
    {
        tot_sect = (fs1->n_fatent - 2) * fs1->csize; // 得到总扇区数
        fre_sect = fre_clust * fs1->csize;           // 得到空闲扇区数
		//printf("fs1->n_fatent %d, fs1->csize %d, tot_sect %d, fre_sect %d\r\n", fs1->n_fatent, fs1->csize, tot_sect, fre_sect);
#if FF_MAX_SS != 512                                 // 扇区大小不是512字节,则转换为512字节
        tot_sect *= fs1->ssize / 512;
        fre_sect *= fs1->ssize / 512;
#endif
        *total = tot_sect >> 1; // 单位为KB
        *free = fre_sect >> 1;  // 单位为KB
    }
    return res;
}

/**
  * @brief  文件复制,将psrc文件copy到pdst.
  * @note   注意: 文件大小不要超过4GB.
  * @param  fcpymsg 函数指针, 用于实现拷贝时的信息显示
  *                 pname:文件/文件夹名
  *                 pct:百分比
  *                 mode:bit0,更新文件名；bit1,更新百分比pct；bit2,更新文件夹；其他,保留
  *                 返回值: 0, 正常; 1, 强制退出;
  * @param  psrc 源文件
  * @param  pdst 目标文件
  * @param  totsize 总大小(当totsize为0的时候,表示仅仅为单个文件拷贝)
  * @param  cpdsize 已复制了的大小.
  * @param  fwmode 文件写入模式
  *           @arg 0 不覆盖原有的文件
  *           @arg 1 覆盖原有的文件
  * @retval 执行结果.0, 正常;0XFF, 强制退出;其他, 错误代码
  */
uint8_t func_fatfs_file_copy(CpyMsgFunc fcpymsg, uint8_t *psrc, uint8_t *pdst,
                             uint32_t totsize, uint32_t cpdsize, uint8_t fwmode)
{
    uint8_t res;
    uint16_t br = 0;
    uint16_t bw = 0;
    FIL *fsrc = 0;
    FIL *fdst = 0;
    uint8_t *fbuf = 0;
    uint8_t curpct = 0;
    unsigned long long lcpdsize = cpdsize;

    fsrc = (FIL *)pub_malloc(SRAMIN, sizeof(FIL)); /* 申请内存 */
    fdst = (FIL *)pub_malloc(SRAMIN, sizeof(FIL));
    fbuf = (uint8_t *)pub_malloc(SRAMIN, 8192);

    if (fsrc == NULL || fdst == NULL || fbuf == NULL)
    {
        res = 100; /* 前面的值留给fatfs */
    }
    else
    {
        if (fwmode == 0)
        {
            fwmode = FA_CREATE_NEW; /* 不覆盖 */
        }
        else
        {
            fwmode = FA_CREATE_ALWAYS; /* 覆盖存在的文件 */
        }

        res = f_open(fsrc, (const TCHAR *)psrc, FA_READ | FA_OPEN_EXISTING); /* 打开只读文件 */

        if (res == 0)
            res = f_open(fdst, (const TCHAR *)pdst, FA_WRITE | fwmode); /* 第一个打开成功,才开始打开第二个 */

        if (res == 0) /* 两个都打开成功了 */
        {
            if (totsize == 0) /* 仅仅是单个文件复制 */
            {
                totsize = fsrc->obj.objsize;
                lcpdsize = 0;
                curpct = 0;
            }
            else
            {
                curpct = (lcpdsize * 100) / totsize; /* 得到新百分比 */
            }

            fcpymsg(psrc, curpct, 0X02); /* 更新百分比 */

            while (res == 0) /* 开始复制 */
            {
                res = f_read(fsrc, fbuf, 8192, (UINT *)&br); /* 源头读出512字节 */

                if (res || br == 0)
                    break;

                res = f_write(fdst, fbuf, (UINT)br, (UINT *)&bw); /* 写入目的文件 */
                lcpdsize += bw;

                if (curpct != (lcpdsize * 100) / totsize) /* 是否需要更新百分比 */
                {
                    curpct = (lcpdsize * 100) / totsize;

                    if (fcpymsg(psrc, curpct, 0X02)) /* 更新百分比 */
                    {
                        res = 0XFF; /* 强制退出 */
                        break;
                    }
                }

                if (res || bw < br)
                    break;
            }

            f_close(fsrc);
            f_close(fdst);
        }
    }

    pub_free(SRAMIN, fsrc); /* 释放内存 */
    pub_free(SRAMIN, fdst);
    pub_free(SRAMIN, fbuf);
    return res;
}

/**
  * @brief  得到路径下的文件夹,即把路径全部去掉, 只留下文件夹名字.
  * @note   
  * @param  pname 详细路径
  * @retval 0,路径就是个卷标号;其他,文件夹名字首地址
  */
uint8_t *func_fatfs_get_src_dname(uint8_t *pname)
{
    uint16_t temp = 0;

    while (*pname != 0)
    {
        pname++;
        temp++;
    }

    if (temp < 4)
        return 0;

    while ((*pname != 0x5c) && (*pname != 0x2f))
    {
        pname--; /* 追述到倒数第一个"\"或者"/"处 */
    }

    return ++pname;
}

/**
  * @brief  得到文件夹大小
  * @note   注意: 文件夹大小不要超过4GB.
  * @param  pname 详细路径
  * @retval 0, 文件夹大小为0, 或者读取过程中发生了错误.其他, 文件夹大小
  */
uint32_t func_fatfs_get_folder_size(uint8_t *fdname)
{
    uint8_t res = 0;
    DIR *fddir = 0;        /* 目录 */
    FILINFO *finfo = 0;    /* 文件信息 */
    uint8_t *pathname = 0; /* 目标文件夹路径+文件名 */
    uint16_t pathlen = 0;  /* 目标路径长度 */
    uint32_t fdsize = 0;

    fddir = (DIR *)pub_malloc(SRAMIN, sizeof(DIR)); /* 申请内存 */
    finfo = (FILINFO *)pub_malloc(SRAMIN, sizeof(FILINFO));

    if (fddir == NULL || finfo == NULL)
    {
        printf("malloc DIR or FILINFO failed!!!");
        res = 100;
    }

    if (res == 0)
    {
        pathname = (uint8_t *)pub_malloc(SRAMIN, MAX_PATHNAME_DEPTH);

        if (pathname == NULL)
        {
            printf("malloc pathname failed!!!");
            res = 101;
        }
        if (res == 0)
        {
            pathname[0] = 0;
            strcat((char *)pathname, (const char *)fdname); /* 复制路径 */
            res = f_opendir(fddir, (const TCHAR *)fdname);  /* 打开源目录 */

            if (res == 0) /* 打开目录成功 */
            {
                while (res == 0) /* 开始复制文件夹里面的东东 */
                {
                    res = f_readdir(fddir, finfo); /* 读取目录下的一个文件 */

                    if (res != FR_OK || finfo->fname[0] == 0)
                        break; /* 错误了/到末尾了,退出 */

                    if (finfo->fname[0] == '.')
                        continue; /* 忽略上级目录 */

                    if (finfo->fattrib & 0X10) /* 是子目录(文件属性,0X20,归档文件;0X10,子目录;) */
                    {
                        pathlen = strlen((const char *)pathname);             /* 得到当前路径的长度 */
                        strcat((char *)pathname, (const char *)"/");          /* 加斜杠 */
                        strcat((char *)pathname, (const char *)finfo->fname); /* 源路径加上子目录名字 */
                        // printf("\r\nsub folder:%s\r\n",pathname);      /* 打印子目录名 */
                        fdsize += func_fatfs_get_folder_size(pathname); /* 得到子目录大小,递归调用 */
                        pathname[pathlen] = 0;                      /* 加入结束符 */
                    }
                    else
                    {
                        fdsize += finfo->fsize; /* 非目录,直接加上文件的大小 */
                    }
                }
            }

            pub_free(SRAMIN, pathname);
        }
    }

    pub_free(SRAMIN, fddir);
    pub_free(SRAMIN, finfo);

    if (res)
        return 0;
    else
        return fdsize;
}

/**
  * @brief  文件复制,将psrc文件夹copy到pdst文件夹.
  * @note   注意: 文件大小不要超过4GB.
  * @param  fcpymsg 函数指针, 用于实现拷贝时的信息显示
  *                 pname:文件/文件夹名
  *                 pct:百分比
  *                 mode:bit0,更新文件名；bit1,更新百分比pct；bit2,更新文件夹；其他,保留
  *                 返回值: 0, 正常; 1, 强制退出;
  * @param  psrc 源文件夹
  * @param  pdst 目标文件夹，必须形如"X:"/"X:XX"/"X:XX/XX"之类的. 且要确认上一级文件夹存在
  * @param  totsize 总大小(当totsize为0的时候,表示仅仅为单个文件拷贝)
  * @param  cpdsize 已复制了的大小.
  * @param  fwmode 文件写入模式
  *           @arg 0 不覆盖原有的文件
  *           @arg 1 覆盖原有的文件
  * @retval 执行结果.0, 正常;0XFF, 强制退出;其他, 错误代码
  */
uint8_t func_fatfs_folder_copy(CpyMsgFunc fcpymsg, uint8_t *psrc, uint8_t *pdst,
                           uint32_t *totsize, uint32_t *cpdsize, uint8_t fwmode)
{
    uint8_t res = 0;
    DIR *srcdir = 0;    /* 源目录 */
    DIR *dstdir = 0;    /* 源目录 */
    FILINFO *finfo = 0; /* 文件信息 */
    uint8_t *fn = 0;    /* 长文件名 */

    uint8_t *dstpathname = 0; /* 目标文件夹路径+文件名 */
    uint8_t *srcpathname = 0; /* 源文件夹路径+文件名 */

    uint16_t dstpathlen = 0; /* 目标路径长度 */
    uint16_t srcpathlen = 0; /* 源路径长度 */

    srcdir = (DIR *)pub_malloc(SRAMIN, sizeof(DIR)); /* 申请内存 */
    dstdir = (DIR *)pub_malloc(SRAMIN, sizeof(DIR));
    finfo = (FILINFO *)pub_malloc(SRAMIN, sizeof(FILINFO));

    if (srcdir == NULL || dstdir == NULL || finfo == NULL)
        res = 100;

    if (res == 0)
    {
        dstpathname = pub_malloc(SRAMIN, MAX_PATHNAME_DEPTH);
        srcpathname = pub_malloc(SRAMIN, MAX_PATHNAME_DEPTH);

        if (dstpathname == NULL || srcpathname == NULL)
            res = 101;

        if (res == 0)
        {
            dstpathname[0] = 0;
            srcpathname[0] = 0;
            strcat((char *)srcpathname, (const char *)psrc); /* 复制原始源文件路径 */
            strcat((char *)dstpathname, (const char *)pdst); /* 复制原始目标文件路径 */
            res = f_opendir(srcdir, (const TCHAR *)psrc);    /* 打开源目录 */

            if (res == 0) /* 打开目录成功 */
            {
                strcat((char *)dstpathname, (const char *)"/"); /* 加入斜杠 */
                fn = func_fatfs_get_src_dname(psrc);

                if (fn == 0) /* 卷标拷贝 */
                {
                    dstpathlen = strlen((const char *)dstpathname);
                    dstpathname[dstpathlen] = psrc[0]; /* 记录卷标 */
                    dstpathname[dstpathlen + 1] = 0;   /* 结束符 */
                }
                else
                    strcat((char *)dstpathname, (const char *)fn); /* 加文件名 */

                fcpymsg(fn, 0, 0X04);                      /* 更新文件夹名 */
                res = f_mkdir((const TCHAR *)dstpathname); /* 如果文件夹已经存在,就不创建.如果不存在就创建新的文件夹. */

                if (res == FR_EXIST)
                    res = 0;

                while (res == 0) /* 开始复制文件夹里面的东东 */
                {
                    res = f_readdir(srcdir, finfo); /* 读取目录下的一个文件 */

                    if (res != FR_OK || finfo->fname[0] == 0)
                        break; /* 错误了/到末尾了,退出 */

                    if (finfo->fname[0] == '.')
                        continue; /* 忽略上级目录 */

                    fn = (uint8_t *)finfo->fname;                   /* 得到文件名 */
                    dstpathlen = strlen((const char *)dstpathname); /* 得到当前目标路径的长度 */
                    srcpathlen = strlen((const char *)srcpathname); /* 得到源路径长度 */

                    strcat((char *)srcpathname, (const char *)"/"); /* 源路径加斜杠 */

                    if (finfo->fattrib & 0X10) /* 是子目录(文件属性,0X20,归档文件;0X10,子目录;) */
                    {
                        strcat((char *)srcpathname, (const char *)fn);                                         /* 源路径加上子目录名字 */
                        res = func_fatfs_folder_copy(fcpymsg, srcpathname, dstpathname, totsize, cpdsize, fwmode); /* 拷贝文件夹 */
                    }
                    else /* 非目录 */
                    {
                        strcat((char *)dstpathname, (const char *)"/");                                        /* 目标路径加斜杠 */
                        strcat((char *)dstpathname, (const char *)fn);                                         /* 目标路径加文件名 */
                        strcat((char *)srcpathname, (const char *)fn);                                         /* 源路径加文件名 */
                        fcpymsg(fn, 0, 0X01);                                                                  /* 更新文件名 */
                        res = func_fatfs_file_copy(fcpymsg, srcpathname, dstpathname, *totsize, *cpdsize, fwmode); /* 复制文件 */
                        *cpdsize += finfo->fsize;                                                              /* 增加一个文件大小 */
                    }

                    srcpathname[srcpathlen] = 0; /* 加入结束符 */
                    dstpathname[dstpathlen] = 0; /* 加入结束符 */
                }
            }

            pub_free(SRAMIN, dstpathname);
            pub_free(SRAMIN, srcpathname);
        }
    }

    pub_free(SRAMIN, srcdir);
    pub_free(SRAMIN, dstdir);
    pub_free(SRAMIN, finfo);
    return res;
}

/**
  * @brief  获取出错的字符串含义
  * @note   
  * @param  err_num 错误码
  * @retval 
  */
BYTE *func_fatfs_err(uint16_t err_num)
{
   return API_RET[err_num];
}

/**
  * @brief  FatFs文件系统挂载
  * @note   将会挂载所有支持的存储介质,哟个bug，就是sd卡有问题的时候，会产生段错误，没查是哪的问题
  * @param  
  * @retval 
  */
uint8_t func_fatfs_fmount(void)
{
    uint8_t res = 0;
    uint8_t i = 0;
    TCHAR drv_num[3] = {0};
    func_fatfs_init(); /* 为fatfs相关变量申请内存 */
    for (i = 0; i < FF_VOLUMES; i++)
    {
        sprintf(drv_num, "%d:", i);
        res = f_mount(gFatfsParam.fs[i], (const TCHAR *)drv_num, 1); // 挂载文件系统
        if (res != FR_OK)
        {
            printf("f_mount %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
            if (res == FR_NO_FILESYSTEM) // FAT文件系统错误,重新格式化，创建文件系统
            {
                printf("Disk Formatting...\r\n"); // 格式化存储介质，创建文件系统
                res = f_mkfs((const TCHAR *)drv_num, 0, 0, FF_MAX_SS);
                if (res == FR_OK)
                {
                    printf("Disk Format Finish!!!\r\n");                  // 格式化完成
                    res = f_mount(NULL, (const TCHAR *)drv_num, 1);                  // 格式化后，先取消挂载
                    res = f_mount(gFatfsParam.fs[i], (const TCHAR *)drv_num, 1); // 重新挂载
                    if (res != FR_OK)
                    {
                        printf("f_mount %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
                        continue; // 直接进行下一次循环
                    }
                    gFatfsParam.fsStatus |= 1<<i;
                }
                else
                {
                    printf("f_mkfs %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
                    continue; // 直接进行下一次循环
                }
            }
        }
        else
            gFatfsParam.fsStatus |= 1<<i;
    }
	return 0;
}
