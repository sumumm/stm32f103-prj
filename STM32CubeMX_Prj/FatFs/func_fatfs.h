/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : func_fatfs.h
 * Author     : 上上签
 * Date       : 2023-06-08
 * Version    : 
 * Description: 
 * ======================================================
 */

#ifndef __FUNC_FATFS_H__
#define __FUNC_FATFS_H__

/* 头文件 */
#include <stdio.h>
#include "./ff.h"
#include "./func_mem.h"
#include "./ex_fatfs_test.h"
#include "../Hardware/func_printf.h"
/* 宏定义 */
#define FILE_MAX_TYPE_NUM       7       /* 最多FILE_MAX_TYPE_NUM个大类 */
#define FILE_MAX_SUBT_NUM       7       /* 最多FILE_MAX_SUBT_NUM个小类 */
#define MAX_PATHNAME_DEPTH      512 + 1 /* 最大目标文件路径+文件名深度 */
/* 结构体定义 */
typedef struct __ex_fatfs
{
    FATFS *fs[FF_VOLUMES]; // 逻辑磁盘工作区.
    BYTE *work;  // 挂载的时候用
    BYTE fsStatus;//文件系统的状态，bit[0]表示"0:"的状态,bit[1]表示"1:"的状态
} FUNC_FATFS;

typedef uint8_t (*CpyMsgFunc)(uint8_t *pname, uint8_t pct, uint8_t mode);
/* 全局变量声明 */
extern FUNC_FATFS gFatfsParam;
/* 函数声明 */

int8_t func_fatfs_init(void);
uint8_t func_fatfs_char_upper(uint8_t ch);
uint8_t func_fatfs_getfree(uint8_t *drv, uint32_t *total, uint32_t *free);
uint8_t func_fatfs_file_type(char *fname);
uint8_t func_fatfs_file_copy(CpyMsgFunc fcpymsg, uint8_t *psrc, uint8_t *pdst,
                             uint32_t totsize, uint32_t cpdsize, uint8_t fwmode);
uint8_t *func_fatfs_get_src_dname(uint8_t *pname);
uint32_t func_fatfs_get_folder_size(uint8_t *fdname);
uint8_t func_fatfs_folder_copy(CpyMsgFunc fcpymsg, uint8_t *psrc, uint8_t *pdst,
                               uint32_t *totsize, uint32_t *cpdsize, uint8_t fwmode);
BYTE *func_fatfs_err(uint16_t err_num);
uint8_t func_fatfs_fmount(void);
#endif /* __FUNC_FATFS_H__ */
