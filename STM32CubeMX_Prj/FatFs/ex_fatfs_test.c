/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : ex_fatfs_test.c
 * Author     : 上上签
 * Date       : 2023-06-07
 * Version    : 
 * Description: 
 * ======================================================
 */

#include "./ex_fatfs_test.h"

#if USE_FATTESTER == 1

EX_FATFS_TEST fattester = {0};

/**
  * @brief  初始化文件系统测试(申请内存)
  * @note   该函数必须在执行任何文件系统测试之前被调用一次.该函数只需要被成功调用一次即可,无需重复调用!!
  * @param  
  * @retval 执行结果: 0, 成功; -1, 失败;
  */
int8_t mf_init(void)
{
    fattester.file = (FIL *)pub_malloc(SRAMIN, sizeof(FIL));   /* ?file???? */
    fattester.fatbuf = (BYTE *)pub_malloc(SRAMIN, 512);        /* ?fattester.fatbuf???? */

    if ((fattester.file == NULL) || (fattester.fatbuf == NULL))
    {
        mf_free();  /* 释放内存 */
        return -1;  /* 申请失败 */
    }
    return 0;
}

/**
  * @brief  释放文件系统测试申请的内存
  * @note   调用完该函数以后, 文件系统测试功能将失效.
  * @param  
  * @retval 
  */
void mf_free(void)
{
    pub_free(SRAMIN, fattester.file);
    pub_free(SRAMIN, fattester.fatbuf);
}

/**
  * @brief  打开文件
  * @note   
  * @param  path 路径 + 文件名
  * @param  mode 打开模式
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_open(uint8_t *path, uint8_t mode)
{
    uint8_t res;
    res = f_open(fattester.file, (const TCHAR *)path, mode);    /* 打开文件 */
    return res;
}

/**
  * @brief  关闭文件
  * @note   
  * @param  
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_close(void)
{
    f_close(fattester.file);
    return 0;
}

/**
  * @brief  读出数据
  * @note   
  * @param  len 读出的长度
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_read(uint16_t len)
{
    uint16_t i, t;
    uint8_t res = 0;
    uint16_t tlen = 0;
    uint32_t br = 0;
    printf("\r\nRead fattester.file data is:\r\n");

    for (i = 0; i < len / 512; i++)
    {
        res = f_read(fattester.file, fattester.fatbuf, 512, &br);

        if (res)
        {
          printf("Read Error:%d\r\n", res);
          break;
        }
        else
        {
          tlen += br;

          for (t = 0; t < br; t++)
            printf("%c", fattester.fatbuf[t]);
        }
    }

    if (len % 512)
    {
        res = f_read(fattester.file, fattester.fatbuf, len % 512, &br);

        if (res) /* 读数据出错了 */
        {
          printf("\r\nRead Error:%d\r\n", res);
        }
        else
        {
          tlen += br;

          for (t = 0; t < br; t++)
            printf("%c", fattester.fatbuf[t]);
        }
    }

    if (tlen)
        printf("\r\nReaded data len:%d\r\n", tlen); /* 读到的数据长度 */

    printf("Read data over\r\n");
    return res;
}

/**
  * @brief  写入数据
  * @note   
  * @param  pdata 数据缓存区
  * @param  len 写入长度
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_write(uint8_t *pdata, uint16_t len)
{
    uint8_t res;
    uint32_t bw = 0;

    printf("\r\nBegin Write fattester.file...\r\n");
    printf("Write data len:%d\r\n", len);
    res = f_write(fattester.file, pdata, len, &bw);

    if (res)
    {
        printf("Write Error:%d\r\n", res);
    }
    else
    {
        printf("Writed data len:%d\r\n", bw);
    }

    printf("Write data over.\r\n");
    return res;
}

/**
  * @brief  打开目录
  * @note   
  * @param  path 路径
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_opendir(uint8_t *path)
{
    return f_opendir(&fattester.dir, (const TCHAR *)path);
}

/**
  * @brief  关闭目录
  * @note   
  * @param  
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_closedir(void)
{
    return f_closedir(&fattester.dir);
}

/**
  * @brief  打读取文件夹
  * @note   
  * @param  
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_readdir(void)
{
    uint8_t res;
    res = f_readdir(&fattester.dir, &fattester.fileinfo); /* 读取一个文件的信息 */

    if (res != FR_OK)
        return res; /* 出错了 */

    printf("\r\n fattester.dir info:\r\n");

    printf("fattester.dir.dptr:%d\r\n", fattester.dir.dptr);
    printf("fattester.dir.obj.id:%d\r\n", fattester.dir.obj.id);
    printf("fattester.dir.obj.sclust:%d\r\n", fattester.dir.obj.sclust);
    printf("fattester.dir.obj.objsize:%lld\r\n", fattester.dir.obj.objsize);
    #if FF_FS_EXFAT
    printf("fattester.dir.obj.c_ofs:%d\r\n", fattester.dir.obj.c_ofs);
    #endif
    printf("fattester.dir.clust:%d\r\n", fattester.dir.clust);
    printf("fattester.dir.sect:%d\r\n", fattester.dir.sect);
    #if FF_USE_LFN
    printf("fattester.dir.blk_ofs:%d\r\n", fattester.dir.blk_ofs);
    #endif
      

    printf("\r\n");
    printf("fattester.file Name is:%s\r\n", fattester.fileinfo.fname);
    printf("fattester.file Size is:%lld\r\n", fattester.fileinfo.fsize);
    printf("fattester.file data is:%d\r\n", fattester.fileinfo.fdate);
    printf("fattester.file time is:%d\r\n", fattester.fileinfo.ftime);
    printf("fattester.file Attr is:%d\r\n", fattester.fileinfo.fattrib);
    printf("\r\n");
    return 0;
}

 /**
   * @brief  遍历文件
   * @note   
   * @param  path 路径
   * @retval 执行结果(参见FATFS, FRESULT的定义)
   */
uint8_t mf_scan_files(uint8_t *path)
{
    FRESULT res;
    res = f_opendir(&fattester.dir, (const TCHAR *)path); /* 打开一个目录 */

    if (res == FR_OK)
    {
        printf("\r\n");

        while (1)
        {
          res = f_readdir(&fattester.dir, &fattester.fileinfo); /* 读取目录下的一个文件 */

          if (res != FR_OK || fattester.fileinfo.fname[0] == 0)
          {
            break; /* 错误了/到末尾了,退出 */
          }

          // if (fattester.fileinfo.fname[0] == '.') continue;    /* 忽略上级目录 */
          printf("%s/", path);                        /* 打印路径 */
          printf("%s\r\n", fattester.fileinfo.fname); /* 打印文件名 */
        }
    }

    return res;
}

/**
  * @brief  文件读写指针偏移
  * @note   
  * @param  offset 相对首地址的偏移量
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_lseek(uint32_t offset)
{
    return f_lseek(fattester.file, offset);
}

/**
  * @brief  读取文件当前读写指针的位置
  * @note   
  * @param  
  * @retval 当前位置
  */
uint32_t mf_tell(void)
{
    return f_tell(fattester.file);
}

/**
  * @brief  读取文件大小
  * @note   
  * @param  
  * @retval 文件大小
  */
uint32_t mf_size(void)
{
    return f_size(fattester.file);
}

/**
 * @brief       
 * @param       
 * @retval      
 */
/**
  * @brief  创建目录
  * @note   
  * @param  path 目录路径 + 名字
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_mkdir(uint8_t *path)
{
    return f_mkdir((const TCHAR *)path);
}

/**
  * @brief  格式化文件系统(创建文件系统)
  * @note   
  * @param  path 磁盘路径，比如"0:"、"1:"
  * @param  opt 模式,FM_FAT,FM_FAT32,FM_EXFAT,FM_ANY等...
  * @param  au 簇大小
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_fmkfs(uint8_t *path, uint8_t opt, uint16_t au)
{
    MKFS_PARM temp = {FM_ANY, 0, 0, 0, 0};
    temp.fmt = opt;                                          /* 文件系统格式,1：FM_FAT;2,FM_FAT32;4,FM_EXFAT; */
    temp.au_size = au;                                       /* 簇大小定义,0则使用默认簇大小 */
    return f_mkfs((const TCHAR *)path, &temp, 0, FF_MAX_SS); /* 格式化,默认参数,workbuf,最少_MAX_SS大小 */
}

/**
  * @brief  删除文件/目录
  * @note   
  * @param  path 文件/目录路径+名字
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_unlink(uint8_t *path)
{
    return f_unlink((const TCHAR *)path);
}

/**
  * @brief  修改文件/目录名字(如果目录不同,还可以移动文件!)
  * @note   
  * @param  oldname 之前的名字
  * @param  newname 新名字
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_rename(uint8_t *oldname, uint8_t *newname)
{
    return f_rename((const TCHAR *)oldname, (const TCHAR *)newname);
}

/**
  * @brief  获取盘符(磁盘名字)
  * @note   
  * @param  path 磁盘路径，比如"0:"、"1:"
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
void mf_getlabel(uint8_t *path)
{
    uint8_t buf[20];
    uint32_t sn = 0;
    uint8_t res;
    res = f_getlabel((const TCHAR *)path, (TCHAR *)buf, (DWORD *)&sn);

    if (res == FR_OK)
    {
        printf("\r\n磁盘%s 的盘符为:%s\r\n", path, buf);
        printf("磁盘%s 的序列号:%X\r\n\r\n", path, sn);
    }
    else
    {
        printf("\r\n获取失败，错误码:%X\r\n", res);
    }
}

/**
  * @brief  设置盘符（磁盘名字），最长11个字符！！，支持数字和大写字母组合以及汉字等
  * @note   
  * @param  path 磁盘号+名字，比如"0:ALIENTEK"、"1:OPENEDV"
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
void mf_setlabel(uint8_t *path)
{
    uint8_t res;
    res = f_setlabel((const TCHAR *)path);

    if (res == FR_OK)
    {
        printf("\r\n磁盘盘符设置成功:%s\r\n", path);
    }
    else
        printf("\r\n磁盘盘符设置失败，错误码:%X\r\n", res);
}

/**
  * @brief  从文件里面读取一段字符串
  * @note   
  * @param  size 要读取的长度
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
void mf_gets(uint16_t size)
{
    TCHAR *rbuf;
    rbuf = f_gets((TCHAR *)fattester.fatbuf, size, fattester.file);

    if (*rbuf == 0)
        return; /* 没有数据读到 */
    else
    {
        printf("\r\nThe String Readed Is:%s\r\n", rbuf);
    }
}

/**
  * @brief  写一个字符到文件(需要 FF_USE_STRFUNC >= 1)
  * @note   
  * @param  c 要写入的字符
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_putc(uint8_t c)
{
    return f_putc((TCHAR)c, fattester.file);
}

/**
  * @brief  写字符串到文件(需要 FF_USE_STRFUNC >= 1)
  * @note   
  * @param  str 要写入的字符串
  * @retval 执行结果(参见FATFS, FRESULT的定义)
  */
uint8_t mf_puts(uint8_t *str)
{
    return f_puts((TCHAR *)str, fattester.file);
}


/**
  * @brief  FatFs文件系统测试函数
  * @note   
  * @param  
  * @retval 
  */
void fatfs_Test(void)
{
    uint8_t res = 0;
    uint8_t i = 0;
    TCHAR drv_num[3] = {0};
    func_fatfs_init(); /* 为fatfs相关变量申请内存 */
    for (i = 0; i < FF_VOLUMES; i++)
    {
        sprintf(drv_num, "%d:", i);
        res = f_mount(gFatfsParam.fs[i], (const TCHAR *)drv_num, 1);
        if (res != FR_OK)
        {
            printf("f_mount %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
            if (res == FR_NO_FILESYSTEM) // FAT文件系统错误,重新格式化，创建文件系统
            {
                printf("Disk Formatting...\r\n"); // 格式化存储介质，创建文件系统
                res = f_mkfs((const TCHAR *)drv_num, 0, 0, FF_MAX_SS);
                if (res == FR_OK)
                {
                    printf("Disk Format Finish!!!\r\n");                  // 格式化完成
                    res = f_mount(NULL, (const TCHAR *)drv_num, 1);                  // 格式化后，先取消挂载
                    res = f_mount(gFatfsParam.fs[i], (const TCHAR *)drv_num, 1); // 重新挂载
                    if (res != FR_OK)
                    {
                        printf("f_mount %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
                        return;
                    }
                }
                else
                {
                    printf("f_mkfs %d failed!!!res=%s\r\n", i, func_fatfs_err(res));
                    return;
                }
            }
        }
        printf("f_mount %d success!!!\r\n", i);
    }

    mf_open("0:SD_test1.txt", FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
    mf_write("This is my sd_test1.txt!!!", 32);
    mf_lseek(0);
    mf_read(32);
    mf_mkdir("0:SD_DIR");
    mf_close();
    mf_scan_files("0:");

    mf_open("1:flash_test1.txt", FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
    mf_write("This is my flash_test1.txt!!!", 32);
    mf_lseek(0);
    mf_read(32);
    mf_mkdir("1:flash_DIR");
    mf_close();

    mf_scan_files("1:");
}


#endif
