/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : font_data.h
 * Author     : 上上签
 * Date       : 2023-05-26
 * Version    : 
 * Description: 
 * ======================================================
 */

#ifndef __FONT_DATA_H__
#define __FONT_DATA_H__

/* 头文件 */

/* 宏定义 */

/* 结构体定义 */
typedef struct __FONT_
{
  const unsigned char *table;
  unsigned short Width;
  unsigned short Height;
} FONT_PARAM;

/* 全局变量声明 */
extern FONT_PARAM font_24x32;
extern FONT_PARAM font_16x24;
extern FONT_PARAM font_8x16;
/* 函数声明 */


#endif
