/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : func_font.h
 * Author     : 上上签
 * Date       : 2023-06-10
 * Version    : 
 * Description: 
 * ======================================================
 */
#ifndef __FUNC_FONT_H__
#define __FUNC_FONT_H__

/* 头文件 */
#include "../FatFs/ff.h"
#include "../Hardware/func_mem.h"
#include "../Hardware/w25qxx.h"
#include "../Hardware/lcd.h"
/* 宏定义 */
// 字库区域占用的总扇区数大小(3个字库+unigbk表+字库信息=3238700字节,约占791个W25QXX扇区)
#define FONTSECSIZE  791
// 字库存放起始地址 
#define FONTINFOADDR 1024*1024*12 // WarShip STM32F103 V3是从12M地址以后开始存放字库
	
/* 结构体定义 */
//字库信息结构体定义, 用来保存字库基本信息，地址，大小等
__packed typedef struct __font_info
{
    uint8_t fontok;     // 字库存在标志，0XAA，字库正常；其他，字库不存在
    uint32_t ugbkaddr;  // unigbk的地址
    uint32_t ugbksize;  // unigbk的大小
    uint32_t f12addr;   // gbk12地址
    uint32_t gbk12size; // gbk12的大小
    uint32_t f16addr;   // gbk16地址
    uint32_t gbk16size; // gbk16的大小
    uint32_t f24addr;   // gbk24地址
    uint32_t gkb24size; // gbk24的大小
} FUNC_FONT_INFO;
/* 全局变量声明 */
extern FUNC_FONT_INFO ftinfo;	//字库信息结构体

/* 函数声明 */

uint32_t func_fupd_prog(uint16_t x, uint16_t y, uint32_t fsize, uint32_t pos);
uint8_t func_updata_fontx(uint16_t x, uint16_t y, uint8_t *fxpath, uint8_t fx);
uint8_t func_update_font(uint16_t x, uint16_t y, uint8_t *src);
uint8_t func_font_init(void);
void func_Get_HzMat(char *code, uint8_t *mat, uint8_t size);

#endif /* __FUNC_FONT_H__ */

