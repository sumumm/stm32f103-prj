/** =====================================================
 * Copyright ? hk. 2022-2025. All rights reserved.
 * File name  : func_font.c
 * Author     : 上上签
 * Date       : 2023-06-10
 * Version    : 
 * Description: 
 * ======================================================
 */
#include "./func_font.h"

//用来保存字库基本信息，地址，大小等
FUNC_FONT_INFO ftinfo;

// 字库存放在磁盘中的路径
uint8_t *const GBK24_PATH = "/SYSTEM/FONT/GBK24.FON";   // GBK24的存放位置
uint8_t *const GBK16_PATH = "/SYSTEM/FONT/GBK16.FON";   // GBK16的存放位置
uint8_t *const GBK12_PATH = "/SYSTEM/FONT/GBK12.FON";   // GBK12的存放位置
uint8_t *const UNIGBK_PATH = "/SYSTEM/FONT/UNIGBK.BIN"; // UNIGBK.BIN的存放位置
 
/**
  * @brief  显示当前字体更新进度
  * @note   
  * @param  x 第几列
  * @param  y 第几行 
  * @param  fsize 整个文件大小
  * @param  pos 当前文件指针位置
  * @retval 
  */
uint32_t func_fupd_prog(uint16_t x, uint16_t y, uint32_t fsize, uint32_t pos)
{
    float prog;
    uint8_t t = 0XFF;
    prog = (float)pos / fsize;
    prog *= 100;
    if (t != prog)
    {
        LCD_ShowString(x+3*lcd_param.LCD_Currentfonts->Width, y, "%");
        t = prog;
        if (t > 100)
        {
            t = 100;
        }
        LCD_ShowNum(x, y, t, 3); // 显示数值
    }
    return 0;
}

/**
  * @brief  更新某一个字库
  * @note   
  * @param  x 第几列
  * @param  y 第几行 
  * @param  fxpath 路径
  * @param  fx 更新的内容 0,ungbk;1,gbk12;2,gbk16;3,gbk24;
  * @retval 0,成功;其他,失败.
  */
uint8_t func_updata_fontx(uint16_t x, uint16_t y, uint8_t *fxpath, uint8_t fx)
{
    uint32_t flashaddr = 0;
    FIL *fftemp;
    uint8_t *tempbuf;
    uint8_t res;
    uint16_t bread;
    uint32_t offx = 0;
    uint8_t rval = 0;
    fftemp = (FIL *)pub_malloc(SRAMIN, sizeof(FIL)); // 分配内存
    if (fftemp == NULL)
        rval = 1;
    tempbuf = (uint8_t *)pub_malloc(SRAMIN, 4096); // 分配4096个字节空间
    if (tempbuf == NULL)
        rval = 1;
    res = f_open(fftemp, (const TCHAR *)fxpath, FA_READ);
    if (res)
        rval = 2; // 打开文件失败

    if (rval == 0)
    {
        switch (fx)
        {
            case 0:                                              // 更新UNIGBK.BIN
                ftinfo.ugbkaddr = FONTINFOADDR + sizeof(ftinfo); // 信息头之后，紧跟UNIGBK转换码表
                ftinfo.ugbksize = f_size(fftemp);                // UNIGBK大小
                flashaddr = ftinfo.ugbkaddr;
                break;
            case 1:
                ftinfo.f12addr = ftinfo.ugbkaddr + ftinfo.ugbksize; // UNIGBK之后，紧跟GBK12字库
                ftinfo.gbk12size = f_size(fftemp);                  // GBK12字库大小
                flashaddr = ftinfo.f12addr;                         // GBK12的起始地址
                break;
            case 2:
                ftinfo.f16addr = ftinfo.f12addr + ftinfo.gbk12size; // GBK12之后，紧跟GBK16字库
                ftinfo.gbk16size = f_size(fftemp);                  // GBK16字库大小
                flashaddr = ftinfo.f16addr;                         // GBK16的起始地址
                break;
            case 3:
                ftinfo.f24addr = ftinfo.f16addr + ftinfo.gbk16size; // GBK16之后，紧跟GBK24字库
                ftinfo.gkb24size = f_size(fftemp);                  // GBK24字库大小
                flashaddr = ftinfo.f24addr;                         // GBK24的起始地址
                break;
        }

        while (res == FR_OK) // 死循环执行
        {
            res = f_read(fftemp, tempbuf, 4096, (UINT *)&bread); // 读取数据
            if (res != FR_OK)
                break;                                     // 执行错误
            W25QXX_Write(tempbuf, offx + flashaddr, 4096); // 从0开始写入4096个数据
            offx += bread;
            func_fupd_prog(x, y, f_size(fftemp), offx); // 进度显示
            if (bread != 4096)
                break; // 读完了.
        }
        f_close(fftemp);
    }
    pub_free(SRAMIN, fftemp);  // 释放内存
    pub_free(SRAMIN, tempbuf); // 释放内存
    return res;
}
		 
/**
  * @brief  更新字体文件,UNIGBK,GBK12,GBK16,GBK24一起更新
  * @note   
  * @param  x 第几列
  * @param  y 第几行 
  * @param  src 字库来源磁盘."0:",SD卡;"1:",FLASH盘,"2:",U盘. 
  * @retval 0,更新成功;其他,错误代码.	 
  */
uint8_t func_update_font(uint16_t x, uint16_t y, uint8_t *src)
{
    uint8_t *pname;
    uint32_t *buf;
    uint8_t res = 0;
    uint16_t i, j;
    FIL *fftemp;
    uint8_t rval = 0;
    res = 0XFF;
    ftinfo.fontok = 0XFF;
    pname = (uint8_t *)pub_malloc(SRAMIN, 100);      // 申请100字节内存
    buf = (uint32_t *)pub_malloc(SRAMIN, 4096);       // 申请4K字节内存
    fftemp = (FIL *)pub_malloc(SRAMIN, sizeof(FIL)); // 分配内存
    if (buf == NULL || pname == NULL || fftemp == NULL)
    {
        pub_free(SRAMIN, fftemp);
        pub_free(SRAMIN, pname);
        pub_free(SRAMIN, buf);
        return 5; // 内存申请失败
    }
    // 先查找文件是否正常
    strcpy((char *)pname, (char *)src); // copy src内容到pname
    strcat((char *)pname, (char *)UNIGBK_PATH);
    res = f_open(fftemp, (const TCHAR *)pname, FA_READ);
    if (res) rval |= 1 << 4;            // 打开文件失败

    strcpy((char *)pname, (char *)src); // copy src内容到pname
    strcat((char *)pname, (char *)GBK12_PATH);
    res = f_open(fftemp, (const TCHAR *)pname, FA_READ);
    if (res) rval |= 1 << 5;            // 打开文件失败

    strcpy((char *)pname, (char *)src); // copy src内容到pname
    strcat((char *)pname, (char *)GBK16_PATH);
    res = f_open(fftemp, (const TCHAR *)pname, FA_READ);
    if (res) rval |= 1 << 6;            // 打开文件失败

    strcpy((char *)pname, (char *)src); // copy src内容到pname
    strcat((char *)pname, (char *)GBK24_PATH);
    res = f_open(fftemp, (const TCHAR *)pname, FA_READ);
    if (res) rval |= 1 << 7;            // 打开文件失败
    pub_free(SRAMIN, fftemp); // 释放内存

    if (rval == 0)          // 字库文件都存在.
    {
        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, "Erasing sectors... "); // 提示正在擦除扇区
        for (i = 0; i < FONTSECSIZE; i++)   // 先擦除字库区域,提高写入速度
        {
            func_fupd_prog(x+20*lcd_param.LCD_Currentfonts->Width, y, FONTSECSIZE, i);                 // 进度显示
            W25QXX_Read((uint8_t *)buf, ((FONTINFOADDR / 4096) + i) * 4096, 4096); // 读出整个扇区的内容
            for (j = 0; j < 1024; j++)                                             // 校验数据
            {
                if (buf[j] != 0XFFFFFFFF)
                    break; // 需要擦除
            }
            if (j != 1024)
                W25QXX_Erase_Sector((FONTINFOADDR / 4096) + i); // 需要擦除的扇区
        }
        pub_free(SRAMIN, buf);

        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, "Updating UNIGBK.BIN");
        strcpy((char *)pname, (char *)src); // copy src内容到pname
        strcat((char *)pname, (char *)UNIGBK_PATH);
        res = func_updata_fontx(x+20*lcd_param.LCD_Currentfonts->Width, y, pname, 0); // 更新UNIGBK.BIN
        if (res)
        {
            pub_free(SRAMIN, pname);
            return 1;
        }

        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, "Updating GBK12.BIN  ");
        strcpy((char *)pname, (char *)src); // copy src内容到pname
        strcat((char *)pname, (char *)GBK12_PATH);
        res = func_updata_fontx(x+20*lcd_param.LCD_Currentfonts->Width, y, pname, 1); // 更新GBK12.FON
        if (res)
        {
            pub_free(SRAMIN, pname);
            return 2;
        }
        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, "Updating GBK16.BIN  ");
        strcpy((char *)pname, (char *)src); // copy src内容到pname
        strcat((char *)pname, (char *)GBK16_PATH);
        res = func_updata_fontx(x+20*lcd_param.LCD_Currentfonts->Width, y, pname, 2); // 更新GBK16.FON
        if (res)
        {
            pub_free(SRAMIN, pname);
            return 3;
        }

        LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
        LCD_ShowString(x, y, "Updating GBK24.BIN  ");
        strcpy((char *)pname, (char *)src); // copy src内容到pname
        strcat((char *)pname, (char *)GBK24_PATH);
        res = func_updata_fontx(x+20*lcd_param.LCD_Currentfonts->Width, y, pname, 3); // 更新GBK24.FON
        if (res)
        {
            pub_free(SRAMIN, pname);
            return 4;
        }

        // 全部更新好了
        ftinfo.fontok = 0XAA;
        W25QXX_Write((uint8_t *)&ftinfo, FONTINFOADDR, sizeof(ftinfo)); // 保存字库信息
    }
    pub_free(SRAMIN, pname); // 释放内存
    pub_free(SRAMIN, buf);
    LCD_Fill(x, y, lcd_param.LCD_X_LENGTH, lcd_param.LCD_Currentfonts->Height, lcd_param.BackColor);
    return rval; // 无错误.
}
	 
/**
  * @brief  初始化字体
  * @note   
  * @param  
  * @retval 0,字库完好.其他,字库丢失
  */
uint8_t func_font_init(void)
{
    uint8_t t = 0;
    Get_W25QXX_Type();
    while (t < 10) // 连续读取10次,都是错误,说明确实是有问题,得更新字库了
    {
        t++;
        W25QXX_Read((uint8_t *)&ftinfo, FONTINFOADDR, sizeof(ftinfo)); // 读出ftinfo结构体数据
        if (ftinfo.fontok == 0XAA)
            break;
        HAL_Delay(20);
    }
    if (ftinfo.fontok != 0XAA)
        return 1;
    return 0;
}

/**
  * @brief  从字库中查找出字模
  * @note   
  * @param  code 字符串的开始地址,GBK码
  * @param  mat 数据存放地址 (size/8+((size%8)?1:0))*(size) bytes大小
  * @param  size 字体大小	
  * @retval 
  */
void func_Get_HzMat(char *code, uint8_t *mat, uint8_t size)
{
    unsigned char qh, ql;
    unsigned char i;
    unsigned long foffset;
    uint8_t csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size); // 得到字体一个字符对应点阵集所占的字节数
    
    // 获取汉字编码的高8位和低8位
    qh = *code;
    ql = *(++code);

    if (qh < 0x81 || ql < 0x40 || ql == 0xff || qh == 0xff) // 非 常用汉字
    {
        for (i = 0; i < csize; i++)
            *mat++ = 0x00; // 填充满格
        return;            // 结束访问
    }
    if (ql < 0x7f)
        ql -= 0x40; // 注意!
    else
        ql -= 0x41;
    qh -= 0x81;

    foffset = ((unsigned long)190 * qh + ql) * csize; // 得到字库中的字节偏移量
    switch (size)
    {
        case 12:
            W25QXX_Read(mat, foffset + ftinfo.f12addr, csize);
            break;
        case 16:
            W25QXX_Read(mat, foffset + ftinfo.f16addr, csize);
            break;
        case 24:
            W25QXX_Read(mat, foffset + ftinfo.f24addr, csize);
            break;
    }
}

