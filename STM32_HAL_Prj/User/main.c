#include "./SYSTEM/sys/sys.h"
#include "./SYSTEM/delay/delay.h"
#include "./SYSTEM/usart/usart.h"

#include "./demo.h"
#include "./MYPRINTF/func_printf.h"
#include "./USMART/usmart.h"

#include "./BSP/LED/led.h"
#include "./BSP/KEY/key.h"
#include "./BSP/LCD/lcd.h"
#include "./BSP/BEEP/beep.h"
#include "./BSP/EXTI/exti.h"
#include "./BSP/TIMER/gtim.h"
#include "./BSP/24CXX/24cxx.h"
#include "./BSP/NORFLASH/norflash.h"
#include "./BSP/TOUCH/touch.h"
#include "./BSP/STMFLASH/stmflash.h"
#include "./BSP/SRAM/sram.h"
#include "./BSP/TPAD/tpad.h"
#include "./BSP/ATK_MC7725F/atk_mc7725f.h"
#include "./MALLOC/malloc.h"
#include "./BSP/SDIO/sdio_sdcard.h"
#include "usbd_storage.h"
#include "./FATFS/exfuns/fattester.h"

/**
 * @brief       显示实验信息
 * @param       无
 * @retval      无
 */
void show_mesg(void)
{
    /* LCD显示实验信息 */

    /* 串口输出实验信息 */
    printf("\r\n");
	printf("******************* help *****************\r\n");
	printf("* author    : %s\r\n", "ssq");
	printf("* build time: %s %s\r\n", __DATE__, __TIME__);
    printf("* platform  : STM32F103ZE\r\n");
	printf("* USART     : USART1,115200\r\n");
	printf("* lcd id    : 0x%x\r\n", lcddev.id);     /* 打印LCD ID */
	printf("* USMART    : USMART can be used!\r\n"); /* USMART组件可用 */
	printf("\r\n");
	printf("* tips:\r\n");
	printf("*      (1)Initialization complete! PRT and PRTE are ready to be used.\r\n");
    printf("******************* end! *****************\r\n");
	
	// 自定义debug宏测试
	PRT("PRT macros can be used!\r\n");
	PRTE("PRTE macros can be used!\r\n\r\n");
}

int main(void)
{
    HAL_Init();                         /* 初始化HAL库 */
    sys_stm32_clock_init(RCC_PLL_MUL9); /* 设置时钟, 72Mhz */
    delay_init(72);                     /* 延时初始化 */
    usart_init(115200);                 /* 串口初始化为115200 */
	usmart_dev.init(72);                /* 初始化USMART */
	led_init();                         /* LED初始化 */
	key_init();                         /* key初始化 */
	lcd_init();                         /* lcd初始化 */
	beep_init();                        /* 初始化蜂鸣器 */
	show_mesg();
    //gtim_timx_int_init(10000 - 1, 7200 - 1); /* 10Khz的计数频率，计数10K次为1000ms */
	
	//gtim_timx_pwm_chy_test();
	//gtim_timx_cap_chy_test();
	//gtim_timx_cnt_chy_test();
	#if USER_FATFS_TEST == 1
	user_fatfs_test();
	#endif
	while(1)
	{
		LED0_TOGGLE();
		delay_ms(500);
	}
}
