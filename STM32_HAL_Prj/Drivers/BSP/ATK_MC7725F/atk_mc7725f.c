/**
 ****************************************************************************************************
 * @file        atk_mc7725f.c
 * @author      xxx
 * @version     Vx.x
 * @date        xxx-xx-xx
 * @brief       ATK-MC7725F模块驱动代码
 * @license     Copyright (c) 2020-2032, xxx
 ****************************************************************************************************
 * @attention
 *
 ****************************************************************************************************
 */

#include "./BSP/ATK_MC7725F/atk_mc7725f.h"
#include "./BSP/ATK_MC7725F/atk_mc7725f_sccb.h"
#include "./BSP/ATK_MC7725F/atk_mc7725f_cfg.h"
#include "./SYSTEM/delay/delay.h"

/* ATK-MC7725F模块制造商ID和产品ID */
#define ATK_MC7725F_MID 0x7FA2
#define ATK_MC7725F_PID 0x7721

/* ATK-MC7725F模块数据结构体 */
static struct
{
    struct {
        uint16_t width;                 /* 宽度 */
        uint16_t height;                /* 高度 */
    } output;                           /* 输出图像大小 */
    struct {
        enum
        {
            FRAME_HANDLE_DONE = 0x00,   /* 处理完成 */
            FRAME_HANDLE_PEND,          /* 等待处理 */
        } handle_flag;                  /* 处理标记 */
        uint16_t count;                 /* 计数 */
    } frame;                            /* 接收图像帧信息 */
} g_atk_mc7725f_sta = {0};

/**
 * @brief       ATK-MC7725F模块写寄存器
 * @param       reg: 寄存器地址
 *              dat: 待写入的值
 * @retval      无
 */
static void atk_mc7725f_write_reg(uint8_t reg, uint8_t dat)
{
    atk_mc7725f_sccb_3_phase_write(ATK_MC7725F_SCCB_ADDR, reg, dat);
}

/**
 * @brief       ATK-MC7725F模块读寄存器
 * @param       reg: 寄存器的地址
 * @retval      读取到的寄存器值
 */
static uint8_t atk_mc7725f_read_reg(uint8_t reg)
{
    uint8_t dat = 0;
    
    atk_mc7725f_sccb_2_phase_write(ATK_MC7725F_SCCB_ADDR, reg);
    atk_mc7725f_sccb_2_phase_read(ATK_MC7725F_SCCB_ADDR, &dat);
    
    return dat;
}

/**
 * @brief       ATK-MC7725F模块硬件初始化
 * @param       无
 * @retval      无
 */
static void atk_mc7725f_hw_init(void)
{
    GPIO_InitTypeDef gpio_init_struct = {0};
    
    /* 使能GPIO时钟 */
    ATK_MC7725F_WRST_GPIO_CLK_ENABLE();
    ATK_MC7725F_RRST_GPIO_CLK_ENABLE();
    ATK_MC7725F_OE_GPIO_CLK_ENABLE();
    ATK_MC7725F_D0_GPIO_CLK_ENABLE();
    ATK_MC7725F_D1_GPIO_CLK_ENABLE();
    ATK_MC7725F_D2_GPIO_CLK_ENABLE();
    ATK_MC7725F_D3_GPIO_CLK_ENABLE();
    ATK_MC7725F_D4_GPIO_CLK_ENABLE();
    ATK_MC7725F_D5_GPIO_CLK_ENABLE();
    ATK_MC7725F_D6_GPIO_CLK_ENABLE();
    ATK_MC7725F_D7_GPIO_CLK_ENABLE();
    ATK_MC7725F_RCLK_GPIO_CLK_ENABLE();
    ATK_MC7725F_WEN_GPIO_CLK_ENABLE();
    ATK_MC7725F_VSYNC_INT_GPIO_CLK_ENABLE();
    
    /* 初始化WRST引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_WRST_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_OUTPUT_PP;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_WRST_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化RRST引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_RRST_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_OUTPUT_PP;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_RRST_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化OE引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_OE_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_OUTPUT_PP;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_OE_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D0引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D0_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D0_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D1引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D1_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D1_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D2引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D2_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D2_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D3引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D3_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D3_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D4引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D4_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D4_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D5引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D5_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D5_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D6引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D6_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D6_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化D7引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_D7_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_INPUT;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_D7_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化RCLK引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_RCLK_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_OUTPUT_PP;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_RCLK_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化WEN引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_WEN_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_OUTPUT_PP;
    gpio_init_struct.Pull   = GPIO_PULLUP;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_WEN_GPIO_PORT, &gpio_init_struct);
    
    /* 初始化VSYNC引脚 */
    gpio_init_struct.Pin    = ATK_MC7725F_VSYNC_INT_GPIO_PIN;
    gpio_init_struct.Mode   = GPIO_MODE_IT_RISING;
    gpio_init_struct.Pull   = GPIO_NOPULL;
    gpio_init_struct.Speed  = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ATK_MC7725F_VSYNC_INT_GPIO_PORT, &gpio_init_struct);
    HAL_NVIC_SetPriority(ATK_MC7725F_VSYNC_INT_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ATK_MC7725F_VSYNC_INT_IRQn);
    
    ATK_MC7725F_WRST(1);
    ATK_MC7725F_RRST(1);
    ATK_MC7725F_OE(1);
    ATK_MC7725F_RCLK(1);
    ATK_MC7725F_WEN(1);
}

/**
 * @brief       ATK-MC7725F模块软件复位
 * @param       无
 * @retval      无
 */
static void atk_mc7725f_sw_reset(void)
{
    atk_mc7725f_write_reg(ATK_MC7725F_REG_COM7, 0x80);
    delay_ms(2);
}

/**
 * @brief       获取ATK-MC7725F模块制造商ID
 * @param       无
 * @retval      制造商ID
 */
static uint16_t atk_mc7725f_get_mid(void)
{
    uint16_t mid;
    
    mid = atk_mc7725f_read_reg(ATK_MC7725F_REG_MIDH) << 8;
    mid |= atk_mc7725f_read_reg(ATK_MC7725F_REG_MIDL);
    
    return mid;
}

/**
 * @brief       获取ATK-MC7725F模块产品ID
 * @param       无
 * @retval      产品ID
 */
static uint16_t atk_mc7725f_get_pid(void)
{
    uint16_t pid;
    
    pid = atk_mc7725f_read_reg(ATK_MC7725F_REG_PID) << 8;
    pid |= atk_mc7725f_read_reg(ATK_MC7725F_REG_VER);
    
    return pid;
}

/**
 * @brief       初始化ATK-MD7725F寄存器配置
 * @param       无
 * @retval      无
 */
static void atk_mc7725f_init_reg(void)
{
    uint8_t cfg_index;
    
    for (cfg_index=0; cfg_index<(sizeof(atk_mc7725f_init_cfg)/sizeof(atk_mc7725f_init_cfg[0])); cfg_index++)
    {
        atk_mc7725f_write_reg(atk_mc7725f_init_cfg[cfg_index][0], atk_mc7725f_init_cfg[cfg_index][1]);
        switch (atk_mc7725f_init_cfg[cfg_index][0])
        {
            case ATK_MC7725F_REG_HSIZE:
            {
                g_atk_mc7725f_sta.output.width = atk_mc7725f_init_cfg[cfg_index][1] << 2;
                break;
            }
            case ATK_MC7725F_REG_VSIZE:
            {
                g_atk_mc7725f_sta.output.height = atk_mc7725f_init_cfg[cfg_index][1] << 1;
                break;
            }
            default:
            {
                break;
            }
        }
    }
}

/**
 * @brief       获取ATK-MD7725F端口D0~D7的一字节数据
 * @param       无
 * @retval      ATK-MD7725F端口D0~D7的一字节数据
 */
static inline uint8_t atk_mc7725f_get_byte_data(void)
{
    uint8_t dat = 0;
    
#if (ATK_MC7725F_DATA_PIN_IN_SAME_GPIO_PORT == 0)
    dat |= (((ATK_MC7725F_D0_GPIO_PORT->IDR & ATK_MC7725F_D0_GPIO_PIN) == 0) ? (0) : (1)) << 0;
    dat |= (((ATK_MC7725F_D1_GPIO_PORT->IDR & ATK_MC7725F_D1_GPIO_PIN) == 0) ? (0) : (1)) << 1;
    dat |= (((ATK_MC7725F_D2_GPIO_PORT->IDR & ATK_MC7725F_D2_GPIO_PIN) == 0) ? (0) : (1)) << 2;
    dat |= (((ATK_MC7725F_D3_GPIO_PORT->IDR & ATK_MC7725F_D3_GPIO_PIN) == 0) ? (0) : (1)) << 3;
    dat |= (((ATK_MC7725F_D4_GPIO_PORT->IDR & ATK_MC7725F_D4_GPIO_PIN) == 0) ? (0) : (1)) << 4;
    dat |= (((ATK_MC7725F_D5_GPIO_PORT->IDR & ATK_MC7725F_D5_GPIO_PIN) == 0) ? (0) : (1)) << 5;
    dat |= (((ATK_MC7725F_D6_GPIO_PORT->IDR & ATK_MC7725F_D6_GPIO_PIN) == 0) ? (0) : (1)) << 6;
    dat |= (((ATK_MC7725F_D7_GPIO_PORT->IDR & ATK_MC7725F_D7_GPIO_PIN) == 0) ? (0) : (1)) << 7;
#else
    dat = ATK_MC7725F_DATE_GPIO_PORT->IDR & ATK_MC7725F_DATA_READ_MASK;
#endif
    
    return dat;
}

/**
 * @brief       初始化ATK-MC7725F模块
 * @param       无
 * @retval      ATK_MC7725F_EOK  : ATK-MC7725模块初始化成功
 *              ATK_MC7725F_ERROR: ATK-MC7725模块初始化失败
 */
uint8_t atk_mc7725f_init(void)
{
    uint16_t mid;
    uint16_t pid;
    
    atk_mc7725f_hw_init();          /* ATK-MC7725F模块硬件初始化 */
    atk_mc7725f_sccb_init();        /* ATK-MC7725F SCCB接口初始化 */
    atk_mc7725f_sw_reset();         /* ATK-MC7725F模块软件复位 */
    
    mid = atk_mc7725f_get_mid();    /* 获取制造商ID */
    if (mid != ATK_MC7725F_MID)
    {
        return ATK_MC7725F_ERROR;
    }
    
    pid = atk_mc7725f_get_pid();    /* 获取产品ID */
    if (pid != ATK_MC7725F_PID)
    {
        return ATK_MC7725F_ERROR;
    }
    
    atk_mc7725f_init_reg();         /* 初始化ATK-MD7725F寄存器配置 */
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块灯光模式
 * @param       mode: ATK_MC7725F_LIGHT_MOED_AUTO  : Auto
 *                    ATK_MC7725F_LIGHT_MOED_SUNNY : Sunny
 *                    ATK_MC7725F_LIGHT_MOED_CLOUDY: Cloudy
 *                    ATK_MC7725F_LIGHT_MOED_OFFICE: Office
 *                    ATK_MC7725F_LIGHT_MOED_HOME  : Home
 *                    ATK_MC7725F_LIGHT_MOED_NIGHT : Night
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725模块灯光模式成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_light_mode(atk_mc7725f_light_mode_t mode)
{
    switch (mode)
    {
        case ATK_MC7725F_LIGHT_MODE_AUTO:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFF);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0x65);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFL, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFH, 0x00);
            break;
        }
        case ATK_MC7725F_LIGHT_MODE_SUNNY:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFD);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BLUE, 0x5A);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_RED, 0x5C);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0x65);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFL, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFH, 0x00);
            break;
        }
        case ATK_MC7725F_LIGHT_MODE_CLOUDY:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFD);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BLUE, 0x58);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_RED, 0x60);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0x65);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFL, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFH, 0x00);
            break;
        }
        case ATK_MC7725F_LIGHT_MODE_OFFICE:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFD);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BLUE, 0x84);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_RED, 0x4C);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0x65);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFL, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFH, 0x00);
            break;
        }
        case ATK_MC7725F_LIGHT_MODE_HOME:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFD);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BLUE, 0x96);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_RED, 0x40);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0x65);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFL, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_ADVFH, 0x00);
            break;
        }
        case ATK_MC7725F_LIGHT_MODE_NIGHT:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM8, 0xFF);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM5, 0xE5);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块色彩饱和度
 * @param       saturation: ATK_MC7725F_COLOR_SATURATION_0: +4
 *                          ATK_MC7725F_COLOR_SATURATION_1: +3
 *                          ATK_MC7725F_COLOR_SATURATION_2: +2
 *                          ATK_MC7725F_COLOR_SATURATION_3: +1
 *                          ATK_MC7725F_COLOR_SATURATION_4: 0
 *                          ATK_MC7725F_COLOR_SATURATION_5: -1
 *                          ATK_MC7725F_COLOR_SATURATION_6: -2
 *                          ATK_MC7725F_COLOR_SATURATION_7: -3
 *                          ATK_MC7725F_COLOR_SATURATION_8: -4
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725F模块色彩饱和度成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_color_saturation(atk_mc7725f_color_saturation_t saturation)
{
    switch (saturation)
    {
        case ATK_MC7725F_COLOR_SATURATION_0:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x80);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x80);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_1:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x70);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x70);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_2:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x60);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x60);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_3:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x50);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x50);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_4:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x40);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x40);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_5:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x30);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x30);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_6:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x20);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x20);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_7:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x10);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x10);
            break;
        }
        case ATK_MC7725F_COLOR_SATURATION_8:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_USAT, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSAT, 0x00);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块亮度
 * @param       brightness: ATK_MC7725F_BRIGHTNESS_0: +4
 *                          ATK_MC7725F_BRIGHTNESS_1: +3
 *                          ATK_MC7725F_BRIGHTNESS_2: +2
 *                          ATK_MC7725F_BRIGHTNESS_3: +1
 *                          ATK_MC7725F_BRIGHTNESS_4: 0
 *                          ATK_MC7725F_BRIGHTNESS_5: -1
 *                          ATK_MC7725F_BRIGHTNESS_6: -2
 *                          ATK_MC7725F_BRIGHTNESS_7: -3
 *                          ATK_MC7725F_BRIGHTNESS_8: -4
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725F模块亮度成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_brightness(atk_mc7725f_brightness_t brightness)
{
    switch (brightness)
    {
        case ATK_MC7725F_BRIGHTNESS_0:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x48);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x06);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_1:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x38);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x06);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_2:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x28);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x06);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_3:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x18);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x06);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_4:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x08);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x06);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_5:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x08);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x0E);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_6:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x18);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x0E);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_7:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x28);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x0E);
            break;
        }
        case ATK_MC7725F_BRIGHTNESS_8:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_BRIGHT, 0x38);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SIGN, 0x0E);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块对比度
 * @param       contrast: ATK_MC7725F_CONTRAST_0: +4
 *                        ATK_MC7725F_CONTRAST_1: +3
 *                        ATK_MC7725F_CONTRAST_2: +2
 *                        ATK_MC7725F_CONTRAST_3: +1
 *                        ATK_MC7725F_CONTRAST_4: 0
 *                        ATK_MC7725F_CONTRAST_5: -1
 *                        ATK_MC7725F_CONTRAST_6: -2
 *                        ATK_MC7725F_CONTRAST_7: -3
 *                        ATK_MC7725F_CONTRAST_8: -4
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725F模块对比度成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_contrast(atk_mc7725f_contrast_t contrast)
{
    switch (contrast)
    {
        case ATK_MC7725F_CONTRAST_0:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x30);
            break;
        }
        case ATK_MC7725F_CONTRAST_1:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x2C);
            break;
        }
        case ATK_MC7725F_CONTRAST_2:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x28);
            break;
        }
        case ATK_MC7725F_CONTRAST_3:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x24);
            break;
        }
        case ATK_MC7725F_CONTRAST_4:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x20);
            break;
        }
        case ATK_MC7725F_CONTRAST_5:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x1C);
            break;
        }
        case ATK_MC7725F_CONTRAST_6:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x18);
            break;
        }
        case ATK_MC7725F_CONTRAST_7:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x14);
            break;
        }
        case ATK_MC7725F_CONTRAST_8:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_CNST, 0x10);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块特殊效果
 * @param       contrast: ATK_MC7725F_SPECIAL_EFFECT_NORMAL  : Normal
 *                        ATK_MC7725F_SPECIAL_EFFECT_BW      : B&W
 *                        ATK_MC7725F_SPECIAL_EFFECT_BLUISH  : Bluish
 *                        ATK_MC7725F_SPECIAL_EFFECT_SEPIA   : Sepia
 *                        ATK_MC7725F_SPECIAL_EFFECT_REDISH  : Redish
 *                        ATK_MC7725F_SPECIAL_EFFECT_GREENISH: Greenish
 *                        ATK_MC7725F_SPECIAL_EFFECT_NEGATIE : Negative
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725F模块特殊效果成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_special_effect(atk_mc7725f_special_effect_t effect)
{
    switch (effect)
    {
        case ATK_MC7725F_SPECIAL_EFFECT_NORMAL:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x06);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0x80);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0x80);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_BW:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x26);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0x80);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0x80);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_BLUISH:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x1E);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0xA0);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0x40);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_SEPIA:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x1E);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0x40);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0xA0);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_REDISH:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x1E);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0x80);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0xC0);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_GREENISH:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x1E);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_UFIX, 0x60);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VFIX, 0x60);
            break;
        }
        case ATK_MC7725F_SPECIAL_EFFECT_NEGATIVE:
        {
            atk_mc7725f_write_reg(ATK_MC7725F_REG_SDE, 0x46);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       设置ATK-MC7725F模块输出模式
 * @param       width : 输出图像宽度（VGA，<=640；QVGA，<=320）
 *              height: 输出图像高度（VGA，<=480；QVGA，<=240）
 *              mode  : ATK_MC7725F_OUTPUT_MODE_VGA : VGA输出模式
 *                      ATK_MC7725F_OUTPUT_MODE_QVGA: QVGA输出模式
 * @retval      ATK_MC7725F_EOK   : 设置ATK-MC7725F模块输出模式成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 */
uint8_t atk_mc7725f_set_output(uint16_t width, uint16_t height, atk_mc7725f_output_mode_t mode)
{
    uint16_t xs;
    uint16_t ys;
    uint8_t hstart_raw;
    uint8_t hstart_new;
    uint8_t vstrt_raw;
    uint8_t vstrt_new;
    uint8_t href_raw;
    uint8_t href_new;
    uint8_t exhch;
    
    switch (mode)
    {
        case ATK_MC7725F_OUTPUT_MODE_VGA:
        {
            if ((width > ATK_MC7725F_VGA_WIDTH_MAX) || (height > ATK_MC7725F_VGA_HEIGHT_MAX))
            {
                return ATK_MC7725F_EINVAL;
            }
            break;
        }
        case ATK_MC7725F_OUTPUT_MODE_QVGA:
        {
            if ((width > ATK_MC7725F_QVGA_WIDTH_MAX) || (height > ATK_MC7725F_QVGA_HEIGHT_MAX))
            {
                return ATK_MC7725F_EINVAL;
            }
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    switch (mode)
    {
        case ATK_MC7725F_OUTPUT_MODE_VGA:
        {
            xs = (ATK_MC7725F_VGA_WIDTH_MAX - width) >> 1;
            ys = (ATK_MC7725F_VGA_HEIGHT_MAX - height) >> 1;
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM7, 0x06);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HSTART, 0x23);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HSIZE, 0xA0);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSTRT, 0x07);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSIZE, 0xF0);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HREF, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HOutSize, 0xA0);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VOutSize, 0xF0);
            break;
        }
        case ATK_MC7725F_OUTPUT_MODE_QVGA:
        {
            xs = (ATK_MC7725F_QVGA_WIDTH_MAX - width) >> 1;
            ys = (ATK_MC7725F_QVGA_HEIGHT_MAX - height) >> 1;
            atk_mc7725f_write_reg(ATK_MC7725F_REG_COM7, 0x46);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HSTART, 0x3F);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HSIZE, 0x50);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSTRT, 0x03);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VSIZE, 0x78);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HREF, 0x00);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_HOutSize, 0x50);
            atk_mc7725f_write_reg(ATK_MC7725F_REG_VOutSize, 0x78);
            break;
        }
        default:
        {
            return ATK_MC7725F_EINVAL;
        }
    }
    
    hstart_raw = atk_mc7725f_read_reg(ATK_MC7725F_REG_HSTART);
    hstart_new = hstart_raw + (xs >> 2);
    atk_mc7725f_write_reg(ATK_MC7725F_REG_HSTART, hstart_new);
    atk_mc7725f_write_reg(ATK_MC7725F_REG_HSIZE, width >> 2);
    g_atk_mc7725f_sta.output.width = atk_mc7725f_read_reg(ATK_MC7725F_REG_HSIZE) << 2;
    
    vstrt_raw = atk_mc7725f_read_reg(ATK_MC7725F_REG_VSTRT);
    vstrt_new = vstrt_raw + (ys >> 1);
    atk_mc7725f_write_reg(ATK_MC7725F_REG_VSTRT, vstrt_new);
    atk_mc7725f_write_reg(ATK_MC7725F_REG_VSIZE, height >> 1);
    g_atk_mc7725f_sta.output.height = atk_mc7725f_read_reg(ATK_MC7725F_REG_VSIZE) << 1;
    
    href_raw = atk_mc7725f_read_reg(ATK_MC7725F_REG_HREF);
    href_new = ((ys & 0x01) << 6) | ((xs & 0x03) << 4) | ((height & 0x01) << 2) | (width & 0x03) | href_raw;
    atk_mc7725f_write_reg(ATK_MC7725F_REG_HREF, href_new);
    
    atk_mc7725f_write_reg(ATK_MC7725F_REG_HOutSize, width >> 2);
    atk_mc7725f_write_reg(ATK_MC7725F_REG_VOutSize, height >> 1);
    
    exhch = (href_raw | (width & 0x03) | ((height & 0x01) << 2));
    atk_mc7725f_write_reg(ATK_MC7725F_REG_EXHCH, exhch);
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       使能ATK-MD7725F模块输出图像
 * @param       无
 * @retval      无
 */
void atk_mc7725f_enable_output(void)
{
    ATK_MC7725F_OE(0);
}

/**
 * @brief       禁止ATK-MD7725F模块输出图像
 * @param       无
 * @retval      无
 */
void atk_mc7725f_disable_output(void)
{
    ATK_MC7725F_OE(1);
}

/**
 * @brief       获取ATK-MC7725F模块输出图像宽度
 * @param       无
 * @retval      ATK-MC7725F模块输出图像宽度
 */
uint16_t atk_mc7725f_get_output_width(void)
{
    return g_atk_mc7725f_sta.output.width;
}

/**
 * @brief       获取ATK-MC7725F模块输出图像高度
 * @param       无
 * @retval      ATK-MC7725F模块输出图像高度
 */
uint16_t atk_mc7725f_get_output_height(void)
{
    return g_atk_mc7725f_sta.output.height;
}

/**
 * @brief       获取ATK-MC7725F模块输出的一帧图像数据
 * @param       dts : 图像数据返回首地址
 *              type: ATK_MC7725F_GET_FRAME_TYPE_NOINC   : 目的地址固定
 *                    ATK_MC7725F_GET_FRAME_TYPE_AUTO_INC: 目的地址自动自增
 * @retval      ATK_MC7725F_EOK   : 获取ATK-MC7725F模块输出的一帧图像数据成功
 *              ATK_MC7725F_EINVAL: 传入参数错误
 *              ATK_MC7725F_EEMPTY: 图像数据为空
 */
uint8_t atk_mc7725f_get_frame(volatile uint16_t *dts, atk_mc7725f_get_frame_type_t type)
{
    uint16_t width_index;
    uint16_t height_index;
    uint16_t dat;
    
    if (g_atk_mc7725f_sta.frame.handle_flag == FRAME_HANDLE_DONE)
    {
        return ATK_MC7725F_EEMPTY;
    }
    
    ATK_MC7725F_RRST(0);
    ATK_MC7725F_RCLK(0);
    ATK_MC7725F_RCLK(1);
    ATK_MC7725F_RCLK(0);
    ATK_MC7725F_RRST(1);
    ATK_MC7725F_RCLK(1);
    for (height_index=0; height_index<g_atk_mc7725f_sta.output.height; height_index++)
    {
        for (width_index=0; width_index<g_atk_mc7725f_sta.output.width; width_index++)
        {
            ATK_MC7725F_RCLK(0);
            dat = (atk_mc7725f_get_byte_data() << 8);
            ATK_MC7725F_RCLK(1);
            ATK_MC7725F_RCLK(0);
            dat |= atk_mc7725f_get_byte_data();
            ATK_MC7725F_RCLK(1);
            *dts = dat;
            switch (type)
            {
                case ATK_MC7725F_GET_FRAME_TYPE_NOINC:
                {
                    break;
                }
                case ATK_MC7725F_GET_FRAME_TYPE_AUTO_INC:
                {
                    dts++;
                    break;
                }
                default:
                {
                    return ATK_MC7725F_EINVAL;
                }
            }
        }
    }
    
    g_atk_mc7725f_sta.frame.handle_flag = FRAME_HANDLE_DONE;
    g_atk_mc7725f_sta.frame.count++;
    
    return ATK_MC7725F_EOK;
}

/**
 * @brief       ATK-MD7725F VSYNC外部中断服务函数
 * @param       无
 * @retval      无
 */
void ATK_MC7725F_VSYNC_INT_IRQHandler(void)
{
    if (__HAL_GPIO_EXTI_GET_IT(ATK_MC7725F_VSYNC_INT_GPIO_PIN) != RESET)
    {
        if (g_atk_mc7725f_sta.frame.handle_flag == FRAME_HANDLE_DONE)
        {
            ATK_MC7725F_WRST(0);
            ATK_MC7725F_WEN(1);
            ATK_MC7725F_WRST(1);
            g_atk_mc7725f_sta.frame.handle_flag = FRAME_HANDLE_PEND;
        }
        else
        {
            ATK_MC7725F_WEN(0);
        }
        
        __HAL_GPIO_EXTI_CLEAR_IT(ATK_MC7725F_VSYNC_INT_GPIO_PIN);
    }
}


#if ATK_MC7725F_TEST == 1
#include "./BSP/LED/led.h"
#include "./SYSTEM/USART/usart.h"
#include "./BSP/LCD/lcd.h"

/**
 * @brief       例程演示入口函数
 * @param       无
 * @retval      无
 */
void atk_mc7725f_camera_test(void)
{
    uint8_t ret;
    
    ret = atk_mc7725f_init();   /* 初始化初始化ATK-MC7725F模块 */
    if (ret != 0)
    {
        printf("ATK-MC7725F Init failed!\r\n");
        while (1)
        {
            LED0_TOGGLE();
            delay_ms(200);
        }
    }
    
    /* 配置ATK-MC7725F模块 */
    ret  = atk_mc7725f_set_output(ATK_MC7725F_QVGA_WIDTH_MAX, ATK_MC7725F_QVGA_HEIGHT_MAX, ATK_MC7725F_OUTPUT_MODE_QVGA);
    ret += atk_mc7725f_set_light_mode(ATK_MC7725F_LIGHT_MODE_AUTO);
    ret += atk_mc7725f_set_color_saturation(ATK_MC7725F_COLOR_SATURATION_4);
    ret += atk_mc7725f_set_brightness(ATK_MC7725F_BRIGHTNESS_4);
    ret += atk_mc7725f_set_contrast(ATK_MC7725F_CONTRAST_4);
    ret += atk_mc7725f_set_special_effect(ATK_MC7725F_SPECIAL_EFFECT_NORMAL);
    if (ret != 0)
    {
        printf("ATK-MC7725F Config failed!\r\n");
        while (1)
        {
            LED0_TOGGLE();
            delay_ms(200);
        }
    }
    atk_mc7725f_enable_output();
    
    /* 配置LCD */
    lcd_scan_dir(U2D_L2R);
    lcd_set_window(0, 0, atk_mc7725f_get_output_width(), atk_mc7725f_get_output_height());
    lcd_write_ram_prepare();
    
    while (1)
    {
        /* 将ATK-MC7725F模块输出的图像显示到LCD上 */
        atk_mc7725f_get_frame(&(LCD->LCD_RAM), ATK_MC7725F_GET_FRAME_TYPE_NOINC);
    }
}
#endif

