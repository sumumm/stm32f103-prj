/**
 ****************************************************************************************************
 * @file        atk_mc7725f_cfg.h
 * @author      xxx
 * @version     Vx.x
 * @date        xxx-xx-xx
 * @brief       ATK-MC7725F�Ĵ�������
 * @license     Copyright (c) 2020-2032, xxx
 ****************************************************************************************************
 * @attention
 *
 ****************************************************************************************************
 */

#ifndef __ATK_MC7725F_CFG_H
#define __ATK_MC7725F_CFG_H

#include "./BSP/ATK_MC7725F/atk_mc7725f.h"

#define ATK_MC7725F_REG_BLUE        0x01
#define ATK_MC7725F_REG_RED         0x02
#define ATK_MC7725F_REG_PID         0x0A
#define ATK_MC7725F_REG_VER         0x0B
#define ATK_MC7725F_REG_COM3        0x0C
#define ATK_MC7725F_REG_COM4        0x0D
#define ATK_MC7725F_REG_COM5        0x0E
#define ATK_MC7725F_REG_COM6        0x0F
#define ATK_MC7725F_REG_CLKRC       0x11
#define ATK_MC7725F_REG_COM7        0x12
#define ATK_MC7725F_REG_COM8        0x13
#define ATK_MC7725F_REG_COM9        0x14
#define ATK_MC7725F_REG_HSTART      0x17
#define ATK_MC7725F_REG_HSIZE       0x18
#define ATK_MC7725F_REG_VSTRT       0x19
#define ATK_MC7725F_REG_VSIZE       0x1A
#define ATK_MC7725F_REG_MIDH        0x1C
#define ATK_MC7725F_REG_MIDL        0x1D
#define ATK_MC7725F_REG_BDBase      0x22
#define ATK_MC7725F_REG_BDMStep     0x23
#define ATK_MC7725F_REG_AEW         0x24
#define ATK_MC7725F_REG_AEB         0x25
#define ATK_MC7725F_REG_VPT         0x26
#define ATK_MC7725F_REG_HOutSize    0x29
#define ATK_MC7725F_REG_EXHCH       0x2A
#define ATK_MC7725F_REG_EXHCL       0x2B
#define ATK_MC7725F_REG_VOutSize    0x2C
#define ATK_MC7725F_REG_ADVFL       0x2D
#define ATK_MC7725F_REG_ADVFH       0x2E
#define ATK_MC7725F_REG_HREF        0x32
#define ATK_MC7725F_REG_TGT_B       0x42
#define ATK_MC7725F_REG_FixGain     0x4D
#define ATK_MC7725F_REG_UFIX        0x60
#define ATK_MC7725F_REG_VFIX        0x61
#define ATK_MC7725F_REG_AWB_Ctrl0   0x63
#define ATK_MC7725F_REG_DSP_Ctrl1   0x64
#define ATK_MC7725F_REG_DSP_Ctrl2   0x65
#define ATK_MC7725F_REG_DSP_Ctrl3   0x66
#define ATK_MC7725F_REG_DSP_Ctrl4   0x67
#define ATK_MC7725F_REG_AWBCtrl3    0x6B
#define ATK_MC7725F_REG_GAM1        0x7E
#define ATK_MC7725F_REG_GAM2        0x7F
#define ATK_MC7725F_REG_GAM3        0x80
#define ATK_MC7725F_REG_GAM4        0x81
#define ATK_MC7725F_REG_GAM5        0x82
#define ATK_MC7725F_REG_GAM6        0x83
#define ATK_MC7725F_REG_GAM7        0x84
#define ATK_MC7725F_REG_GAM8        0x85
#define ATK_MC7725F_REG_GAM9        0x86
#define ATK_MC7725F_REG_GAM10       0x87
#define ATK_MC7725F_REG_GAM11       0x88
#define ATK_MC7725F_REG_GAM12       0x89
#define ATK_MC7725F_REG_GAM13       0x8A
#define ATK_MC7725F_REG_GAM14       0x8B
#define ATK_MC7725F_REG_GAM15       0x8C
#define ATK_MC7725F_REG_SLOP        0x8D
#define ATK_MC7725F_REG_EDGE1       0x90
#define ATK_MC7725F_REG_DNSOff      0x91
#define ATK_MC7725F_REG_EDGE2       0x92
#define ATK_MC7725F_REG_EDGE3       0x93
#define ATK_MC7725F_REG_MTX1        0x94
#define ATK_MC7725F_REG_MTX2        0x95
#define ATK_MC7725F_REG_MTX3        0x96
#define ATK_MC7725F_REG_MTX4        0x97
#define ATK_MC7725F_REG_MTX5        0x98
#define ATK_MC7725F_REG_MTX6        0x99
#define ATK_MC7725F_REG_MTX_Ctrl    0x9A
#define ATK_MC7725F_REG_BRIGHT      0x9B
#define ATK_MC7725F_REG_CNST        0x9C
#define ATK_MC7725F_REG_UVADJ0      0x9E
#define ATK_MC7725F_REG_SDE         0xA6
#define ATK_MC7725F_REG_USAT        0xA7
#define ATK_MC7725F_REG_VSAT        0xA8
#define ATK_MC7725F_REG_HUECOS      0xA9
#define ATK_MC7725F_REG_HUESIN      0xAA
#define ATK_MC7725F_REG_SIGN        0xAB

const uint8_t atk_mc7725f_init_cfg[][2] = {
    {ATK_MC7725F_REG_CLKRC,     0x00},  /* �Ĵ���CLKRC */
    {ATK_MC7725F_REG_COM7,      0x46},  /* �Ĵ���COM7 */
    {ATK_MC7725F_REG_HSTART,    0x3F},  /* �Ĵ���HSTART */
    {ATK_MC7725F_REG_HSIZE,     0x50},  /* �Ĵ���HSIZE */
    {ATK_MC7725F_REG_VSTRT,     0x03},  /* �Ĵ���VSTRT */
    {ATK_MC7725F_REG_VSIZE,     0x78},  /* �Ĵ���VSIZE */
    {ATK_MC7725F_REG_HREF,      0x00},  /* �Ĵ���HREF */
    {ATK_MC7725F_REG_HOutSize,  0x50},  /* �Ĵ���HOutSize */
    {ATK_MC7725F_REG_VOutSize,  0x78},  /* �Ĵ���VOutSize */
    {ATK_MC7725F_REG_TGT_B,     0x7F},  /* �Ĵ���TGT_B */
    {ATK_MC7725F_REG_FixGain,   0x09},  /* �Ĵ���FixGain */
    {ATK_MC7725F_REG_AWB_Ctrl0, 0xE0},  /* �Ĵ���AWB_Ctrl0 */
    {ATK_MC7725F_REG_DSP_Ctrl1, 0xFF},  /* �Ĵ���DSP_Ctrl1 */
    {ATK_MC7725F_REG_DSP_Ctrl2, 0x00},  /* �Ĵ���DSP_Ctrl2 */
    {ATK_MC7725F_REG_DSP_Ctrl3, 0x00},  /* �Ĵ���DSP_Ctrl3 */
    {ATK_MC7725F_REG_DSP_Ctrl4, 0x00},  /* �Ĵ���DSP_Ctrl4 */
    {ATK_MC7725F_REG_COM8,      0xF0},  /* �Ĵ���COM8 */
    {ATK_MC7725F_REG_COM4,      0xC1},  /* �Ĵ���COM4 */
    {ATK_MC7725F_REG_COM6,      0xC5},  /* �Ĵ���COM6 */
    {ATK_MC7725F_REG_COM9,      0x11},  /* �Ĵ���COM9 */
    {ATK_MC7725F_REG_BDBase,    0x7F},  /* �Ĵ���BDBase */
    {ATK_MC7725F_REG_BDMStep,   0x03},  /* �Ĵ���BDMStep */
    {ATK_MC7725F_REG_AEW,       0x40},  /* �Ĵ���AEW */
    {ATK_MC7725F_REG_AEB,       0x30},  /* �Ĵ���AEB */
    {ATK_MC7725F_REG_VPT,       0xA1},  /* �Ĵ���VPT */
    {ATK_MC7725F_REG_EXHCL,     0x9E},  /* �Ĵ���EXHCL */
    {ATK_MC7725F_REG_AWBCtrl3,  0xAA},  /* �Ĵ���AWBCtrl3 */
    {ATK_MC7725F_REG_COM8,      0xFF},  /* �Ĵ���COM8 */
    {ATK_MC7725F_REG_EDGE1,     0x08},  /* �Ĵ���EDGE1 */
    {ATK_MC7725F_REG_DNSOff,    0x01},  /* �Ĵ���DNSOff */
    {ATK_MC7725F_REG_EDGE2,     0x03},  /* �Ĵ���EDGE2 */
    {ATK_MC7725F_REG_EDGE3,     0x00},  /* �Ĵ���EDGE3 */
    {ATK_MC7725F_REG_MTX1,      0xB0},  /* �Ĵ���MTX1 */
    {ATK_MC7725F_REG_MTX2,      0x9D},  /* �Ĵ���MTX2 */
    {ATK_MC7725F_REG_MTX3,      0x13},  /* �Ĵ���MTX3 */
    {ATK_MC7725F_REG_MTX4,      0x16},  /* �Ĵ���MTX4 */
    {ATK_MC7725F_REG_MTX5,      0x7B},  /* �Ĵ���MTX5 */
    {ATK_MC7725F_REG_MTX6,      0x91},  /* �Ĵ���MTX6 */
    {ATK_MC7725F_REG_MTX_Ctrl,  0x1E},  /* �Ĵ���MTX_Ctrl */
    {ATK_MC7725F_REG_BRIGHT,    0x08},  /* �Ĵ���BRIGHT */
    {ATK_MC7725F_REG_CNST,      0x20},  /* �Ĵ���CNST */
    {ATK_MC7725F_REG_UVADJ0,    0x81},  /* �Ĵ���UVADJ0 */
    {ATK_MC7725F_REG_SDE,       0x06},  /* �Ĵ���SDE */
    {ATK_MC7725F_REG_USAT,      0x65},  /* �Ĵ���USAT */
    {ATK_MC7725F_REG_VSAT,      0x65},  /* �Ĵ���VSAT */
    {ATK_MC7725F_REG_HUECOS,    0x80},  /* �Ĵ���HUECOS */
    {ATK_MC7725F_REG_HUESIN,    0x80},  /* �Ĵ���HUESIN */
    {ATK_MC7725F_REG_GAM1,      0x0C},  /* �Ĵ���GAM1 */
    {ATK_MC7725F_REG_GAM2,      0x16},  /* �Ĵ���GAM2 */
    {ATK_MC7725F_REG_GAM3,      0x2A},  /* �Ĵ���GAM3 */
    {ATK_MC7725F_REG_GAM4,      0x4E},  /* �Ĵ���GAM4 */
    {ATK_MC7725F_REG_GAM5,      0x61},  /* �Ĵ���GAM5 */
    {ATK_MC7725F_REG_GAM6,      0x6F},  /* �Ĵ���GAM6 */
    {ATK_MC7725F_REG_GAM7,      0x7B},  /* �Ĵ���GAM7 */
    {ATK_MC7725F_REG_GAM8,      0x86},  /* �Ĵ���GAM8 */
    {ATK_MC7725F_REG_GAM9,      0x8E},  /* �Ĵ���GAM9 */
    {ATK_MC7725F_REG_GAM10,     0x97},  /* �Ĵ���GAM10 */
    {ATK_MC7725F_REG_GAM11,     0xA4},  /* �Ĵ���GAM11 */
    {ATK_MC7725F_REG_GAM12,     0xAF},  /* �Ĵ���GAM12 */
    {ATK_MC7725F_REG_GAM13,     0xC5},  /* �Ĵ���GAM13 */
    {ATK_MC7725F_REG_GAM14,     0xD7},  /* �Ĵ���GAM14 */
    {ATK_MC7725F_REG_GAM15,     0xE8},  /* �Ĵ���GAM15 */
    {ATK_MC7725F_REG_SLOP,      0x20},  /* �Ĵ���SLOP */
    {ATK_MC7725F_REG_COM3,      0x50},  /* �Ĵ���COM3 */
    {ATK_MC7725F_REG_COM5,      0xF5},  /* �Ĵ���COM5 */
};

#endif
