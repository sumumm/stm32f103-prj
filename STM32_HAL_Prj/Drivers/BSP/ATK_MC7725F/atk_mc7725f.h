/**
 ****************************************************************************************************
 * @file        atk_mc7725f.h
 * @author      xxx
 * @version     Vx.x
 * @date        xxx-xx-xx
 * @brief       ATK-MC7725F模块驱动代码
 * @license     Copyright (c) 2020-2032, xxx
 ****************************************************************************************************
 * @attention
 *
 ****************************************************************************************************
 */

#ifndef __ATK_MC7725F_H
#define __ATK_MC7725F_H

#include "./SYSTEM/sys/sys.h"

/* 定义是否使用同一GPIO端口连接ATK-MC7725F的D0~D7数据引脚 */
#define ATK_MC7725F_DATA_PIN_IN_SAME_GPIO_PORT  1

#if (ATK_MC7725F_DATA_PIN_IN_SAME_GPIO_PORT != 0)
/* 连接ATK-MC7725F模块D0~D7的GPIO端口 */
#define ATK_MC7725F_DATE_GPIO_PORT  GPIOC
/* 一次性读取连接至ATK-MC7725F的D0~D7的GPIO引脚数据的掩码 */
#define ATK_MC7725F_DATA_READ_MASK  0x00FF
#endif

/* 引脚定义 */
#define ATK_MC7725F_WRST_GPIO_PORT              GPIOD
#define ATK_MC7725F_WRST_GPIO_PIN               GPIO_PIN_6
#define ATK_MC7725F_WRST_GPIO_CLK_ENABLE()      do{ __HAL_RCC_GPIOD_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_RRST_GPIO_PORT              GPIOG
#define ATK_MC7725F_RRST_GPIO_PIN               GPIO_PIN_14
#define ATK_MC7725F_RRST_GPIO_CLK_ENABLE()      do{ __HAL_RCC_GPIOG_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_OE_GPIO_PORT                GPIOG
#define ATK_MC7725F_OE_GPIO_PIN                 GPIO_PIN_15
#define ATK_MC7725F_OE_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOG_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D0_GPIO_PORT                GPIOC
#define ATK_MC7725F_D0_GPIO_PIN                 GPIO_PIN_0
#define ATK_MC7725F_D0_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D1_GPIO_PORT                GPIOC
#define ATK_MC7725F_D1_GPIO_PIN                 GPIO_PIN_1
#define ATK_MC7725F_D1_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D2_GPIO_PORT                GPIOC
#define ATK_MC7725F_D2_GPIO_PIN                 GPIO_PIN_2
#define ATK_MC7725F_D2_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D3_GPIO_PORT                GPIOC
#define ATK_MC7725F_D3_GPIO_PIN                 GPIO_PIN_3
#define ATK_MC7725F_D3_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D4_GPIO_PORT                GPIOC
#define ATK_MC7725F_D4_GPIO_PIN                 GPIO_PIN_4
#define ATK_MC7725F_D4_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D5_GPIO_PORT                GPIOC
#define ATK_MC7725F_D5_GPIO_PIN                 GPIO_PIN_5
#define ATK_MC7725F_D5_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D6_GPIO_PORT                GPIOC
#define ATK_MC7725F_D6_GPIO_PIN                 GPIO_PIN_6
#define ATK_MC7725F_D6_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_D7_GPIO_PORT                GPIOC
#define ATK_MC7725F_D7_GPIO_PIN                 GPIO_PIN_7
#define ATK_MC7725F_D7_GPIO_CLK_ENABLE()        do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_RCLK_GPIO_PORT              GPIOB
#define ATK_MC7725F_RCLK_GPIO_PIN               GPIO_PIN_4
#define ATK_MC7725F_RCLK_GPIO_CLK_ENABLE()      do{ __HAL_RCC_GPIOB_CLK_ENABLE();   \
                                                    __HAL_RCC_AFIO_CLK_ENABLE();    \
                                                    __HAL_AFIO_REMAP_SWJ_NOJTAG();  \
                                                }while(0)
#define ATK_MC7725F_WEN_GPIO_PORT               GPIOB
#define ATK_MC7725F_WEN_GPIO_PIN                GPIO_PIN_3
#define ATK_MC7725F_WEN_GPIO_CLK_ENABLE()       do{ __HAL_RCC_GPIOB_CLK_ENABLE();   \
                                                    __HAL_RCC_AFIO_CLK_ENABLE();    \
                                                    __HAL_AFIO_REMAP_SWJ_NOJTAG();  \
                                                }while(0)
#define ATK_MC7725F_VSYNC_INT_GPIO_PORT         GPIOA
#define ATK_MC7725F_VSYNC_INT_GPIO_PIN          GPIO_PIN_8
#define ATK_MC7725F_VSYNC_INT_GPIO_CLK_ENABLE() do{ __HAL_RCC_GPIOA_CLK_ENABLE(); }while(0)
#define ATK_MC7725F_VSYNC_INT_IRQn              EXTI9_5_IRQn
#define ATK_MC7725F_VSYNC_INT_IRQHandler        EXTI9_5_IRQHandler

/* IO操作 */
#define ATK_MC7725F_WRST(x)                     do{ x ?                                                                                         \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_WRST_GPIO_PORT, ATK_MC7725F_WRST_GPIO_PIN, GPIO_PIN_SET) :    \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_WRST_GPIO_PORT, ATK_MC7725F_WRST_GPIO_PIN, GPIO_PIN_RESET);   \
                                                }while(0)
#define ATK_MC7725F_RRST(x)                     do{ x ?                                                                                         \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_RRST_GPIO_PORT, ATK_MC7725F_RRST_GPIO_PIN, GPIO_PIN_SET) :    \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_RRST_GPIO_PORT, ATK_MC7725F_RRST_GPIO_PIN, GPIO_PIN_RESET);   \
                                                }while(0)
#define ATK_MC7725F_OE(x)                       do{ x ?                                                                                         \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_OE_GPIO_PORT, ATK_MC7725F_OE_GPIO_PIN, GPIO_PIN_SET) :        \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_OE_GPIO_PORT, ATK_MC7725F_OE_GPIO_PIN, GPIO_PIN_RESET);       \
                                                }while(0)
#define ATK_MC7725F_RCLK(x)                     do{ x ?                                                                                         \
                                                    (ATK_MC7725F_RCLK_GPIO_PORT->BSRR = (uint32_t)ATK_MC7725F_RCLK_GPIO_PIN) :                  \
                                                    (ATK_MC7725F_RCLK_GPIO_PORT->BRR = (uint32_t)ATK_MC7725F_RCLK_GPIO_PIN);                    \
                                                }while(0)
#define ATK_MC7725F_WEN(x)                      do{ x ?                                                                                         \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_WEN_GPIO_PORT, ATK_MC7725F_WEN_GPIO_PIN, GPIO_PIN_SET) :      \
                                                    HAL_GPIO_WritePin(ATK_MC7725F_WEN_GPIO_PORT, ATK_MC7725F_WEN_GPIO_PIN, GPIO_PIN_RESET);     \
                                                }while(0)

/* ATK-MC7725F SCCB通讯地址 */
#define ATK_MC7725F_SCCB_ADDR   0x21


/* ATK-MC7725F模块在不同输出模式下的最大输出分辨率 */
#define ATK_MC7725F_VGA_WIDTH_MAX   640
#define ATK_MC7725F_VGA_HEIGHT_MAX  480
#define ATK_MC7725F_QVGA_WIDTH_MAX  320
#define ATK_MC7725F_QVGA_HEIGHT_MAX 240

/* ATK-MC7725F模块灯光模式枚举 */
typedef enum
{
    ATK_MC7725F_LIGHT_MODE_AUTO = 0x00,         /* Auto */
    ATK_MC7725F_LIGHT_MODE_SUNNY,               /* Sunny */
    ATK_MC7725F_LIGHT_MODE_CLOUDY,              /* Cloudy */
    ATK_MC7725F_LIGHT_MODE_OFFICE,              /* Office */
    ATK_MC7725F_LIGHT_MODE_HOME,                /* Home */
    ATK_MC7725F_LIGHT_MODE_NIGHT,               /* Night */
} atk_mc7725f_light_mode_t;

/* ATK-MC7725F模块色彩饱和度枚举 */
typedef enum
{
    ATK_MC7725F_COLOR_SATURATION_0 = 0x00,      /* +4 */
    ATK_MC7725F_COLOR_SATURATION_1,             /* +3 */
    ATK_MC7725F_COLOR_SATURATION_2,             /* +2 */
    ATK_MC7725F_COLOR_SATURATION_3,             /* +1 */
    ATK_MC7725F_COLOR_SATURATION_4,             /* 0 */
    ATK_MC7725F_COLOR_SATURATION_5,             /* -1 */
    ATK_MC7725F_COLOR_SATURATION_6,             /* -2 */
    ATK_MC7725F_COLOR_SATURATION_7,             /* -3 */
    ATK_MC7725F_COLOR_SATURATION_8,             /* -4 */
} atk_mc7725f_color_saturation_t;

/* ATK-MC7725F模块亮度枚举 */
typedef enum
{
    ATK_MC7725F_BRIGHTNESS_0 = 0x00,            /* +4 */
    ATK_MC7725F_BRIGHTNESS_1,                   /* +3 */
    ATK_MC7725F_BRIGHTNESS_2,                   /* +2 */
    ATK_MC7725F_BRIGHTNESS_3,                   /* +1 */
    ATK_MC7725F_BRIGHTNESS_4,                   /* 0 */
    ATK_MC7725F_BRIGHTNESS_5,                   /* -1 */
    ATK_MC7725F_BRIGHTNESS_6,                   /* -2 */
    ATK_MC7725F_BRIGHTNESS_7,                   /* -3 */
    ATK_MC7725F_BRIGHTNESS_8,                   /* -4 */
} atk_mc7725f_brightness_t;

/* ATK-MC7725F模块对比度枚举 */
typedef enum
{
    ATK_MC7725F_CONTRAST_0 = 0x00,              /* +4 */
    ATK_MC7725F_CONTRAST_1,                     /* +3 */
    ATK_MC7725F_CONTRAST_2,                     /* +2 */
    ATK_MC7725F_CONTRAST_3,                     /* +1 */
    ATK_MC7725F_CONTRAST_4,                     /* 0 */
    ATK_MC7725F_CONTRAST_5,                     /* -1 */
    ATK_MC7725F_CONTRAST_6,                     /* -2 */
    ATK_MC7725F_CONTRAST_7,                     /* -3 */
    ATK_MC7725F_CONTRAST_8,                     /* -4 */
} atk_mc7725f_contrast_t;

/* ATK-MC7725F模块特殊效果枚举 */
typedef enum
{
    ATK_MC7725F_SPECIAL_EFFECT_NORMAL = 0x00,   /* Normal */
    ATK_MC7725F_SPECIAL_EFFECT_BW,              /* B&W */
    ATK_MC7725F_SPECIAL_EFFECT_BLUISH,          /* Bluish */
    ATK_MC7725F_SPECIAL_EFFECT_SEPIA,           /* Sepia */
    ATK_MC7725F_SPECIAL_EFFECT_REDISH,          /* Redish */
    ATK_MC7725F_SPECIAL_EFFECT_GREENISH,        /* Greenish */
    ATK_MC7725F_SPECIAL_EFFECT_NEGATIVE,        /* Negative */
} atk_mc7725f_special_effect_t;

/* ATK-MC7725F模块输出模式枚举 */
typedef enum
{
    ATK_MC7725F_OUTPUT_MODE_VGA = 0x00,         /* VGA */
    ATK_MC7725F_OUTPUT_MODE_QVGA,               /* QVGA */
} atk_mc7725f_output_mode_t;

/* ATK-MC7725F模块获取图像方式枚举 */
typedef enum
{
    ATK_MC7725F_GET_FRAME_TYPE_NOINC = 0x00,    /* 目的地址不自增 */
    ATK_MC7725F_GET_FRAME_TYPE_AUTO_INC,        /* 目的地址自增 */
} atk_mc7725f_get_frame_type_t;

/* 错误代码 */
#define ATK_MC7725F_EOK     0   /* 没有错误 */
#define ATK_MC7725F_ERROR   1   /* 错误 */
#define ATK_MC7725F_EINVAL  2   /* 非法参数 */
#define ATK_MC7725F_EEMPTY  3   /* 资源为空 */
#define ATK_MC7725F_TEST    1
/* 操作函数 */
uint8_t atk_mc7725f_init(void);                                                                     /* 初始化ATK-MC7725F模块 */
uint8_t atk_mc7725f_set_light_mode(atk_mc7725f_light_mode_t mode);                                  /* 设置ATK-MC7725F模块灯光模式 */
uint8_t atk_mc7725f_set_color_saturation(atk_mc7725f_color_saturation_t saturation);                /* 设置ATK-MC7725F模块色彩饱和度 */
uint8_t atk_mc7725f_set_brightness(atk_mc7725f_brightness_t brightness);                            /* 设置ATK-MC7725F模块亮度 */
uint8_t atk_mc7725f_set_contrast(atk_mc7725f_contrast_t contrast);                                  /* 设置ATK-MC7725F模块对比度 */
uint8_t atk_mc7725f_set_special_effect(atk_mc7725f_special_effect_t effect);                        /* 设置ATK-MC7725F模块特殊效果 */
uint8_t atk_mc7725f_set_output(uint16_t width, uint16_t height, atk_mc7725f_output_mode_t mode);    /* 设置ATK-MC7725F模块输出模式 */
void atk_mc7725f_enable_output(void);                                                               /* 使能ATK-MD7725F模块输出图像 */
void atk_mc7725f_disable_output(void);                                                              /* 禁止ATK-MC7725F模块输出图像 */
uint16_t atk_mc7725f_get_output_width(void);                                                        /* 获取ATK-MC7725F模块输出图像宽度 */
uint16_t atk_mc7725f_get_output_height(void);                                                       /* 获取ATK-MC7725F模块输出图像高度 */
uint8_t atk_mc7725f_get_frame(volatile uint16_t *dts, atk_mc7725f_get_frame_type_t type);           /* 获取ATK-MC7725F模块输出的一帧图像数据 */

#if ATK_MC7725F_TEST == 1
void atk_mc7725f_camera_test(void);
#endif
#endif
