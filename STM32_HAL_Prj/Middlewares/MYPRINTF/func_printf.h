#ifndef __FUNC_PRINTF_H__
#define __FUNC_PRINTF_H__

/* 头文件 */
#include <stdio.h>
#include <string.h>
/* 宏定义 */
//#define _WIN32
#define __WIN32 //与ff.h冲突
#ifdef __WIN32
   //define something for Windows (32-bit and 64-bit, this part is common)
   #define filename(x) (strrchr(x,'\\')?(strrchr(x,'\\')+1):x)
   #ifdef _WIN64
      //define something for Windows (64-bit only)
   #else
      //define something for Windows (32-bit only)
   #endif
#elif __APPLE__
    #if TARGET_IPHONE_SIMULATOR
         // iOS Simulator
    #elif TARGET_OS_IPHONE
        // iOS device
    #elif TARGET_OS_MAC
        // Other kinds of Mac OS
	#endif
#elif __ANDROID__
    // android
#elif __linux__
    // linux
	#define filename(x) (strrchr(x,'/')?(strrchr(x,'/')+1):x)
#elif __unix__ // all unices not caught above
    // Unix
#elif defined(_POSIX_VERSION)
    // POSIX
#else
	#error "Unknown patform"
#endif

/* 平时调试使用, 注意一定要在uart初始化之后使用 */
#define PRTE(fmt...) \
        do                                                                              \
        {                                                                               \
            printf("[ERR_LOG][%s:%d][%s]", filename(__FILE__), __LINE__, __FUNCTION__); \
            printf(fmt);                                                                \
        } while(0)
#define PRT(fmt...)                                                                     \
        do                                                                              \
        {                                                                               \
            printf("[INF_LOG][%s:%d][%s]", filename(__FILE__), __LINE__, __FUNCTION__); \
            printf(fmt);                                                                \
        } while(0)
/* 结构体定义 */

/* 全局变量声明 */

/* 函数声明 */

int func_pts_get_ms(void);

#endif /* __FUNC_PRINTF_H__ */
